export const getImageUrl = id => `${process.env.REACT_APP_SERVER_URL}/api/images/${id}`;
