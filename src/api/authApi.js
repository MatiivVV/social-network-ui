import requestMethod from "./requestMethod";
import {UnexpectedResponseStatusError} from "./error/UnexpectedResponseStatusError";
import {UnauthorizedError} from "./error/UnauthorizedError";

export const getTokenApi = (login, password) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/oauth/token?grant_type=password` +
        `&username=${encodeURIComponent(login)}` +
        `&password=${encodeURIComponent(password)}`, {
        method: requestMethod.POST,
        headers: {
            "Authorization": `Basic ${btoa(process.env.REACT_APP_SERVER_LOGIN + ":" + process.env.REACT_APP_SERVER_PASSWORD)}`
        }
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else if (response.status === 400) {
            throw new UnauthorizedError();
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const refreshTokenApi = refreshToken => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/oauth/token?grant_type=refresh_token` +
        `&refresh_token=${encodeURIComponent(refreshToken)}`, {
        method: requestMethod.POST,
        headers: {
            "Authorization": `Basic ${btoa(process.env.REACT_APP_SERVER_LOGIN + ":" + process.env.REACT_APP_SERVER_PASSWORD)}`
        }
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else if (response.status === 400) {
            throw new UnauthorizedError();
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};
