import SockJS from "sockjs-client"
import Stomp from 'stompjs';

let client;
let subscription;

export const subscribeNotificationsApi = (id, onNotification, accessToken) => {
    return new Promise((resolve, reject) => {
        try {
            if (!client) {
                let ws = new SockJS(`${process.env.REACT_APP_SERVER_URL}/notifications/?access_token=${accessToken}`);
                client = Stomp.over(ws);
            }
            client.connect({},
                frame => {
                    subscription = client.subscribe(`/user/topic/notifications/`, onNotification);
                    resolve(frame);
                },
                error => reject(new Error(error.headers.message)),
            );
        } catch (e) {
            reject(e);
        } finally {
            window.onbeforeunload = unsubscribeNotificationsApi;
        }
    });
};

export const unsubscribeNotificationsApi = () => {
    return new Promise((resolve, reject) => {
        try {
            if (client) {
                subscription.unsubscribe();
                subscription = null;
                client.disconnect(resolve);
                client = null;
            }
        } catch (e) {
            reject(e);
        }
    });
};
