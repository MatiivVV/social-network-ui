import mediaType from "../mediaType";
import requestMethod from "../requestMethod";
import {mapError, mapLocation, mapPage} from "../../util/apiUtil";
import {UnprocessableEntityError} from "../error/UnprocessableEntityError";
import {UnexpectedResponseStatusError} from "../error/UnexpectedResponseStatusError";
import * as queryString from "query-string";

export const getGroupApi = (id, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${id}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else if (response.status === 404) {
            return null;
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getGroupsApi = (pattern, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        pattern: pattern,
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups?${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const createGroupApi = (group, image, accessToken, locale) => {
    let formData = new FormData();
    if (image) {
        formData.append("groupImage", image);
    }
    formData.append("group",
        new Blob([JSON.stringify(group)],
            {type: mediaType.JSON}
        )
    );
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups`, {
        method: requestMethod.POST,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON
        },
        body: formData
    }).then(async response => {
        if (response.status === 201) {
            return mapLocation(response.headers.get("Location"));
        } else if (response.status === 422) {
            throw new UnprocessableEntityError(mapError(await response.json(), "createGroup.group"));
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const editGroupInfoApi = (id, info, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${id}?part=info`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify(info)
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editGroupInfo.groupInfo"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editGroupImageApi = (id, image, accessToken, locale) => {
    let formData = new FormData();
    if (image) {
        formData.append("groupImage", image);
    }
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${id}?part=image`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON
        },
        body: formData
    }).then(async response => {
            if (response.status === 204) {
                return response.headers.get("Location");
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editGroupDeletedApi = (id, deleted, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${id}?part=deleted`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            deleted: deleted
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const deleteGroupApi = (id, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${id}`, {
        method: requestMethod.DELETE,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};
