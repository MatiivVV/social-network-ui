import mediaType from "../mediaType";
import requestMethod from "../requestMethod";
import {UnexpectedResponseStatusError} from "../error/UnexpectedResponseStatusError";
import {mapError, mapLocation, mapPage} from "../../util/apiUtil";
import {UnprocessableEntityError} from "../error/UnprocessableEntityError";
import * as queryString from "query-string";

export const getMembershipApi = (accountId, groupId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships/${groupId}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else if (response.status === 404) {
            return null;
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getMembershipsApi = (accountId, roles, statuses, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        roles: roles,
        statuses: statuses,
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships?${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getGroupsApi = (accountId, roles, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        roles: roles,
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships?groups&${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const createMembershipApi = (accountId, groupId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships/${groupId}`, {
        method: requestMethod.POST,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 201) {
                return mapLocation(response.headers.get("Location"));
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editMembershipRoleApi = (accountId, groupId, role, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships/${groupId}?part=role`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            role: role
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editMembershipRole.membershipRole"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editMembershipStatusApi = (accountId, groupId, status, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships/${groupId}?part=status`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            status: status
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editMembershipStatus.membershipStatus"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const deleteMembershipApi = (accountId, groupId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/memberships/${groupId}`, {
        method: requestMethod.DELETE,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};
