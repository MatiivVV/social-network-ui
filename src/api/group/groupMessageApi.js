import mediaType from "../mediaType";
import requestMethod from "../requestMethod";
import {UnexpectedResponseStatusError} from "../error/UnexpectedResponseStatusError";
import {mapError, mapPage} from "../../util/apiUtil";
import {UnprocessableEntityError} from "../error/UnprocessableEntityError";
import * as queryString from "query-string";

export const getGroupMessageApi = (groupId, authorId, messageId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${groupId}/authors/${authorId}/messages/${messageId}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else if (response.status === 404) {
            return null;
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getGroupMessagesApi = (groupId, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${groupId}/messages?${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const sendGroupMessageApi = (groupId, authorId, message, image, accessToken, locale) => {
    let formData = new FormData();
    if (image) {
        formData.append("image", image);
    }
    formData.append("message",
        new Blob([JSON.stringify(message)],
            {type: mediaType.JSON}
        )
    );
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${groupId}/authors/${authorId}/messages`, {
        method: requestMethod.POST,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON
        },
        body: formData
    }).then(async response => {
            if (response.status === 201) {
                return response.json();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "sendGroupMessage.message"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const deleteGroupMessageApi = (groupId, authorId, messageId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/groups/${groupId}/authors/${authorId}/messages/${messageId}`, {
        method: requestMethod.DELETE,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};
