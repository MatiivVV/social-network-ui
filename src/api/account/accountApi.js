import mediaType from "../mediaType";
import requestMethod from "../requestMethod";
import {mapError, mapLocation, mapPage} from "../../util/apiUtil";
import {UnexpectedResponseStatusError} from "../error/UnexpectedResponseStatusError";
import {UnprocessableEntityError} from "../error/UnprocessableEntityError";
import * as queryString from 'query-string';

export const getAccountApi = (id, accessToken, locale, acceptMediaType = mediaType.JSON) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": acceptMediaType,
            "Content-Type": mediaType.JSON
        }
    }).then(response => {
        if (response.status === 200) {
            return acceptMediaType === mediaType.JSON ? response.json() : response.text();
        } else if (response.status === 404) {
            return null;
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getAccountsApi = (pattern, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        pattern: pattern,
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts?${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const registerAccountApi = (account, image, locale) => {
    let formData = new FormData();
    if (image) {
        formData.append("accountImage", image);
    }
    formData.append("account",
        new Blob([JSON.stringify(account)],
            {type: mediaType.JSON}
        )
    );
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts`, {
        method: requestMethod.POST,
        headers: {
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
        },
        body: formData
    }).then(async response => {
            if (response.status === 201) {
                return mapLocation(response.headers.get("Location"));
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "registerAccount.account"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editAccountInfoApi = (id, info, accessToken, locale, contentMediaType = mediaType.JSON) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}?part=info`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": contentMediaType
        },
        body: contentMediaType === mediaType.JSON ? JSON.stringify(info) : info
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editAccountInfo.accountInfo"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editAccountPasswordApi = (id, oldPassword, password, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}?part=password`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            oldPassword: oldPassword,
            rawPassword: password
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editAccountPassword.accountPassword"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editAccountRoleApi = (id, role, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}?part=role`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            role: role
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editAccountRole.accountRole"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editAccountImageApi = (id, image, accessToken, locale) => {
    let formData = new FormData();
    if (image) {
        formData.append("accountImage", image);
    }
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}?part=image`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON
        },
        body: formData
    }).then(async response => {
            if (response.status === 204) {
                return response.headers.get("Location");
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const resetAccountSessionApi = (id, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}?part=session`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editAccountDeletedApi = (id, deleted, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}?part=deleted`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            deleted: deleted
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const deleteAccountApi = (id, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${id}`, {
        method: requestMethod.DELETE,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};
