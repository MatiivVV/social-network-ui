import mediaType from "../mediaType";
import requestMethod from "../requestMethod";
import {UnexpectedResponseStatusError} from "../error/UnexpectedResponseStatusError";
import {mapError, mapLocation, mapPage} from "../../util/apiUtil";
import {UnprocessableEntityError} from "../error/UnprocessableEntityError";
import * as queryString from "query-string";

export const getFriendshipApi = (accountId, friendId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/friendships/${friendId}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else if (response.status === 404) {
            return null;
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getFriendshipsApi = (accountId, types, statuses, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        types: types,
        statuses: statuses,
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/friendships?${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const getFriendsApi = (accountId, page, size, accessToken, locale) => {
    let query = queryString.stringify({
        page: page,
        size: size
    });
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/friendships?friends&${query}`, {
        method: requestMethod.GET,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
        if (response.status === 200) {
            return mapPage(await response.json());
        }
        throw new UnexpectedResponseStatusError(response.status);
    });
};

export const createFriendshipApi = (accountId, friendId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/friendships/${friendId}`, {
        method: requestMethod.POST,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 201) {
                return mapLocation(response.headers.get("Location"));
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const editFriendshipStatusApi = (accountId, friendId, status, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/friendships/${friendId}?part=status`, {
        method: requestMethod.PATCH,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        },
        body: JSON.stringify({
            status: status
        })
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json(), "editFriendshipStatus.friendshipStatus"));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};

export const deleteFriendshipApi = (accountId, friendId, accessToken, locale) => {
    return fetch(`${process.env.REACT_APP_SERVER_URL}/api/accounts/${accountId}/friendships/${friendId}`, {
        method: requestMethod.DELETE,
        headers: {
            "Authorization": `Bearer ${accessToken}`,
            "Accept-Language": locale,
            "Accept": mediaType.JSON,
            "Content-Type": mediaType.JSON
        }
    }).then(async response => {
            if (response.status === 204) {
                return Promise.resolve();
            } else if (response.status === 422) {
                throw new UnprocessableEntityError(mapError(await response.json()));
            }
            throw new UnexpectedResponseStatusError(response.status);
        }
    );
};
