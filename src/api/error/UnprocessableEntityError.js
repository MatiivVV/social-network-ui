export function UnprocessableEntityError(error) {
    this.name = "UnprocessableEntityError";
    this.message = "Invalid entity data";
    this.error = error;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, this.constructor);
    } else {
        this.stack = new Error().stack;
    }
}

UnprocessableEntityError.prototype = Object.create(Error.prototype);
UnprocessableEntityError.prototype.constructor = UnprocessableEntityError;
