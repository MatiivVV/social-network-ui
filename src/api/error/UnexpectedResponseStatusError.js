export function UnexpectedResponseStatusError(status) {
    this.name = "UnexpectedResponseStatusError";
    this.status = status;
    this.message = `Illegal response status ${status}`;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, this.constructor);
    } else {
        this.stack = new Error().stack;
    }
}

UnexpectedResponseStatusError.prototype = Object.create(Error.prototype);
UnexpectedResponseStatusError.prototype.constructor = UnexpectedResponseStatusError;
