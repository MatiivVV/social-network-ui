import React, {Component} from "react";
import {Col, Container, CustomInput, FormGroup, Input, Row} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faAt, faSignInAlt, faUnlock} from "@fortawesome/free-solid-svg-icons"
import PropTypes from "prop-types";
import FormInputGroup from "../ui/form/FormInputGroup";
import ProgressButton from "../ui/form/ProgressButton";
import CardForm from "../ui/form/CardForm";
import InputGroupIcon from "../ui/form/InputGroupIcon";
import {FORM_INITIAL_STATE} from "../../initialState";

class Login extends Component {
    render() {
        let {form, onChange, onLogin, t} = this.props;
        return <Container className="mb-3">
            <Row className="justify-content-center align-items-center">
                <Col lg="6">
                    <CardForm header={<h4>{t("title.account.login")}</h4>} className="mt-3">
                        <FormInputGroup label={t("title.account.emailAddress")}
                                        prepend={<InputGroupIcon icon={faAt}/>}>
                            <Input type="email"
                                   name="emailAddress"
                                   value={form.emailAddress || ""}
                                   onChange={onChange}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.password")}
                                        prepend={<InputGroupIcon icon={faUnlock}/>}>
                            <Input type="password"
                                   name="password"
                                   value={form.password || ""}
                                   onChange={onChange}/>
                        </FormInputGroup>
                        <FormGroup>
                            <CustomInput type="checkbox"
                                         id="rememberMe"
                                         name="rememberMe"
                                         checked={form.rememberMe || false}
                                         label={t("title.account.rememberMe")}
                                         onChange={onChange}/>
                        </FormGroup>
                        <FormGroup>
                            <ProgressButton color="primary"
                                            active={form._submitting}
                                            onClick={() => onLogin(form)}>
                                <FontAwesomeIcon icon={faSignInAlt}/>
                                {" " + t("title.account.login")}
                            </ProgressButton>
                        </FormGroup>
                    </CardForm>
                </Col>
            </Row>
        </Container>;
    }
}

export default Login;

Login.propTypes = {
    form: PropTypes.shape({
        emailAddress: PropTypes.string,
        password: PropTypes.string,
        rememberMe: PropTypes.bool
    }),
    onChange: PropTypes.func.isRequired,
    onLogin: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

Login.defaultProps = {
    form: FORM_INITIAL_STATE
};
