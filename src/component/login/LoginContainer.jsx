import Login from "./Login";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {loginAction} from "../../action/user/loginAction";
import {changeFormByEventAction} from "../../action/form/changeFormAction";
import formType from "../../action/form/formType";


const mapStateToProps = state => ({
    form: state.forms[formType.LOGIN]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.LOGIN, e)),
    onLogin: form => dispatch(loginAction(form))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Login));
