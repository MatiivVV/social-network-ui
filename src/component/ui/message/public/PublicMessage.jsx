import React from "react";
import {ListGroupItem, Media} from "reactstrap";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import route from "../../../../constant/route";
import * as imageApi from "../../../../api/imageApi";

const PublicMessage = ({children, message, postedAtTitle, ...rest} = {}) => {
    return <ListGroupItem action {...rest}>
        <Media>
            <img
                src={!message.authorDeleted && message.authorImageId ? imageApi.getImageUrl(message.authorImageId) : "/image/missingAccountImage.jpg"}
                className="my-1 mr-2 rounded-circle img-preview"
                alt=""/>
            <Media body className="mx-2">
                <Link to={route.ACCOUNT(message.authorId)}>
                    {message.authorFirstName + " " + message.authorLastName + " "}
                </Link>
                <small>
                    <i>{postedAtTitle + " " + message.postedAt.substring(0, 10) + " " + message.postedAt.substring(11, 19)}</i>
                </small>
                <div className="float-right d-inline">
                    {children}
                </div>
                <p className="mt-1">
                    {message.message}
                </p>
                {message.imageId && <p className="mt-4">
                    <img src={imageApi.getImageUrl(message.imageId)}
                         className="img-fluid"
                         alt=""/>
                </p>}
            </Media>
        </Media>
    </ListGroupItem>;
};

export default PublicMessage;

PublicMessage.propTypes = {
    message: PropTypes.shape({
        id: PropTypes.number.isRequired,
        authorId: PropTypes.number.isRequired,
        authorFirstName: PropTypes.string.isRequired,
        authorLastName: PropTypes.string.isRequired,
        authorImageId: PropTypes.string,
        authorDeleted: PropTypes.bool.isRequired,
        message: PropTypes.string.isRequired,
        imageId: PropTypes.string,
        postedAt: PropTypes.string.isRequired
    }).isRequired,
    postedAtTitle: PropTypes.string.isRequired
};
