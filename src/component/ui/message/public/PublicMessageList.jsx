import React, {Component} from "react";
import {Button, Card, CardHeader, Collapse, Spinner} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";
import {faComments} from "@fortawesome/free-solid-svg-icons/index";
import {FORM_INITIAL_STATE} from "../../../../initialState";
import ListGroup from "reactstrap/es/ListGroup";
import PublicMessage from "./PublicMessage";
import PropTypes from "prop-types";
import DynamicPagination from "../../DynamicPagination";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

class PublicMessageList extends Component {
    constructor(props) {
        super(props);
        this.handleToggle = this.handleToggle.bind(this);
        this.state = {
            collapsed: props.collapsed
        };
    }

    handleToggle() {
        this.setState(state => ({
            collapsed: !state.collapsed
        }));
    }

    render() {
        let {
            form,
            recipient,
            editable,
            visiblePages,
            pageSize,
            onRefresh,
            onDeleteMessage,
            messagesTitle,
            postedAtTitle,
            deleteTitle,
            ...rest
        } = this.props;
        return <Card {...rest}>
            <CardHeader>
                <Button color="link" onClick={this.handleToggle}>
                    <FontAwesomeIcon icon={faComments}/>
                    {" " + messagesTitle}
                </Button>
                {!this.state.collapsed && form._results.totalPages > 1 && <DynamicPagination
                    page={form._results.page}
                    totalPages={form._results.totalPages}
                    visiblePages={visiblePages}
                    onSelectPage={page => onRefresh(recipient, page, pageSize)}
                    size="sm"
                    className="mt-1 mr-1 d-inline-flex float-right"
                />}
            </CardHeader>
            <Collapse isOpen={!this.state.collapsed}>
                {form._submitting ?
                    <div className="text-center mt-3">
                        <Spinner color="primary"/>
                    </div> :
                    <div>
                        <ListGroup flush={true} className="mt-0">
                            {form._results.content && form._results.content.map(message =>
                                <PublicMessage key={message.id}
                                               message={message}
                                               postedAtTitle={postedAtTitle}>
                                    {editable && <Button color="primary"
                                                         onClick={() => onDeleteMessage(message.id, recipient, message.authorId)}>
                                        <FontAwesomeIcon icon={faTimes}/>
                                        {" " + deleteTitle}
                                    </Button>}
                                </PublicMessage>)}
                        </ListGroup>
                    </div>}
            </Collapse>
        </Card>;
    }
}

export default PublicMessageList;

PublicMessageList.propTypes = {
    form: PropTypes.shape({
        page: PropTypes.number
    }),
    recipient: PropTypes.number.isRequired,
    editable: PropTypes.bool.isRequired,
    visiblePages: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    onRefresh: PropTypes.func.isRequired,
    onDeleteMessage: PropTypes.func.isRequired,
    messagesTitle: PropTypes.string.isRequired,
    postedAtTitle: PropTypes.string.isRequired,
    deleteTitle: PropTypes.string
};

PublicMessageList.defaultProps = {
    form: {
        ...FORM_INITIAL_STATE,
        page: 1
    }
};
