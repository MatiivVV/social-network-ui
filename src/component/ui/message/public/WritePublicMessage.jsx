import React, {Component} from "react";
import {Button, CustomInput, FormFeedback, FormGroup, Input} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";
import {faComment, faEnvelope, faImage} from "@fortawesome/free-solid-svg-icons/index";
import FormInputGroup from "../../form/FormInputGroup";
import {existViolationsByField, getViolationsByField} from "../../../../util/formUtil";
import InputGroupIcon from "../../form/InputGroupIcon";
import ProgressButton from "../../form/ProgressButton";
import CardForm from "../../form/CardForm";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../../../initialState";

class WritePublicMessage extends Component {
    constructor(props) {
        super(props);
        this.handleToggle = this.handleToggle.bind(this);
        this.image = React.createRef();
        this.state = {
            collapsed: props.collapsed
        };
    }

    handleToggle() {
        this.setState(state => ({
            collapsed: !state.collapsed
        }));
    }

    render() {
        let {recipient, form, onChange, onSend, writeTitle, browseTitle, sendTitle, collapsed, ...rest} = this.props;
        return <CardForm
            collapsible={true}
            collapsed={this.state.collapsed}
            header={
                <Button color="link" onClick={this.handleToggle}>
                    <FontAwesomeIcon icon={faComment}/>
                    {" " + writeTitle}
                </Button>}
            {...rest}>
            <FormInputGroup violations={getViolationsByField(form._violations, "message")}>
                <Input type="textarea"
                       name="message"
                       value={form.message || ""}
                       rows="3"
                       onChange={onChange}
                       invalid={existViolationsByField(form._violations, "message")}/>
            </FormInputGroup>
            <FormInputGroup prepend={<InputGroupIcon icon={faImage}/>}>
                <CustomInput type="file"
                             name="image"
                             id="image"
                             label={browseTitle}
                             innerRef={this.image}
                             accept="image/jpeg, image/jpg, image/png, image/gif"
                             invalid={existViolationsByField(form._violations, "image")}
                             className="mb-0"/>
                {getViolationsByField(form._violations, "image")
                    .map((violation, index) => <FormFeedback key={index}>
                        {violation.message}
                    </FormFeedback>)}
            </FormInputGroup>
            <FormGroup>
                <ProgressButton color="primary"
                                active={form._submitting}
                                onClick={() => onSend(recipient, form, this.image.current.files[0])}>
                    <FontAwesomeIcon icon={faEnvelope}/>
                    {" " + sendTitle}
                </ProgressButton>
            </FormGroup>
        </CardForm>;
    }
}

export default WritePublicMessage;

WritePublicMessage.propTypes = {
    recipient: PropTypes.number.isRequired,
    form: PropTypes.shape({
        message: PropTypes.string
    }),
    onChange: PropTypes.func.isRequired,
    onSend: PropTypes.func.isRequired,
    writeTitle: PropTypes.string.isRequired,
    browseTitle: PropTypes.string.isRequired,
    sendTitle: PropTypes.string.isRequired
};

WritePublicMessage.defaultProps = {
    form: FORM_INITIAL_STATE
};
