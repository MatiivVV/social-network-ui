import React, {Component} from "react";
import {Card, CardBody, CardHeader, CustomInput, Form, FormFeedback, Input, Spinner} from "reactstrap";
import {FORM_INITIAL_STATE} from "../../../../initialState";
import PropTypes from "prop-types";
import DynamicPagination from "../../DynamicPagination";
import ChatMessage from "./ChatMessage";
import FormInputGroup from "../../form/FormInputGroup";
import {existViolationsByField, getViolationsByField} from "../../../../util/formUtil";
import ProgressButton from "../../form/ProgressButton";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope, faImage} from "@fortawesome/free-solid-svg-icons";
import InputGroupIcon from "../../form/InputGroupIcon";
import * as imageApi from "../../../../api/imageApi";
import {Link} from "react-router-dom";
import route from "../../../../constant/route";

class ChatMessages extends Component {
    constructor(props) {
        super(props);
        this.scrollToBottom = this.scrollToBottom.bind(this);
        this.readMessages = this.readMessages.bind(this);
        this.image = React.createRef();
    }

    componentDidMount() {
        this.scrollToBottom();
        this.readMessages();
    }

    componentDidUpdate() {
        this.scrollToBottom();
        this.readMessages();
    }

    readMessages() {
        let {account, opponent, messagesForm, onRead} = this.props;
        if (account.id && opponent.id
            && messagesForm && messagesForm._results && messagesForm._results.page === 1) {
            onRead(account.id, opponent.id);
        }
    }

    scrollToBottom() {
        if (this.bottomElement) {
            this.bottomElement.scrollIntoView(false);
        }
    }

    render() {
        let {
            account,
            opponent,
            messagesForm,
            messageForm,
            visiblePages,
            pageSize,
            onRefresh,
            onChangeMessage,
            onSendMessage,
            messagesTitle,
            writeTitle,
            browseTitle,
            sendTitle,
            ...rest
        } = this.props;
        return <Card {...rest}>
            <CardHeader>
                <img src={opponent.imageId ? imageApi.getImageUrl(opponent.imageId) : "/image/missingAccountImage.jpg"}
                     className="my-1 mr-2 rounded-circle img-icon"
                     alt=""/>
                {opponent.id && <Link to={route.ACCOUNT(opponent.id)} className="navbar-brand mr-auto mt-1 my-lg-1">
                    {opponent.firstName && opponent.firstName}
                    {opponent.firstName && opponent.lastName && " "}
                    {opponent.lastName && opponent.lastName}
                </Link>}
                {messagesForm._results.totalPages > 1 && <DynamicPagination
                    page={messagesForm._results.page}
                    totalPages={messagesForm._results.totalPages}
                    visiblePages={visiblePages}
                    onSelectPage={page => onRefresh(account.id, opponent.id, page, pageSize)}
                    size="sm"
                    className="mt-2 mr-1 d-inline-flex float-right"
                />}
            </CardHeader>
            {messagesForm._submitting ?
                <div className="text-center mt-3">
                    <Spinner color="primary"/>
                </div> :
                <div>
                    <div className="mt-0 chat">
                        {messagesForm._results.content && messagesForm._results.content.map((message) =>
                            <ChatMessage key={message.id}
                                         message={message}
                                         author={account.id === message.authorId ? account : opponent}
                                         outMessage={account.id === message.authorId}
                                         onLoad={this.scrollToBottom}>
                            </ChatMessage>)}
                        <div ref={(el) => {
                            this.bottomElement = el;
                        }}/>
                    </div>
                </div>}
            <CardBody className="border-top">
                <Form>
                    <FormInputGroup violations={getViolationsByField(messageForm._violations, "message")}
                                    append={<ProgressButton color="primary"
                                                            active={messageForm._submitting}
                                                            onClick={() => onSendMessage(
                                                                opponent.id,
                                                                account.id,
                                                                messageForm,
                                                                this.image.current.files[0]
                                                            )}>
                                        <FontAwesomeIcon icon={faEnvelope}/>
                                        {" " + sendTitle}
                                    </ProgressButton>}>
                        <Input type="textarea"
                               name="message"
                               value={messageForm.message || ""}
                               rows="1"
                               onChange={onChangeMessage}
                               invalid={existViolationsByField(messageForm._violations, "message")}/>
                    </FormInputGroup>
                    <FormInputGroup prepend={<InputGroupIcon icon={faImage}/>} className="mb-0">
                        <CustomInput type="file"
                                     name="image"
                                     id="image"
                                     label={browseTitle}
                                     innerRef={this.image}
                                     accept="image/jpeg, image/jpg, image/png, image/gif"
                                     invalid={existViolationsByField(messageForm._violations, "image")}
                                     className="mb-0"/>
                        {getViolationsByField(messageForm._violations, "image")
                            .map((violation, index) => <FormFeedback key={index}>
                                {violation.message}
                            </FormFeedback>)}
                    </FormInputGroup>
                </Form>
            </CardBody>
        </Card>;
    }
}

export default ChatMessages;

ChatMessages.propTypes = {
    account: PropTypes.shape({
        id: PropTypes.number.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        imageId: PropTypes.string,
        deleted: PropTypes.bool.isRequired
    }).isRequired,
    opponent: PropTypes.shape({
        id: PropTypes.number.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        imageId: PropTypes.string,
        deleted: PropTypes.bool.isRequired
    }).isRequired,
    messagesForm: PropTypes.shape({
        page: PropTypes.number
    }),
    messageForm: PropTypes.shape({
        message: PropTypes.string
    }),
    visiblePages: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    onRefresh: PropTypes.func.isRequired,
    onChangeMessage: PropTypes.func.isRequired,
    onSendMessage: PropTypes.func.isRequired,
    onRead: PropTypes.func.isRequired,
    messagesTitle: PropTypes.string.isRequired,
    writeTitle: PropTypes.string.isRequired,
    browseTitle: PropTypes.string.isRequired,
    sendTitle: PropTypes.string.isRequired
};

ChatMessages.defaultProps = {
    messagesForm: {
        ...FORM_INITIAL_STATE,
        page: 1
    },
    messageForm: FORM_INITIAL_STATE
};
