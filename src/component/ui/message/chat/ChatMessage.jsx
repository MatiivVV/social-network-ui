import React from "react";
import {Media} from "reactstrap";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import route from "../../../../constant/route";
import * as imageApi from "../../../../api/imageApi";

const ChatMessage = ({children, message, author, outMessage, onLoad, ...rest} = {}) => {
    let authorImage = <Link to={route.ACCOUNT(author.id)}>
        <img
            src={!author.deleted && author.imageId ? imageApi.getImageUrl(author.imageId) : "/image/missingAccountImage.jpg"}
            className="mx-2 rounded-circle img-preview"
            alt=""/>
    </Link>;
    return <Media className="my-3 mx-2" {...rest}>
        {!outMessage && authorImage}
        <Media body className={"mx-2 px-2 pt-1 rounded " + (outMessage ? "out-chat-message" : "in-chat-message")}>
            <small>
                <i>{message.postedAt.substring(0, 10) + " " + message.postedAt.substring(11, 19)}</i>
            </small>
            <div className="float-right d-inline">
                {children}
            </div>
            {message.message && <p className="mt-1">
                {message.message}
            </p>}
            {message.imageId && <p className="mb-2 mt-1">
                <img src={imageApi.getImageUrl(message.imageId)}
                     onLoad={onLoad}
                     className="img-fluid"
                     alt=""/>
            </p>}
        </Media>
        {outMessage && authorImage}
    </Media>;
};

export default ChatMessage;

ChatMessage.propTypes = {
    message: PropTypes.shape({
        id: PropTypes.number.isRequired,
        message: PropTypes.string.isRequired,
        imageId: PropTypes.string,
        postedAt: PropTypes.string.isRequired
    }).isRequired,
    author: PropTypes.shape({
        id: PropTypes.number.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        imageId: PropTypes.string,
        deleted: PropTypes.bool.isRequired
    }).isRequired,
    outMessage: PropTypes.bool.isRequired,
    onLoad: PropTypes.func
};
