import React from "react";
import {FormFeedback, FormGroup, InputGroup, InputGroupAddon, Label} from "reactstrap";
import PropTypes from "prop-types";

const FormInputGroup = ({children, label, prepend, append, violations, ...rest} = {}) => {
    return <FormGroup {...rest}>
        {label && <Label>
            {label}
        </Label>}
        <InputGroup>
            {prepend && <InputGroupAddon addonType="prepend">
                {prepend}
            </InputGroupAddon>}
            {children}
            {append && <InputGroupAddon addonType="append">
                {append}
            </InputGroupAddon>}
            {violations && violations.map((violation, index) => <FormFeedback key={index}>
                {violation.message}
            </FormFeedback>)}
        </InputGroup>
    </FormGroup>;
};

export default FormInputGroup;

FormInputGroup.propTypes = {
    label: PropTypes.string,
    prepend: PropTypes.element,
    append: PropTypes.element,
    violations: PropTypes.arrayOf(
        PropTypes.shape({
            field: PropTypes.string,
            message: PropTypes.string.isRequired
        })
    )
};
