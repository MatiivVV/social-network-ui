import React from "react";
import {InputGroupText} from "reactstrap";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";

const InputGroupIcon = ({icon, ...rest} = {}) => {
    return <InputGroupText {...rest}>
        <FontAwesomeIcon icon={icon}/>
    </InputGroupText>;
};

export default InputGroupIcon;

InputGroupIcon.propTypes = {
    icon: PropTypes.object
};
