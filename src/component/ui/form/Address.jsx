import React, {Component} from "react";
import {Input} from "reactstrap";
import PropTypes from "prop-types";
import {existViolationsByField, getEventAsObject} from "../../../util/formUtil";
import FormInputGroup from "./FormInputGroup";
import InputGroupIcon from "./InputGroupIcon";

class Address extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        let {name, value, onChange} = this.props;
        onChange({
            target: {
                name: name,
                value: {
                    ...value,
                    ...getEventAsObject(e)
                }
            }
        });
    }

    render() {
        let {value, label, icon, countryPlaceholder, cityPlaceholder, streetPlaceholder, violations, ...rest} = this.props;
        return <FormInputGroup label={label}
                               prepend={<InputGroupIcon icon={icon}/>}
                               violations={violations}
                               {...rest}>
            <Input type="text"
                   name="country"
                   value={value.country || ""}
                   placeholder={countryPlaceholder}
                   onChange={this.handleChange}
                   invalid={existViolationsByField(violations, "country")}/>
            <Input type="text"
                   name="city"
                   value={value.city || ""}
                   placeholder={cityPlaceholder}
                   onChange={this.handleChange}
                   invalid={existViolationsByField(violations, "city")}/>
            <Input type="text"
                   name="street"
                   value={value.street || ""}
                   placeholder={streetPlaceholder}
                   onChange={this.handleChange}
                   invalid={existViolationsByField(violations, "street")}/>
        </FormInputGroup>;
    }
}

export default Address;

Address.propTypes = {
    value: PropTypes.shape({
        country: PropTypes.string,
        city: PropTypes.string,
        street: PropTypes.string
    }).isRequired,
    label: PropTypes.string,
    icon: PropTypes.object,
    countryPlaceholder: PropTypes.string,
    cityPlaceholder: PropTypes.string,
    streetPlaceholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    violations: PropTypes.arrayOf(
        PropTypes.shape({
            field: PropTypes.string,
            message: PropTypes.string.isRequired
        }))
};

Address.defaultProps = {
    value: {},
    violations: []
};
