import React from "react";
import {Button, Spinner} from "reactstrap";
import PropTypes from "prop-types";

const ProgressButton = ({active, children, ...rest} = {}) => {
    return <Button {...rest}>
        {active && <Spinner size="sm" color="light" className="mr-1"/>}
        {children}
    </Button>;
};

export default ProgressButton;

ProgressButton.propTypes = {
    active: PropTypes.bool.isRequired
};
