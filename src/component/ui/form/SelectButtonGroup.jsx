import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonGroup, FormGroup} from "reactstrap";

class SelectButtonGroup extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(buttonValue) {
        let {name, value, onChange} = this.props;
        if (!(value.length === 1 && !buttonValue.selected)) {
            onChange({
                target: {
                    name: name,
                    value: buttonValue.selected ? [
                        ...value,
                        buttonValue.value
                    ] : value.filter(v => v !== buttonValue.value)
                }
            });
        }
    }

    render() {
        let {value, buttons, ...rest} = this.props;
        return <FormGroup {...rest}>
            <ButtonGroup>
                {buttons.map(button => {
                    let selected = value.filter(v => v === button.value).length > 0;
                    return <Button
                        key={button.value}
                        color="primary"
                        outline={!selected}
                        onClick={() => this.handleChange({
                            value: button.value,
                            selected: !selected
                        })}>
                        {button.label}
                    </Button>
                })}
            </ButtonGroup>
        </FormGroup>;
    }
}

export default SelectButtonGroup;

SelectButtonGroup.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.arrayOf(PropTypes.string.isRequired),
    buttons: PropTypes.arrayOf(PropTypes.shape({
            value: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired
        })
    ).isRequired,
    onChange: PropTypes.func.isRequired
};

SelectButtonGroup.defaultProps = {
    values: [],
    buttons: []
};
