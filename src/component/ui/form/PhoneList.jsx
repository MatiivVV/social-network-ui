import React, {Component} from "react";
import {Button, CustomInput, Input, Label} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";
import {faPhone, faPlus, faTimes} from "@fortawesome/free-solid-svg-icons/index";
import FormInputGroup from "./FormInputGroup";
import InputGroupIcon from "./InputGroupIcon";
import {existViolationsByField, getEventAsObject, getViolationsByField} from "../../../util/formUtil";
import PropTypes from "prop-types";

const PHONE_NUMBER_REGEXP = /^([+]?[0-9]{1,2}?[ -]?)?(\(?[0-9]{3}\)?[ -]?)?[0-9]{3}[ -]?[0-9]{2}[ -]?[0-9]{2}$/;
const INITIAL_STATE = {
    number: "",
    type: "PERSONAL",
};

class PhoneList extends Component {
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.state = INITIAL_STATE;
    }

    handleDelete(index) {
        let {name, value, onChange} = this.props;
        onChange({
            target: {
                name: name,
                value: value.filter((phone, i) => i !== index)
            }
        });
    }

    handleChange(e) {
        e.persist();
        this.setState(prevState => ({
            ...prevState,
            ...getEventAsObject(e)
        }));
    }

    handleAdd() {
        let {name, value, onChange, onInvalid} = this.props;
        let {state} = this;
        if (state.number && PHONE_NUMBER_REGEXP.test(state.number)) {
            onChange({
                target: {
                    name: name,
                    value: [
                        ...value,
                        state
                    ]
                }
            });
            this.setState(INITIAL_STATE);
            return;
        }
        onInvalid();
    }

    render() {
        let {value, label, numberPlaceholder, options, addLabel, violations, ...rest} = this.props;
        return <div {...rest}>
            <Label>
                {label}
            </Label>
            {value.map((phone, index) =>
                <FormInputGroup key={index}
                                prepend={<InputGroupIcon icon={faPhone}/>}
                                append={
                                    <Button color="danger" onClick={() => this.handleDelete(index)}>
                                        <FontAwesomeIcon icon={faTimes}/>
                                    </Button>}
                                violations={getViolationsByField(violations, `[${index}]`)}>
                    <Input type="text"
                           name={`[${index}].number`}
                           value={value[index].number}
                           invalid={existViolationsByField(violations, `[${index}].number`)}
                           disabled/>
                    <CustomInput type="select"
                                 id={`[${index}].type`}
                                 name={`[${index}].type`}
                                 value={value[index].type}
                                 className="col-3"
                                 invalid={existViolationsByField(violations, `[${index}].type`)}
                                 disabled>
                        {options.map(option =>
                            <option key={option.value} value={option.value}>
                                {option.title}
                            </option>
                        )}
                    </CustomInput>
                </FormInputGroup>
            )}
            <FormInputGroup prepend={<InputGroupIcon icon={faPhone}/>}
                            append={
                                <Button color="success" onClick={this.handleAdd}>
                                    <FontAwesomeIcon icon={faPlus}/>
                                    {" " + addLabel}
                                </Button>}>
                <Input type="text"
                       name="number"
                       value={this.state.number}
                       placeholder={numberPlaceholder}
                       onChange={this.handleChange}/>
                <CustomInput type="select"
                             id="type"
                             name="type"
                             value={this.state.type}
                             onChange={this.handleChange}
                             className="col-3">
                    {options.map(option =>
                        <option key={option.value} value={option.value}>
                            {option.title}
                        </option>
                    )}
                </CustomInput>
            </FormInputGroup>
        </div>;
    }
}

export default PhoneList;

PhoneList.propTypes = {
    value: PropTypes.arrayOf(
        PropTypes.shape({
            number: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired
        })
    ),
    label: PropTypes.string,
    numberPlaceholder: PropTypes.string,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired
        })
    ),
    onChange: PropTypes.func.isRequired,
    addLabel: PropTypes.string.isRequired,
    onInvalid: PropTypes.func.isRequired,
    violations: PropTypes.arrayOf(
        PropTypes.shape({
            field: PropTypes.string,
            message: PropTypes.string.isRequired
        })
    )
};

PhoneList.defaultProps = {
    value: [],
    violations: []
};
