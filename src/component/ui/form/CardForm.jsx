import React from "react";
import {Card, CardBody, CardHeader, Collapse, Form} from "reactstrap";
import PropTypes from "prop-types";

const CardForm = ({children, header, collapsible = false, collapsed = false, ...rest} = {}) => {
    let form = <CardBody>
        <Form>
            {children}
        </Form>
    </CardBody>;
    return <Card {...rest}>
        {header && <CardHeader>
            {header}
        </CardHeader>}
        {collapsible && <Collapse isOpen={!collapsed}>
            {form}
        </Collapse>}
        {!collapsible && form}
    </Card>;
};

export default CardForm;

CardForm.propTypes = {
    header: PropTypes.element.isRequired,
    collapsible: PropTypes.bool,
    collapsed: PropTypes.bool
};
