import React from "react";
import PropTypes from "prop-types";
import DatePicker from "react-datepicker/es";
import lang from "../../../constant/domain/lang";

const DATE_FORMAT = "yyyy-MM-dd";

const DateInput = ({name, value, invalid, onChange, openToDate, locale = lang.EN.locale, ...rest} = {}) => {
    return <div className={`form-control-wrapper ${invalid ? `is-invalid` : ``}`} {...rest}>
        <DatePicker className={`form-control ${invalid ? `is-invalid` : ``}`}
                    name={name}
                    showYearDropdown
                    selected={value ? Date.parse(value) : null}
                    onChange={date => onChange({
                        target: {
                            name: name,
                            value: date ? date.toISOString().slice(0, 10) : null
                        }
                    })}
                    dateFormat={DATE_FORMAT}
                    openToDate={value ? Date.parse(value) : Date.parse(openToDate)}
                    locale={locale}/>
    </div>;
};

export default DateInput;

DateInput.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    invalid: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    openToDate: PropTypes.string.isRequired,
    locale: PropTypes.string
};
