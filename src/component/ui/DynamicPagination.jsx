import React from "react";
import {Pagination, PaginationItem, PaginationLink} from "reactstrap";
import PropTypes from "prop-types";

const DynamicPagination = ({page, totalPages, visiblePages, onSelectPage, ...rest} = {}) => {
    let pageLinkCount = Math.min(visiblePages, totalPages);
    let firstPage;
    if (page < Math.round(pageLinkCount / 2)) {
        firstPage = 1
    } else if (totalPages - page + 1 <= Math.round(pageLinkCount / 2)) {
        firstPage = totalPages - pageLinkCount + 1
    } else {
        firstPage = page - Math.round(pageLinkCount / 2) + 1
    }

    function* pageGenerator() {
        for (let i = firstPage; i < firstPage + pageLinkCount; i++) {
            yield i;
        }
    }

    let pageItems = [...pageGenerator()];
    return <Pagination listClassName="justify-content-end" {...rest}>
        <PaginationItem disabled={page === 1}>
            <PaginationLink first onClick={() => onSelectPage(1)}/>
        </PaginationItem>
        <PaginationItem disabled={page === 1}>
            <PaginationLink previous onClick={() => onSelectPage(page - 1)}/>
        </PaginationItem>
        {pageItems.map(number =>
            <PaginationItem key={number} active={page === number}>
                <PaginationLink onClick={() => onSelectPage(number)}>
                    {number}
                </PaginationLink>
            </PaginationItem>)}
        <PaginationItem disabled={page === totalPages}>
            <PaginationLink next onClick={() => onSelectPage(page + 1)}/>
        </PaginationItem>
        <PaginationItem disabled={page === totalPages}>
            <PaginationLink last onClick={() => onSelectPage(totalPages)}/>
        </PaginationItem>
    </Pagination>;
};

export default DynamicPagination;

DynamicPagination.propTypes = {
    page: PropTypes.number.isRequired,
    totalPages: PropTypes.number.isRequired,
    visiblePages: PropTypes.number.isRequired,
    onSelectPage: PropTypes.func.isRequired
};
