import React from "react";
import {Button, Modal, ModalBody, ModalFooter} from "reactstrap";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus, faTimes} from "@fortawesome/free-solid-svg-icons";

const AcceptModal = ({active = false, onToggle, onAccept, title, acceptTitle, cancelTitle, ...rest} = {}) => {
    return <Modal isOpen={active} toggle={onToggle} {...rest}>
        <ModalBody>
            {title}
        </ModalBody>
        <ModalFooter>
            <Button color="primary"
                    onClick={onAccept}
                    className="mr-2">
                <FontAwesomeIcon icon={faPlus}/>
                {" " + acceptTitle}
            </Button>
            <Button color="secondary"
                    onClick={onToggle}>
                <FontAwesomeIcon icon={faTimes}/>
                {" " + cancelTitle}
            </Button>
        </ModalFooter>
    </Modal>;
};

export default AcceptModal;

AcceptModal.propTypes = {
    active: PropTypes.bool.isRequired,
    onToggle: PropTypes.func.isRequired,
    onAccept: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    acceptTitle: PropTypes.string.isRequired,
    cancelTitle: PropTypes.string.isRequired
};
