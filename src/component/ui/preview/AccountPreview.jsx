import React from "react";
import {Badge, Media} from "reactstrap";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import route from "../../../constant/route";
import * as imageApi from "../../../api/imageApi";

const AccountPreview = ({children, account, descriptor, ...rest} = {}) => {
    return <Link to={route.ACCOUNT(account.id)}
                 className="list-group-item list-group-item-action"
                 {...rest}>
        <Media>
            <img
                src={!account.deleted && account.imageId ? imageApi.getImageUrl(account.imageId) : "/image/missingAccountImage.jpg"}
                className="m-1 rounded-circle img-preview"
                alt=""/>
            <Media body className="ml-2">
                <h4 className="mb-1">
                    <FontAwesomeIcon icon={faUser}/>
                    {account.firstName && " " + account.firstName}
                    {account.lastName && " " + account.lastName}
                    {descriptor && " "}
                    {descriptor && <Badge color="secondary">
                        {descriptor}
                    </Badge>}
                </h4>
                {!account.deleted && <p>
                    {account.country && account.country}
                    {account.country && account.city && " "}
                    {account.city && account.city}
                </p>}
            </Media>
            {children}
        </Media>
    </Link>;
};

export default AccountPreview;

AccountPreview.propTypes = {
    account: PropTypes.shape({
        id: PropTypes.number,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        country: PropTypes.string,
        city: PropTypes.string,
        imageId: PropTypes.string
    }).isRequired,
    descriptor: PropTypes.element
};
