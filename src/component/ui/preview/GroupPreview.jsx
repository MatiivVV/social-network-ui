import React from "react";
import {Badge, Media} from "reactstrap";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUsers} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import route from "../../../constant/route";
import * as imageApi from "../../../api/imageApi";

const GroupPreview = ({children, group, descriptor, ...rest} = {}) => {
    return <Link to={route.GROUP(group.id)}
                 className="list-group-item list-group-item-action"
                 {...rest}>
        <Media>
            <img
                src={!group.deleted && group.imageId ? imageApi.getImageUrl(group.imageId) : "/image/missingGroupImage.jpg"}
                className="m-1 rounded-circle img-preview"
                alt=""/>
            <Media body className="ml-2">
                <h4 className="mb-1">
                    <FontAwesomeIcon icon={faUsers}/>
                    {group.name && " " + group.name}
                    {descriptor && " "}
                    {descriptor && <Badge color="secondary">
                        {descriptor}
                    </Badge>}
                </h4>
                {!group.deleted && group.description && <p>{group.description}</p>}
            </Media>
            {children}
        </Media>
    </Link>;
};

export default GroupPreview;

GroupPreview.propTypes = {
    group: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        description: PropTypes.string,
        imageId: PropTypes.string,
        deleted: PropTypes.bool
    }).isRequired,
    descriptor: PropTypes.element
};
