import React from "react";
import {Badge, Media} from "reactstrap";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import route from "../../../constant/route";
import * as imageApi from "../../../api/imageApi";

const ChatPreview = ({children, id, chat, ...rest} = {}) => {
    return <Link to={route.ACCOUNT_CHAT(id, chat.opponentId)}
                 className="list-group-item list-group-item-action"
                 {...rest}>
        <Media>
            <img
                src={!chat.opponentDeleted && chat.opponentImageId ? imageApi.getImageUrl(chat.opponentImageId) : "/image/missingAccountImage.jpg"}
                className="m-1 rounded-circle img-preview"
                alt=""/>
            <Media body className="ml-2">
                <h4 className="mb-1">
                    <FontAwesomeIcon icon={faUser}/>
                    {chat.opponentFirstName && " " + chat.opponentFirstName}
                    {chat.opponentLastName && " " + chat.opponentLastName}
                    {chat.newMessageCount > 0 && " "}
                    {chat.newMessageCount > 0 && <Badge color="secondary">
                        {chat.newMessageCount}
                    </Badge>}
                </h4>
                {!chat.opponentDeleted && <p>
                    {chat.lastMessage && chat.lastMessage}
                </p>}
            </Media>
            {children}
        </Media>
    </Link>;
};

export default ChatPreview;

ChatPreview.propTypes = {
    id: PropTypes.number.isRequired,
    chat: PropTypes.shape({
        opponentId: PropTypes.number,
        opponentFirstName: PropTypes.string,
        opponentLastName: PropTypes.string,
        opponentImageId: PropTypes.string,
        opponentDeleted: PropTypes.bool,
        lastMessage: PropTypes.string,
        newMessageCount: PropTypes.number
    }).isRequired
};
