import GroupCreate from "./GroupCreate";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from "react-router-dom";
import {changeFormByEventAction} from "../../../action/form/changeFormAction";
import {createGroupAction} from "../../../action/group/createGroupAction";
import formType from "../../../action/form/formType";


const mapStateToProps = state => ({
    form: state.forms[formType.GROUP_CREATE]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.GROUP_CREATE, e)),
    onCreateGroup: (form, image, history) => dispatch(createGroupAction(form, image, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(GroupCreate)));
