import React, {Component} from "react";
import {Button, Col, Container, CustomInput, FormFeedback, FormGroup, Input, Row} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faPlus, faUsers} from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import FormInputGroup from "../../ui/form/FormInputGroup";
import ProgressButton from "../../ui/form/ProgressButton";
import CardForm from "../../ui/form/CardForm";
import InputGroupIcon from "../../ui/form/InputGroupIcon";
import {existViolationsByField, getViolationsByField} from "../../../util/formUtil";
import {FORM_INITIAL_STATE} from "../../../initialState";
import AcceptModal from "../../ui/AcceptModal";

class GroupCreate extends Component {
    constructor(props) {
        super(props);
        this.handleToggleForm = this.handleToggleForm.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.handleCreateGroup = this.handleCreateGroup.bind(this);
        this.image = React.createRef();
        this.state = {
            formCollapsed: false,
            modalActive: false
        };
    }

    handleToggleForm() {
        this.setState(state => ({
            formCollapsed: !state.formCollapsed
        }));
    }

    handleToggleModal() {
        this.setState(state => ({
            modalActive: !state.modalActive
        }));
    }

    handleCreateGroup() {
        let {form, onCreateGroup, history} = this.props;
        this.handleToggleModal();
        onCreateGroup(form, this.image.current.files[0], history);
    }

    render() {
        let {form, onChange, t} = this.props;
        let {formCollapsed, modalActive} = this.state;
        return <Container className="mb-3">
            <Row className="justify-content-center align-items-center">
                <Col lg="8">
                    <CardForm collapsible={true}
                              collapsed={formCollapsed}
                              header={<Button color="link" onClick={this.handleToggleForm}>
                                  <h4>{t("title.group.create")}</h4>
                              </Button>}
                              className="mt-3">
                        <FormInputGroup label={t("title.group.name")}
                                        prepend={<InputGroupIcon icon={faUsers}/>}
                                        violations={getViolationsByField(form._violations, "name")}>
                            <Input type="text"
                                   name="name"
                                   value={form.name || ""}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "name")}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.group.image")}
                                        prepend={<InputGroupIcon icon={faImage}/>}>
                            <CustomInput type="file"
                                         name="image"
                                         id="image"
                                         label={t("title.browse")}
                                         innerRef={this.image}
                                         accept="image/jpeg, image/jpg, image/png, image/gif"
                                         invalid={existViolationsByField(form._violations, "image")}/>
                            {getViolationsByField(form._violations, "image")
                                .map((violation, index) => <FormFeedback key={index}>
                                    {violation.message}
                                </FormFeedback>)}
                        </FormInputGroup>
                        <FormInputGroup label={t("title.group.description")}
                                        violations={getViolationsByField(form._violations, "description")}>
                            <Input type="textarea"
                                   name="description"
                                   value={form.description || ""}
                                   rows="6"
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "description")}/>
                        </FormInputGroup>
                        <FormGroup>
                            <ProgressButton color="primary"
                                            active={form._submitting}
                                            onClick={this.handleToggleModal}>
                                <FontAwesomeIcon icon={faPlus}/>
                                {" " + t("title.group.create")}
                            </ProgressButton>
                        </FormGroup>
                    </CardForm>
                    <AcceptModal active={modalActive}
                                 onToggle={this.handleToggleModal}
                                 onAccept={this.handleCreateGroup}
                                 title={t("message.performAction")}
                                 acceptTitle={t("title.accept")}
                                 cancelTitle={t("title.cancel")}/>
                </Col>
            </Row>
        </Container>;
    }
}

export default GroupCreate;

GroupCreate.propTypes = {
    form: PropTypes.shape({
        name: PropTypes.string,
        description: PropTypes.string
    }),
    onChange: PropTypes.func.isRequired,
    onCreateGroup: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    i18n: PropTypes.object.isRequired
};

GroupCreate.defaultProps = {
    form: FORM_INITIAL_STATE
};
