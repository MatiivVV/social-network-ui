import React, {Component} from "react";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../initialState";
import {Button, Card, CardBody, CardText, CardTitle, Col, Container, ListGroup, Row, Spinner} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserEdit, faUserPlus, faUsers} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import route from "../../constant/route";
import WriteGroupMessageContainer from "./message/WriteGroupMessageContainer";
import GroupMessageListContainer from "./message/GroupMessageListContainer";
import * as imageApi from "../../api/imageApi";

const MESSAGE_VISIBLE_PAGES = 5;
const MESSAGE_PAGE_SIZE = 5;

class Group extends Component {
    constructor(props) {
        super(props);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
    }

    componentDidMount() {
        this.handleChangeLocation();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleChangeLocation();
        }
    }

    handleChangeLocation() {
        let {onFetch, match, history} = this.props;
        onFetch(match.params.id, history, MESSAGE_PAGE_SIZE);
    }

    render() {
        let {group, onCreateGroupRequest, t} = this.props;
        return group._fetching ?
            <div className="text-center mt-3">
                <Spinner color="primary"/>
            </div> :
            <Container fluid={true} className="mb-3">
                <Row className="mt-3">
                    <Col md="3" className="position-fixed d-none d-md-block">
                        <Card>
                            <img className="card-img rounded-0 img-thumbnail"
                                 src={!group.deleted && group.imageId ? imageApi.getImageUrl(group.imageId) : "/image/missingGroupImage.jpg"}
                                 alt=""/>
                            <ListGroup flush={true}>
                                {!group.related && !group.deleted && <Button color="primary"
                                                                             className="btn-block btn-sm rounded-0"
                                                                             onClick={() => onCreateGroupRequest(group.id)}>
                                    <FontAwesomeIcon icon={faUserPlus}/>
                                    {" " + t("title.group.join")}
                                </Button>}
                                {group.editable && <Link to={route.GROUP_EDIT(group.id)}
                                                         className="list-group-item list-group-item-action">
                                    <FontAwesomeIcon icon={faUserEdit}/>
                                    {" " + t("title.group.edit")}
                                </Link>}
                                {!group.deleted && <Link to={route.GROUP_MEMBERS(group.id)}
                                                         className="list-group-item list-group-item-action">
                                    <FontAwesomeIcon icon={faUsers}/>
                                    {" " + t("title.group.members")}
                                </Link>}
                            </ListGroup>
                        </Card>
                    </Col>
                    <Col md={{size: "", offset: 3}}>
                        <Card>
                            <CardBody>
                                <CardTitle>
                                    <h5>
                                        <FontAwesomeIcon icon={faUsers}/>
                                        {group.name && " " + group.name}
                                    </h5>
                                </CardTitle>
                                {!group.deleted && <div>
                                    {group.description && <CardText>
                                        {group.description}
                                    </CardText>}
                                </div>}
                            </CardBody>
                        </Card>
                        {!group.deleted && group.messageUsable
                        && <WriteGroupMessageContainer recipient={group.id}
                                                       writeTitle={t("title.message.write")}
                                                       browseTitle={t("title.browse")}
                                                       sendTitle={t("title.message.send")}
                                                       collapsed={true}
                                                       className="mt-3"/>}
                        {!group.deleted && group.messageUsable
                        && <GroupMessageListContainer recipient={group.id}
                                                      editable={group.editable}
                                                      visiblePages={MESSAGE_VISIBLE_PAGES}
                                                      pageSize={MESSAGE_PAGE_SIZE}
                                                      messagesTitle={t("title.messages")}
                                                      postedAtTitle={t("title.message.postedAt")}
                                                      deleteTitle={t("title.delete")}
                                                      className="mt-3"/>}
                    </Col>
                </Row>
            </Container>;
    }
}

export default Group;

Group.propTypes = {
    group: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        imageId: PropTypes.string,
        description: PropTypes.string,
        related: PropTypes.bool,
        messageUsable: PropTypes.bool,
        editable: PropTypes.bool,
        deleted: PropTypes.bool
    }),
    onFetch: PropTypes.func.isRequired,
    onCreateGroupRequest: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
};

Group.defaultProps = {
    group: FORM_INITIAL_STATE
};
