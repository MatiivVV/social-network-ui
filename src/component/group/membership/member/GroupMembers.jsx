import React, {Component} from "react";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../../../initialState";
import {Button, Container, ListGroup, Spinner} from "reactstrap";
import DynamicPagination from "../../../ui/DynamicPagination";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";
import {faUserCog, faUserPlus, faUserTimes} from "@fortawesome/free-solid-svg-icons/index";
import SelectButtonGroup from "../../../ui/form/SelectButtonGroup";
import role from "../../../../constant/domain/role";
import {getEventAsObject} from "../../../../util/formUtil";
import Form from "reactstrap/es/Form";
import AccountPreview from "../../../ui/preview/AccountPreview";
import {Link} from "react-router-dom";
import route from "../../../../constant/route";

const VISIBLE_PAGES = 5;
const DEFAULT_FORM = {
    ...FORM_INITIAL_STATE,
    roles: [role.USER.value],
    page: 1,
    size: 5
};

class GroupMembers extends Component {
    constructor(props) {
        super(props);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    handleRefresh(content) {
        let {form, onRefresh, match, history} = this.props;
        let actualForm = {
            ...DEFAULT_FORM,
            ...form,
            ...content
        };
        onRefresh(match.params.id, actualForm.roles, actualForm.page, actualForm.size, history);
    }

    componentDidMount() {
        this.handleRefresh({page: 1});
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleRefresh({page: 1});
        }
    }

    render() {
        let {form, onEditMemberRole, onDeleteMember, t, history} = this.props;
        let content = form._results.content;
        return <Container className="mb-3">
            {form._submitting ?
                <div className="text-center mt-3">
                    <Spinner color="primary"/>
                </div> :
                <div className="mt-3">
                    <p className="font-weight-light mt-3 d-inline">
                        {t("message.resultsFound", {
                            postProcess: "interval",
                            count: form._results.totalElements
                        })}
                        {form.editable && <Link to={route.GROUP_MEMBERSHIPS(form.id)}
                                                className="btn btn-primary btn-sm mx-2">
                            {t("title.requests")}
                        </Link>}
                    </p>
                    {form._results.totalPages > 1 && <DynamicPagination
                        page={form._results.page}
                        totalPages={form._results.totalPages}
                        visiblePages={VISIBLE_PAGES}
                        onSelectPage={page => this.handleRefresh({page: page})}
                        size="sm"
                        className="d-inline-flex float-right"
                    />}
                    <Form inline={true} className="mt-3">
                        <SelectButtonGroup name="roles"
                                           value={form.roles}
                                           buttons={Object.values(role)
                                               .map(role => ({
                                                   value: role.value,
                                                   label: t(role.title)
                                               }))}
                                           onChange={e => this.handleRefresh(getEventAsObject(e))}
                                           className="mb-1 mr-2"/>
                    </Form>
                    <ListGroup flush={true} className="mt-3 border-top border-bottom">
                        {content && content.map(member => <AccountPreview key={member.id}
                                                                          account={member}
                                                                          descriptor={member.role === role.ADMIN.value ?
                                                                              <FontAwesomeIcon icon={faUserCog}/> :
                                                                              null}>
                            {form.editable && <Button color="primary"
                                                      onClick={e => {
                                                          e.preventDefault();
                                                          onEditMemberRole(
                                                              form.id,
                                                              member.id,
                                                              member.role === role.ADMIN.value ?
                                                                  role.USER.value :
                                                                  role.ADMIN.value,
                                                              history
                                                          )
                                                      }}
                                                      className="m-1">
                                {member.role === role.USER.value && <span>
                                    <FontAwesomeIcon icon={faUserCog}/>
                                    {" " + t("title.account.makeAdmin")}
                                </span>}
                                {member.role === role.ADMIN.value && <span>
                                    <FontAwesomeIcon icon={faUserPlus}/>
                                    {" " + t("title.account.makeUser")}
                                </span>}
                            </Button>}
                            {form.editable && <Button color="primary"
                                                      onClick={e => {
                                                          e.preventDefault();
                                                          onDeleteMember(form.id, member.id, history)
                                                      }}
                                                      className="m-1">
                                <FontAwesomeIcon icon={faUserTimes}/>
                                {" " + t("title.delete")}
                            </Button>}
                        </AccountPreview>)}
                    </ListGroup>
                </div>}
        </Container>;
    }
}

export default GroupMembers;

GroupMembers.propTypes = {
    form: PropTypes.shape({
        id: PropTypes.string,
        roles: PropTypes.arrayOf(PropTypes.string.isRequired),
        page: PropTypes.number,
        size: PropTypes.number,
        editable: PropTypes.bool
    }),
    onRefresh: PropTypes.func.isRequired,
    onEditMemberRole: PropTypes.func.isRequired,
    onDeleteMember: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

GroupMembers.defaultProps = {
    form: DEFAULT_FORM
};
