import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../../action/form/formType";
import GroupMembers from "./GroupMembers";
import {getGroupMembersAction} from "../../../../action/group/membership/member/getGroupMembersAction";
import {deleteGroupMemberAction} from "../../../../action/group/membership/member/deleteGroupMemberAction";
import {editGroupMemberRoleAction} from "../../../../action/group/membership/member/editGroupMemberRoleAction";

const mapStateToProps = state => ({
    form: state.forms[formType.GROUP_MEMBERS]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, roles, page, size, history) => dispatch(getGroupMembersAction(id, roles, page, size, history)),
    onEditMemberRole: (id, memberId, role, history) => dispatch(editGroupMemberRoleAction(id, memberId, role, history)),
    onDeleteMember: (id, memberId, history) => dispatch(deleteGroupMemberAction(id, memberId, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(GroupMembers)));
