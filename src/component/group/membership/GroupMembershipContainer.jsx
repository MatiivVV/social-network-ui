import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../action/form/formType";
import GroupMembership from "./GroupMembership";
import {getGroupMembershipsAction} from "../../../action/group/membership/getGroupMembershipsAction";
import {editGroupMembershipStatusAction} from "../../../action/group/membership/editGroupMembershipStatusAction";

const mapStateToProps = state => ({
    form: state.forms[formType.GROUP_MEMBERSHIPS]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, statuses, page, size, history) => dispatch(getGroupMembershipsAction(id, statuses, page, size, history)),
    onEditMembershipStatus: (id, memberId, status, history) => dispatch(editGroupMembershipStatusAction(id, memberId, status, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(GroupMembership)));
