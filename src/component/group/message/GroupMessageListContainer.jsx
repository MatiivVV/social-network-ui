import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import formType from "../../../action/form/formType";
import PublicMessageList from "../../ui/message/public/PublicMessageList";
import {getGroupMessagesAction} from "../../../action/group/message/getGroupMessagesAction";
import {deleteGroupMessageAction} from "../../../action/group/message/deleteGroupMessageAction";

const mapStateToProps = state => ({
    form: state.forms[formType.GROUP_MESSAGES]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, page = 1, size) => dispatch(getGroupMessagesAction(id, page, size)),
    onDeleteMessage: (id, groupId, authorId) => dispatch(deleteGroupMessageAction(id, groupId, authorId))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PublicMessageList));
