import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import {changeFormByEventAction} from "../../../action/form/changeFormAction";
import WritePublicMessage from "../../ui/message/public/WritePublicMessage";
import formType from "../../../action/form/formType";
import {sendGroupMessageAction} from "../../../action/group/message/sendGroupMessageAction";

const mapStateToProps = state => ({
    form: state.forms[formType.GROUP_MESSAGE]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.GROUP_MESSAGE, e)),
    onSend: (id, form, image) => dispatch(sendGroupMessageAction(id, form, image))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WritePublicMessage));
