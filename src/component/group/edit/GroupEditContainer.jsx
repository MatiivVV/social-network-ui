import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {changeFormByEventAction} from "../../../action/form/changeFormAction";
import formType from "../../../action/form/formType";
import {withRouter} from 'react-router-dom'
import GroupEdit from "./GroupEdit";
import {getGroupForEditAction} from "../../../action/group/edit/getGroupForEditAction";
import {editGroupInfoAction} from "../../../action/group/edit/editGroupInfoAction";
import {editGroupDeletedAction} from "../../../action/group/edit/editGroupDeletedAction";
import {editGroupImageAction} from "../../../action/group/edit/editGroupImageAction";

const mapStateToProps = state => ({
    form: state.forms[formType.GROUP_EDIT]
});

const mapDispatchToProps = dispatch => ({
    onFetch: (id, history) => dispatch(getGroupForEditAction(id, history)),
    onChange: e => dispatch(changeFormByEventAction(formType.GROUP_EDIT, e)),
    onEditInfo: (id, info) => dispatch(editGroupInfoAction(id, info)),
    onEditDeleted: (id, deleted) => dispatch(editGroupDeletedAction(id, deleted)),
    onEditImage: (id, image) => dispatch(editGroupImageAction(id, image))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(GroupEdit)));
