import React, {Component} from "react";
import {Button, Col, Container, CustomInput, FormFeedback, FormGroup, Input, Row, Spinner} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faPen, faPlus, faTimes, faUsers} from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import FormInputGroup from "../../ui/form/FormInputGroup";
import ProgressButton from "../../ui/form/ProgressButton";
import CardForm from "../../ui/form/CardForm";
import InputGroupIcon from "../../ui/form/InputGroupIcon";
import {existViolationsByField, getViolationsByField} from "../../../util/formUtil";
import {FORM_INITIAL_STATE} from "../../../initialState";
import AcceptModal from "../../ui/AcceptModal";

class GroupEdit extends Component {
    constructor(props) {
        super(props);
        this.handleFetch = this.handleFetch.bind(this);
        this.handleToggleInfoForm = this.handleToggleInfoForm.bind(this);
        this.handleToggleImageForm = this.handleToggleImageForm.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.image = React.createRef();
        this.state = {
            infoFormCollapsed: false,
            imageFormCollapsed: true,
            modalActive: false,
            action: editAction.EDIT_INFO
        };
    }

    componentDidMount() {
        this.handleFetch();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleFetch();
        }
    }

    handleFetch() {
        let {onFetch, match, history} = this.props;
        onFetch(match.params.id, history);
    }

    handleToggleInfoForm() {
        this.setState(state => ({
            infoFormCollapsed: !state.infoFormCollapsed
        }));
    }

    handleToggleImageForm() {
        this.setState(state => ({
            imageFormCollapsed: !state.imageFormCollapsed
        }));
    }

    handleToggleModal(action) {
        this.setState(state => ({
            modalActive: !state.modalActive,
            action: action ? action : state.action
        }));
    }

    handleEdit() {
        let {
            match,
            form,
            onEditInfo,
            onEditDeleted,
            onEditImage,
        } = this.props;
        let id = match.params.id;
        let action = this.state.action;
        this.handleToggleModal();
        switch (action) {
            case editAction.EDIT_INFO:
                onEditInfo(id, form);
                break;
            case editAction.EDIT_DELETED:
                onEditDeleted(id, !form.deleted);
                break;
            case editAction.EDIT_IMAGE:
                onEditImage(id, this.image.current.files[0]);
                break;
            default:
                throw new Error(`Illegal group edit action - ${action}`)
        }
    }

    render() {
        let {form, onChange, t} = this.props;
        let {infoFormCollapsed, imageFormCollapsed, modalActive, part} = this.state;
        return form._fetching ?
            <div className="text-center mt-3">
                <Spinner color="primary"/>
            </div> :
            <Container className="mb-3">
                <Row className="justify-content-center align-items-center">
                    <Col lg="8">
                        <CardForm collapsible={true}
                                  collapsed={infoFormCollapsed}
                                  header={<Button color="link" onClick={this.handleToggleInfoForm}>
                                      <h4>{t("title.group.editInfo")}</h4>
                                  </Button>}
                                  className="mt-3">
                            <FormInputGroup label={t("title.group.name")}
                                            prepend={<InputGroupIcon icon={faUsers}/>}
                                            violations={getViolationsByField(form._violations, "name")}>
                                <Input type="text"
                                       name="name"
                                       value={form.name || ""}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "name")}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.group.description")}
                                            violations={getViolationsByField(form._violations, "description")}>
                                <Input type="textarea"
                                       name="description"
                                       value={form.description || ""}
                                       rows="6"
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "description")}/>
                            </FormInputGroup>
                            <FormGroup>
                                <ProgressButton color="primary"
                                                active={form._submitting && part === editAction.EDIT_INFO}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_INFO)}
                                                className="m-1">
                                    <FontAwesomeIcon icon={faPen}/>
                                    {" " + t("title.group.editInfo")}
                                </ProgressButton>
                                <ProgressButton color="primary"
                                                active={form._submitting && part === editAction.EDIT_DELETED}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_DELETED)}
                                                className="m-1">
                                    {form.deleted && <span>
                                            <FontAwesomeIcon icon={faPlus}/>
                                        {" " + t("title.group.restore")}
                                        </span>}
                                    {!form.deleted && <span>
                                            <FontAwesomeIcon icon={faTimes}/>
                                        {" " + t("title.group.delete")}
                                        </span>}
                                </ProgressButton>
                            </FormGroup>
                        </CardForm>
                        <CardForm collapsible={true}
                                  collapsed={imageFormCollapsed}
                                  header={<Button color="link" onClick={this.handleToggleImageForm}>
                                      <h4>{t("title.group.editImage")}</h4>
                                  </Button>}
                                  className="mt-3">
                            <FormInputGroup label={t("title.group.image")}
                                            prepend={<InputGroupIcon icon={faImage}/>}>
                                <CustomInput type="file"
                                             name="image"
                                             id="image"
                                             label={t("title.browse")}
                                             innerRef={this.image}
                                             accept="image/jpeg, image/jpg, image/png, image/gif"
                                             invalid={existViolationsByField(form._violations, "image")}/>
                                {getViolationsByField(form._violations, "image")
                                    .map((violation, index) => <FormFeedback key={index}>
                                        {violation.message}
                                    </FormFeedback>)}
                            </FormInputGroup>
                            <FormGroup>
                                <ProgressButton color="primary"
                                                active={form._submitting && part === editAction.EDIT_IMAGE}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_IMAGE)}>
                                    <FontAwesomeIcon icon={faPen}/>
                                    {" " + t("title.group.editImage")}
                                </ProgressButton>
                            </FormGroup>
                        </CardForm>
                        <AcceptModal active={modalActive}
                                     onToggle={this.handleToggleModal}
                                     onAccept={this.handleEdit}
                                     title={t("message.performAction")}
                                     acceptTitle={t("title.accept")}
                                     cancelTitle={t("title.cancel")}/>
                    </Col>
                </Row>
            </Container>;
    }
}

export default GroupEdit;

GroupEdit.propTypes = {
    form: PropTypes.shape({
        name: PropTypes.string,
        description: PropTypes.string,
        deleted: PropTypes.bool
    }),
    onFetch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onEditInfo: PropTypes.func.isRequired,
    onEditDeleted: PropTypes.func.isRequired,
    onEditImage: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired
};

GroupEdit.defaultProps = {
    form: FORM_INITIAL_STATE
};

const editAction = {
    EDIT_INFO: "EDIT_INFO",
    EDIT_IMAGE: "EDIT_IMAGE",
    EDIT_DELETED: "EDIT_DELETED"
};
