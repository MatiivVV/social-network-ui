import Group from "./Group";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../action/form/formType";
import {getGroupAction} from "../../action/group/getGroupAction";
import {createGroupMembershipAction} from "../../action/group/membership/createGroupMembershipAction";

const mapStateToProps = state => ({
    group: state.forms[formType.GROUP]
});

const mapDispatchToProps = dispatch => ({
    onFetch: (id, history, messagePageSize) => dispatch(getGroupAction(id, history, messagePageSize)),
    onCreateGroupRequest: id => dispatch(createGroupMembershipAction(id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Group)));
