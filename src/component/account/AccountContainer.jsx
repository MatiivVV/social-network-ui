import Account from "./Account";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import {getAccountAction} from "../../action/account/getAccountAction";
import {createAccountFriendshipAction} from "../../action/account/friendship/createAccountFriendshipAction";
import formType from "../../action/form/formType";

const mapStateToProps = state => ({
    userId: state.user.id,
    account: state.forms[formType.ACCOUNT]
});

const mapDispatchToProps = dispatch => ({
    onFetch: (id, history, messagePageSize) => dispatch(getAccountAction(id, history, messagePageSize)),
    onCreateFriendRequest: id => dispatch(createAccountFriendshipAction(id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Account)));
