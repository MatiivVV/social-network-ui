import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import formType from "../../../action/form/formType";
import {getAccountMessagesAction} from "../../../action/account/message/getAccountMessagesAction";
import PublicMessageList from "../../ui/message/public/PublicMessageList";
import {deleteAccountMessageAction} from "../../../action/account/message/deleteAccountMessageAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_MESSAGES]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, page, size) => dispatch(getAccountMessagesAction(id, page, size)),
    onDeleteMessage: (id, accountId, authorId) => dispatch(deleteAccountMessageAction(id, accountId, authorId))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PublicMessageList));
