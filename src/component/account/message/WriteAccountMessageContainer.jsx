import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import {changeFormByEventAction} from "../../../action/form/changeFormAction";
import WritePublicMessage from "../../ui/message/public/WritePublicMessage";
import {sendAccountMessageAction} from "../../../action/account/message/sendAccountMessageAction";
import formType from "../../../action/form/formType";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_MESSAGE]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.ACCOUNT_MESSAGE, e)),
    onSend: (id, form, image) => dispatch(sendAccountMessageAction(id, form, image))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WritePublicMessage));
