import React, {Component} from "react";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../initialState";
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardText,
    CardTitle,
    Col,
    Collapse,
    Container,
    ListGroup,
    Row,
    Spinner
} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAt,
    faBuilding,
    faCalendar,
    faComments,
    faHome,
    faPhone,
    faUser,
    faUserEdit,
    faUserFriends,
    faUserPlus,
    faUsers,
    faWindowMaximize
} from "@fortawesome/free-solid-svg-icons";
import {faSkype} from "@fortawesome/free-brands-svg-icons";
import {Link} from "react-router-dom";
import route from "../../constant/route";
import WriteAccountMessageContainer from "./message/WriteAccountMessageContainer";
import AccountMessageListContainer from "./message/AccountMessageListContainer";
import * as imageApi from "../../api/imageApi";

const MESSAGE_VISIBLE_PAGES = 5;
const MESSAGE_PAGE_SIZE = 5;

class Account extends Component {
    constructor(props) {
        super(props);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
        this.handleToggleOtherInfo = this.handleToggleOtherInfo.bind(this);
        this.state = {
            otherInfoCollapsed: true,
        };
    }

    componentDidMount() {
        this.handleChangeLocation();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleChangeLocation();
        }
    }

    handleChangeLocation() {
        let {onFetch, match, history} = this.props;
        onFetch(match.params.id, history, MESSAGE_PAGE_SIZE);
    }

    handleToggleOtherInfo() {
        this.setState(state => ({
            otherInfoCollapsed: !state.otherInfoCollapsed
        }));
    }

    render() {
        let {userId, account, onCreateFriendRequest, t} = this.props;
        return account._fetching ?
            <div className="text-center mt-3">
                <Spinner color="primary"/>
            </div> :
            <Container fluid={true} className="mb-3">
                <Row className="mt-3">
                    <Col md="3" className="position-fixed d-none d-md-block">
                        <Card>
                            <img className="card-img rounded-0 img-thumbnail"
                                 src={!account.deleted && account.imageId ? imageApi.getImageUrl(account.imageId) : "/image/missingAccountImage.jpg"}
                                 alt=""/>
                            <ListGroup flush={true}>
                                {!account.related && !account.deleted && <Button color="primary"
                                                                                 className="btn-block btn-sm rounded-0"
                                                                                 onClick={() => onCreateFriendRequest(account.id)}>
                                    <FontAwesomeIcon icon={faUserPlus}/>
                                    {" " + t("title.account.addToFriends")}
                                </Button>}
                                {!account.owned && !account.deleted &&
                                <Link to={route.ACCOUNT_CHAT(userId, account.id)}>
                                    <Button color="primary" className="btn-block btn-sm rounded-0 mt-1">
                                        <FontAwesomeIcon icon={faComments}/>
                                        {" " + t("title.account.startChat")}
                                    </Button>
                                </Link>}
                                {account.editable && <Link to={route.ACCOUNT_EDIT(account.id)}
                                                           className="list-group-item list-group-item-action">
                                    <FontAwesomeIcon icon={faUserEdit}/>
                                    {" " + t("title.account.edit")}
                                </Link>}
                                {!account.deleted && <Link to={route.ACCOUNT_FRIENDS(account.id)}
                                                           className="list-group-item list-group-item-action">
                                    <FontAwesomeIcon icon={faUserFriends}/>
                                    {" " + t("title.account.friends")}
                                </Link>}
                                {!account.deleted && <Link to={route.ACCOUNT_GROUPS(account.id)}
                                                           className="list-group-item list-group-item-action">
                                    <FontAwesomeIcon icon={faUsers}/>
                                    {" " + t("title.account.groups")}
                                </Link>}
                                {account.owned && <Link to={route.ACCOUNT_CHATS(account.id)}
                                                        className="list-group-item list-group-item-action">
                                    <FontAwesomeIcon icon={faComments}/>
                                    {" " + t("title.account.chats")}
                                    {account.newMessageCount > 0 && " "}
                                    {account.newMessageCount > 0 && <Badge color="secondary">
                                        {account.newMessageCount}
                                    </Badge>}
                                </Link>}
                            </ListGroup>
                        </Card>
                    </Col>
                    <Col md={{size: "", offset: 3}}>
                        <Card>
                            <CardBody>
                                <CardTitle>
                                    <h5>
                                        <FontAwesomeIcon icon={faUser}/>
                                        {account.firstName && " " + account.firstName}
                                        {account.lastName && " " + account.lastName}
                                        {account.paternalName && " " + account.paternalName}
                                    </h5>
                                </CardTitle>
                                {!account.deleted && <div>
                                    {account.birthday && <CardText>
                                        <FontAwesomeIcon icon={faCalendar}/>
                                        {" " + account.birthday}
                                    </CardText>}
                                    {account.personalAddress && <CardText>
                                        <FontAwesomeIcon icon={faHome}/>
                                        {account.personalAddress.country && " " + account.personalAddress.country}
                                        {account.personalAddress.city && " " + account.personalAddress.city}
                                        {account.personalAddress.street && " " + account.personalAddress.street}
                                    </CardText>}
                                    <Button color="link" size="sm" onClick={this.handleToggleOtherInfo}>
                                        <FontAwesomeIcon icon={faWindowMaximize}/>
                                        {" " + t("title.account.otherInfo")}
                                    </Button>
                                    <Collapse isOpen={!this.state.otherInfoCollapsed}>
                                        <hr/>
                                        {account.businessAddress && <CardText>
                                            <FontAwesomeIcon icon={faBuilding}/>
                                            {account.businessAddress.country && " " + account.businessAddress.country}
                                            {account.businessAddress.city && " " + account.businessAddress.city}
                                            {account.businessAddress.street && " " + account.businessAddress.street}
                                        </CardText>}
                                        {account.phones && account.phones.map((phone, i) => phone.number && phone.type &&
                                            <CardText key={i}>
                                                <FontAwesomeIcon icon={faPhone}/>
                                                {" " + phone.number + " "}
                                                {phone.type === "PERSONAL" && <FontAwesomeIcon icon={faHome}/>}
                                                {phone.type === "BUSINESS" && <FontAwesomeIcon icon={faBuilding}/>}
                                            </CardText>)}
                                        {account.email && <CardText>
                                            <FontAwesomeIcon icon={faAt}/>
                                            {" " + account.email}
                                        </CardText>}
                                        {account.skype && <CardText>
                                            <FontAwesomeIcon icon={faSkype}/>
                                            {" " + account.skype}
                                        </CardText>}
                                        {account.additionalInfo && <CardText>
                                            {account.additionalInfo}
                                        </CardText>}
                                    </Collapse>
                                </div>}
                            </CardBody>
                        </Card>
                        {!account.deleted && <WriteAccountMessageContainer recipient={account.id}
                                                                           writeTitle={t("title.message.write")}
                                                                           browseTitle={t("title.browse")}
                                                                           sendTitle={t("title.message.send")}
                                                                           collapsed={true}
                                                                           className="mt-3"/>}
                        {!account.deleted && <AccountMessageListContainer recipient={account.id}
                                                                          editable={account.editable}
                                                                          visiblePages={MESSAGE_VISIBLE_PAGES}
                                                                          pageSize={MESSAGE_PAGE_SIZE}
                                                                          messagesTitle={t("title.messages")}
                                                                          postedAtTitle={t("title.message.postedAt")}
                                                                          deleteTitle={t("title.delete")}
                                                                          className="mt-3"/>}
                    </Col>
                </Row>
            </Container>;
    }
}

export default Account;

Account.propTypes = {
    userId: PropTypes.number,
    account: PropTypes.shape({
        id: PropTypes.number,
        emailAddress: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        paternalName: PropTypes.string,
        birthday: PropTypes.string,
        imageId: PropTypes.string,
        personalAddress: PropTypes.shape({
            country: PropTypes.string,
            city: PropTypes.string,
            street: PropTypes.string
        }),
        businessAddress: PropTypes.shape({
            country: PropTypes.string,
            city: PropTypes.string,
            street: PropTypes.string
        }),
        phones: PropTypes.arrayOf(PropTypes.shape({
            number: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired
        })),
        icq: PropTypes.string,
        skype: PropTypes.string,
        additionalInfo: PropTypes.string,
        owned: PropTypes.bool,
        related: PropTypes.bool,
        editable: PropTypes.bool,
        deleted: PropTypes.bool,
        newMessageCount: PropTypes.number
    }),
    onFetch: PropTypes.func.isRequired,
    onCreateFriendRequest: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
};

Account.defaultProps = {
    account: FORM_INITIAL_STATE
};
