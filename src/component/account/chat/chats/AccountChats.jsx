import React, {Component} from "react";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../../../initialState";
import {Button, Container, Spinner} from "reactstrap";
import ListGroup from "reactstrap/es/ListGroup";
import DynamicPagination from "../../../ui/DynamicPagination";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";
import {faCommentSlash} from "@fortawesome/free-solid-svg-icons/index";
import ChatPreview from "../../../ui/preview/ChatPreview";

const VISIBLE_PAGES = 5;
const DEFAULT_FORM = {
    ...FORM_INITIAL_STATE,
    page: 1,
    size: 5
};

class AccountChats extends Component {
    constructor(props) {
        super(props);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    handleRefresh(content) {
        let {form, onRefresh, match, history} = this.props;
        let actualForm = {
            ...DEFAULT_FORM,
            ...form,
            ...content
        };
        onRefresh(match.params.id, actualForm.page, actualForm.size, history);
    }

    componentDidMount() {
        this.handleRefresh({page: 1});
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleRefresh({page: 1});
        }
    }

    render() {
        let {form, onDeleteChat, t, history} = this.props;
        let content = form._results.content;
        return <Container className="mb-3">
            {form._submitting ?
                <div className="text-center mt-3">
                    <Spinner color="primary"/>
                </div> :
                <div className="mt-3">
                    <p className="font-weight-light mt-3 d-inline">
                        {t("message.resultsFound", {
                            postProcess: "interval",
                            count: form._results.totalElements
                        })}
                    </p>
                    {form._results.totalPages > 1 && <DynamicPagination
                        page={form._results.page}
                        totalPages={form._results.totalPages}
                        visiblePages={VISIBLE_PAGES}
                        onSelectPage={page => this.handleRefresh({page: page})}
                        size="sm"
                        className="d-inline-flex float-right"
                    />}
                    <ListGroup flush={true} className="mt-3 border-top border-bottom">
                        {content && content.map(chat => <ChatPreview key={chat.id} id={form.id} chat={chat}>
                            <Button color="primary"
                                    onClick={e => {
                                        e.preventDefault();
                                        onDeleteChat(form.id, chat.opponentId, history)
                                    }}
                                    className="m-1">
                                <FontAwesomeIcon icon={faCommentSlash}/>
                                {" " + t("title.delete")}
                            </Button>
                        </ChatPreview>)}
                    </ListGroup>
                </div>}
        </Container>;
    }
}

export default AccountChats;

AccountChats.propTypes = {
    form: PropTypes.shape({
        id: PropTypes.string,
        page: PropTypes.number,
        size: PropTypes.number
    }),
    onRefresh: PropTypes.func.isRequired,
    onDeleteChat: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

AccountChats.defaultProps = {
    form: DEFAULT_FORM
};
