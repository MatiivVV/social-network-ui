import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../../action/form/formType";
import AccountChats from "./AccountChats";
import {getAccountChatsAction} from "../../../../action/account/chat/getAccountChatsAction";
import {deleteAccountChatAction} from "../../../../action/account/chat/deleteAccountChatAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_CHATS],
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, page, size, history) => dispatch(getAccountChatsAction(id, page, size, history)),
    onDeleteChat: (id, opponentId, history) => dispatch(deleteAccountChatAction(id, opponentId, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountChats)));
