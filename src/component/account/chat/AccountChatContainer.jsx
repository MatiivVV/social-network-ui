import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../action/form/formType";
import {getAccountChatAction} from "../../../action/account/chat/getAccountChatAction";
import AccountChat from "./AccountChat";

const mapStateToProps = state => ({
    chat: state.forms[formType.ACCOUNT_CHAT]
});

const mapDispatchToProps = dispatch => ({
    onFetch: (id, opponentId, history, messagePageSize) => dispatch(getAccountChatAction(id, opponentId, history, messagePageSize))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountChat)));
