import React, {Component} from "react";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../../initialState";
import {Col, Row, Spinner} from "reactstrap";
import AccountChatMessagesContainer from "./message/AccountChatMessagesContainer";

const MESSAGE_VISIBLE_PAGES = 5;
const MESSAGE_PAGE_SIZE = 50;

class AccountChat extends Component {
    constructor(props) {
        super(props);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
    }

    componentDidMount() {
        this.handleChangeLocation();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleChangeLocation();
        }
    }

    handleChangeLocation() {
        let {onFetch, match, history} = this.props;
        let {id, opponentId} = match.params;
        onFetch(id, opponentId, history, MESSAGE_PAGE_SIZE);
    }

    render() {
        let {chat, t} = this.props;
        return !chat.account || !chat.opponent || chat._fetching ?
            <div className="text-center mt-3">
                <Spinner color="primary"/>
            </div> :
            <Row className="justify-content-center align-items-center">
                <Col lg="7">
                    <AccountChatMessagesContainer account={chat.account}
                                                  opponent={chat.opponent}
                                                  visiblePages={MESSAGE_VISIBLE_PAGES}
                                                  pageSize={MESSAGE_PAGE_SIZE}
                                                  messagesTitle={t("title.messages")}
                                                  writeTitle={t("title.message.write")}
                                                  browseTitle={t("title.browse")}
                                                  sendTitle={t("title.message.send")}
                                                  className="mt-3"/>
                </Col>
            </Row>;
    }
}

export default AccountChat;

AccountChat.propTypes = {
    chat: PropTypes.shape({
        account: PropTypes.shape({
            id: PropTypes.number.isRequired,
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired,
            imageId: PropTypes.string,
            deleted: PropTypes.bool.isRequired
        }),
        opponent: PropTypes.shape({
            id: PropTypes.number.isRequired,
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired,
            imageId: PropTypes.string,
            deleted: PropTypes.bool.isRequired
        })
    }),
    onFetch: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
};

AccountChat.defaultProps = {
    chat: FORM_INITIAL_STATE
};
