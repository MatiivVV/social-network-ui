import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import formType from "../../../../action/form/formType";
import {withTranslation} from "react-i18next";
import ChatMessages from "../../../ui/message/chat/ChatMessages";
import {changeFormByEventAction} from "../../../../action/form/changeFormAction";
import {getChatMessagesAction} from "../../../../action/account/chat/message/getChatMessagesAction";
import {sendChatMessageAction} from "../../../../action/account/chat/message/sendChatMessageAction";
import {readAccountChatAction} from "../../../../action/account/chat/readAccountChatAction";

const mapStateToProps = state => ({
    messagesForm: state.forms[formType.ACCOUNT_CHAT_MESSAGES],
    messageForm: state.forms[formType.ACCOUNT_CHAT_MESSAGE]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, opponentId, page, size) => dispatch(getChatMessagesAction(id, opponentId, page, size)),
    onChangeMessage: e => dispatch(changeFormByEventAction(formType.ACCOUNT_CHAT_MESSAGE, e)),
    onSendMessage: (id, authorId, form, image) => dispatch(sendChatMessageAction(id, authorId, form, image)),
    onRead: (id, opponentId) => dispatch(readAccountChatAction(id, opponentId))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ChatMessages)));
