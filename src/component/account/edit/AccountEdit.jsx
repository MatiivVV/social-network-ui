import React, {Component} from "react";
import {Button, Col, Collapse, Container, CustomInput, FormFeedback, FormGroup, Input, Row, Spinner} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAt,
    faBuilding,
    faCalendar,
    faFile,
    faFileCode,
    faFileDownload,
    faFileUpload,
    faGlobe,
    faHome,
    faImage,
    faSignOutAlt,
    faTimes,
    faUnlock,
    faUser,
    faUserCog,
    faUserEdit,
    faUserPlus
} from "@fortawesome/free-solid-svg-icons";
import {faSkype} from "@fortawesome/free-brands-svg-icons";
import PropTypes from "prop-types";
import Address from "../../ui/form/Address";
import FormInputGroup from "../../ui/form/FormInputGroup";
import ProgressButton from "../../ui/form/ProgressButton";
import CardForm from "../../ui/form/CardForm";
import DateInput from "../../ui/form/DateInput";
import PhoneList from "../../ui/form/PhoneList";
import InputGroupIcon from "../../ui/form/InputGroupIcon";
import notificationType from "../../../action/notification/notificationType";
import {existViolationsByField, getViolationsByField} from "../../../util/formUtil";
import {FORM_INITIAL_STATE} from "../../../initialState";
import contactType from "../../../constant/domain/contactType";
import AcceptModal from "../../ui/AcceptModal";
import role from "../../../constant/domain/role";
import lang from "../../../constant/domain/lang";
import mediaType from "../../../api/mediaType";

class AccountEdit extends Component {
    constructor(props) {
        super(props);
        this.handleFetch = this.handleFetch.bind(this);
        this.handleToggleInfoForm = this.handleToggleInfoForm.bind(this);
        this.handleToggleInfoFormXML = this.handleToggleInfoFormXML.bind(this);
        this.handleToggleImageForm = this.handleToggleImageForm.bind(this);
        this.handleTogglePasswordForm = this.handleTogglePasswordForm.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.image = React.createRef();
        this.xml = React.createRef();
        this.state = {
            infoFormCollapsed: false,
            infoFormXMLCollapsed: true,
            imageFormCollapsed: true,
            passwordFormCollapsed: true,
            modalActive: false,
            action: editAction.EDIT_INFO,
            fetchMediaType: mediaType.JSON
        };
    }

    componentDidMount() {
        this.handleFetch(mediaType.JSON);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleFetch(mediaType.JSON);
        }
    }

    handleFetch(fetchMediaType) {
        this.setState({
            fetchMediaType: fetchMediaType
        });
        let {onFetch, match, history} = this.props;
        onFetch(match.params.id, history, fetchMediaType);
    }

    handleToggleInfoForm() {
        this.setState(state => ({
            infoFormCollapsed: !state.infoFormCollapsed
        }));
    }

    handleToggleInfoFormXML() {
        this.setState(state => ({
            infoFormXMLCollapsed: !state.infoFormXMLCollapsed
        }));
    }

    handleToggleImageForm() {
        this.setState(state => ({
            imageFormCollapsed: !state.imageFormCollapsed
        }));
    }

    handleTogglePasswordForm() {
        this.setState(state => ({
            passwordFormCollapsed: !state.passwordFormCollapsed
        }));
    }

    handleToggleModal(action) {
        this.setState(state => ({
            modalActive: !state.modalActive,
            action: action ? action : state.action
        }));
    }

    handleEdit() {
        let {
            match,
            form,
            onEditInfo,
            onEditDeleted,
            onEditRole,
            onResetSession,
            onEditImage,
            onEditPassword,
            onNotification,
            history
        } = this.props;
        let id = match.params.id;
        let action = this.state.action;
        this.handleToggleModal();
        switch (action) {
            case editAction.EDIT_INFO:
                onEditInfo(id, form, mediaType.JSON);
                break;
            case editAction.EDIT_INFO_XML:
                let file = this.xml.current.files[0];
                if (file) {
                    onEditInfo(id, file, mediaType.XML, history)
                } else {
                    onNotification(notificationType.ERROR, "message.missingFile");
                }
                break;
            case editAction.EDIT_DELETED:
                onEditDeleted(id, !form.deleted);
                break;
            case editAction.EDIT_ROLE:
                onEditRole(id, form.role === role.USER.value ? role.ADMIN.value : role.USER.value);
                break;
            case editAction.RESET_SESSION:
                onResetSession(id);
                break;
            case editAction.EDIT_IMAGE:
                onEditImage(id, this.image.current.files[0]);
                break;
            case editAction.EDIT_PASSWORD:
                if (form.confirmPassword === form.rawPassword) {
                    onEditPassword(id, form.oldPassword, form.rawPassword);
                } else {
                    onNotification(notificationType.ERROR, "message.passwordMismatch");
                }
                break;
            default:
                throw new Error(`Illegal account edit action - ${action}`)
        }
    }

    render() {
        let {form, onChange, onNotification, t, i18n} = this.props;
        let {
            infoFormCollapsed,
            imageFormCollapsed,
            infoFormXMLCollapsed,
            passwordFormCollapsed,
            modalActive,
            action,
            fetchMediaType
        } = this.state;
        return form._fetching && fetchMediaType === mediaType.JSON ?
            <div className="text-center mt-3">
                <Spinner color="primary"/>
            </div> :
            <Container className="mb-3">
                <Row className="justify-content-center align-items-center">
                    <Col lg="8">
                        <CardForm collapsible={true}
                                  collapsed={infoFormCollapsed}
                                  header={<Button color="link" onClick={this.handleToggleInfoForm}>
                                      <h4>{t("title.account.editInfo")}</h4>
                                  </Button>}
                                  className="mt-3">
                            <FormInputGroup label={t("title.account.emailAddress")}
                                            prepend={<InputGroupIcon icon={faAt}/>}
                                            violations={getViolationsByField(form._violations, "emailAddress")}>
                                <Input type="email"
                                       name="emailAddress"
                                       value={form.emailAddress || ""}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "emailAddress")}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.account.name")}
                                            prepend={<InputGroupIcon icon={faUser}/>}
                                            violations={[
                                                ...getViolationsByField(form._violations, "firstName"),
                                                ...getViolationsByField(form._violations, "lastName"),
                                                ...getViolationsByField(form._violations, "paternalName")
                                            ]}>
                                <Input type="text"
                                       name="firstName"
                                       value={form.firstName || ""}
                                       placeholder={t("title.account.firstName")}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "firstName")}/>
                                <Input type="text"
                                       name="lastName"
                                       value={form.lastName || ""}
                                       placeholder={t("title.account.lastName")}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "lastName")}/>
                                <Input type="text"
                                       name="paternalName"
                                       value={form.paternalName || ""}
                                       placeholder={t("title.account.paternalName")}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "paternalName")}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.account.birthday")}
                                            prepend={<InputGroupIcon icon={faCalendar}/>}
                                            violations={getViolationsByField(form._violations, "birthday")}>
                                <DateInput name="birthday"
                                           value={form.birthday}
                                           onChange={onChange}
                                           invalid={existViolationsByField(form._violations, "birthday")}
                                           openToDate="1990-01-01"
                                           locale={i18n.language}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.account.locale")}
                                            prepend={<InputGroupIcon icon={faGlobe}/>}
                                            violations={getViolationsByField(form._violations, "locale")}>
                                <CustomInput type="select"
                                             id="locale"
                                             name="locale"
                                             value={form.locale || lang.EN}
                                             onChange={onChange}
                                             invalid={existViolationsByField(form._violations, "locale")}>
                                    {Object.values(lang).map(lang =>
                                        <option key={lang.value} value={lang.value}>
                                            {t(lang.title)}
                                        </option>
                                    )}
                                </CustomInput>
                            </FormInputGroup>
                            <Address name="personalAddress"
                                     value={form.personalAddress}
                                     label={t("title.account.address")}
                                     icon={faHome}
                                     countryPlaceholder={t("title.account.country")}
                                     cityPlaceholder={t("title.account.city")}
                                     streetPlaceholder={t("title.account.street")}
                                     onChange={onChange}
                                     violations={getViolationsByField(form._violations, "personalAddress")}/>
                            <Address name="businessAddress"
                                     value={form.businessAddress}
                                     icon={faBuilding}
                                     countryPlaceholder={t("title.account.country")}
                                     cityPlaceholder={t("title.account.city")}
                                     streetPlaceholder={t("title.account.street")}
                                     onChange={onChange}
                                     violations={getViolationsByField(form._violations, "personalAddress")}/>
                            <PhoneList name="phones"
                                       label={t("title.account.phones")}
                                       value={form.phones}
                                       onChange={onChange}
                                       addLabel={t("title.account.phone.add")}
                                       onInvalid={() => onNotification(notificationType.ERROR, "message.invalidPhone")}
                                       numberPlaceholder={t("message.enterPhone")}
                                       options={Object.values(contactType).map(type => ({
                                           value: type.value,
                                           title: t(type.title)
                                       }))}
                                       violations={getViolationsByField(form._violations, "phones")}/>
                            <FormInputGroup label={t("title.account.skype")}
                                            prepend={<InputGroupIcon icon={faSkype}/>}
                                            violations={getViolationsByField(form._violations, "skype")}>
                                <Input type="text"
                                       name="skype"
                                       value={form.skype || ""}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "skype")}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.account.additionalInfo")}
                                            violations={getViolationsByField(form._violations, "additionalInfo")}>
                                <Input type="textarea"
                                       name="additionalInfo"
                                       value={form.additionalInfo || ""}
                                       rows="6"
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "additionalInfo")}/>
                            </FormInputGroup>
                            <FormGroup>
                                <ProgressButton color="primary"
                                                active={form._submitting && action === editAction.EDIT_INFO}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_INFO)}
                                                className="m-1">
                                    <FontAwesomeIcon icon={faUserEdit}/>
                                    {" " + t("title.account.editInfo")}
                                </ProgressButton>
                                <ProgressButton color="primary"
                                                active={form._submitting && action === editAction.EDIT_DELETED}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_DELETED)}
                                                className="m-1">
                                    {form.deleted && <span>
                                            <FontAwesomeIcon icon={faUserPlus}/>
                                        {" " + t("title.account.restore")}
                                        </span>}
                                    {!form.deleted && <span>
                                            <FontAwesomeIcon icon={faTimes}/>
                                        {" " + t("title.account.delete")}
                                        </span>}
                                </ProgressButton>
                                {form.administered && <ProgressButton color="primary"
                                                                      active={form._submitting && action === editAction.EDIT_ROLE}
                                                                      onClick={() => this.handleToggleModal(editAction.EDIT_ROLE)}
                                                                      className="m-1">
                                    {form.role === role.USER.value && <span>
                                            <FontAwesomeIcon icon={faUserCog}/>
                                        {" " + t("title.account.makeAdmin")}
                                        </span>}
                                    {form.role === role.ADMIN.value && <span>
                                            <FontAwesomeIcon icon={faUserPlus}/>
                                        {" " + t("title.account.makeUser")}
                                        </span>}
                                </ProgressButton>}
                            </FormGroup>
                            <Button color="link" size="sm" onClick={this.handleToggleInfoFormXML}>
                                <FontAwesomeIcon icon={faFileCode}/>
                                {" " + t("title.account.workWithXML")}
                            </Button>
                            <Collapse isOpen={!infoFormXMLCollapsed}>
                                <hr/>
                                <FormInputGroup prepend={<InputGroupIcon icon={faFile}/>}>
                                    <CustomInput type="file"
                                                 name="xml"
                                                 id="xml"
                                                 label={t("title.browse")}
                                                 innerRef={this.xml}
                                                 accept={mediaType.XML}
                                                 invalid={existViolationsByField(form._violations, "xml")}/>
                                    {getViolationsByField(form._violations, "xml")
                                        .map((violation, index) => <FormFeedback key={index}>
                                            {violation.message}
                                        </FormFeedback>)}
                                </FormInputGroup>
                                <FormGroup>
                                    <ProgressButton color="primary"
                                                    active={form._fetching && fetchMediaType === mediaType.XML}
                                                    onClick={() => this.handleFetch(mediaType.XML)}
                                                    className="m-1">
                                        <FontAwesomeIcon icon={faFileDownload}/>
                                        {" " + t("title.account.exportXML")}
                                    </ProgressButton>


                                    <ProgressButton color="primary"
                                                    active={form._submitting && action === editAction.EDIT_INFO_XML}
                                                    onClick={() => this.handleToggleModal(editAction.EDIT_INFO_XML)}
                                                    className="m-1">
                                        <FontAwesomeIcon icon={faFileUpload}/>
                                        {" " + t("title.account.importXML")}
                                    </ProgressButton>
                                </FormGroup>
                            </Collapse>
                        </CardForm>
                        <CardForm collapsible={true}
                                  collapsed={imageFormCollapsed}
                                  header={<Button color="link" onClick={this.handleToggleImageForm}>
                                      <h4>{t("title.account.editImage")}</h4>
                                  </Button>}
                                  className="mt-3">
                            <FormInputGroup label={t("title.account.image")}
                                            prepend={<InputGroupIcon icon={faImage}/>}>
                                <CustomInput type="file"
                                             name="image"
                                             id="image"
                                             label={t("title.browse")}
                                             innerRef={this.image}
                                             accept="image/jpeg, image/jpg, image/png, image/gif"
                                             invalid={existViolationsByField(form._violations, "image")}/>
                                {getViolationsByField(form._violations, "image")
                                    .map((violation, index) => <FormFeedback key={index}>
                                        {violation.message}
                                    </FormFeedback>)}
                            </FormInputGroup>
                            <FormGroup>
                                <ProgressButton color="primary"
                                                active={form._submitting && action === editAction.EDIT_IMAGE}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_IMAGE)}>
                                    <FontAwesomeIcon icon={faUserEdit}/>
                                    {" " + t("title.account.editImage")}
                                </ProgressButton>
                            </FormGroup>
                        </CardForm>
                        <CardForm collapsible={true}
                                  collapsed={passwordFormCollapsed}
                                  header={<Button color="link" onClick={this.handleTogglePasswordForm}>
                                      <h4>{t("title.account.editPassword")}</h4>
                                  </Button>}
                                  className="mt-3">
                            <FormInputGroup label={t("title.account.oldPassword")}
                                            prepend={<InputGroupIcon icon={faUnlock}/>}
                                            violations={getViolationsByField(form._violations, "oldPassword")}>
                                <Input type="password"
                                       name="oldPassword"
                                       value={form.oldPassword || ""}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "oldPassword")}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.account.password")}
                                            prepend={<InputGroupIcon icon={faUnlock}/>}
                                            violations={getViolationsByField(form._violations, "rawPassword")}>
                                <Input type="password"
                                       name="rawPassword"
                                       value={form.rawPassword || ""}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "rawPassword")}/>
                            </FormInputGroup>
                            <FormInputGroup label={t("title.account.confirmPassword")}
                                            prepend={<InputGroupIcon icon={faUnlock}/>}
                                            violations={form.confirmPassword === form.rawPassword ? [] :
                                                [{field: "confirmPassword", message: t("message.passwordMismatch")}]}>
                                <Input type="password"
                                       name="confirmPassword"
                                       value={form.confirmPassword || ""}
                                       onChange={onChange}
                                       invalid={form.confirmPassword !== form.rawPassword}/>
                            </FormInputGroup>
                            <FormGroup>
                                <ProgressButton color="primary"
                                                active={form._submitting && action === editAction.EDIT_PASSWORD}
                                                onClick={() => this.handleToggleModal(editAction.EDIT_PASSWORD)}
                                                className="m-1">
                                    <FontAwesomeIcon icon={faUserEdit}/>
                                    {" " + t("title.account.editPassword")}
                                </ProgressButton>
                                <ProgressButton color="primary"
                                                active={form._submitting && action === editAction.RESET_SESSION}
                                                onClick={() => this.handleToggleModal(editAction.RESET_SESSION)}
                                                className="m-1">
                                    <FontAwesomeIcon icon={faSignOutAlt}/>
                                    {" " + t("title.account.resetSession")}
                                </ProgressButton>
                            </FormGroup>
                        </CardForm>
                        <AcceptModal active={modalActive}
                                     onToggle={this.handleToggleModal}
                                     onAccept={this.handleEdit}
                                     title={t("message.performAction")}
                                     acceptTitle={t("title.accept")}
                                     cancelTitle={t("title.cancel")}/>
                    </Col>
                </Row>
            </Container>;
    }
}

export default AccountEdit;

AccountEdit.propTypes = {
    form: PropTypes.shape({
        emailAddress: PropTypes.string,
        oldPassword: PropTypes.string,
        rawPassword: PropTypes.string,
        confirmPassword: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        paternalName: PropTypes.string,
        birthday: PropTypes.string,
        locale: PropTypes.string,
        personalAddress: PropTypes.shape({
            country: PropTypes.string,
            city: PropTypes.string,
            street: PropTypes.string
        }),
        businessAddress: PropTypes.shape({
            country: PropTypes.string,
            city: PropTypes.string,
            street: PropTypes.string
        }),
        phones: PropTypes.arrayOf(PropTypes.shape({
            number: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired
        })),
        icq: PropTypes.string,
        skype: PropTypes.string,
        additionalInfo: PropTypes.string,
        administered: PropTypes.bool,
        deleted: PropTypes.bool
    }),
    onFetch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onNotification: PropTypes.func.isRequired,
    onEditInfo: PropTypes.func.isRequired,
    onEditDeleted: PropTypes.func.isRequired,
    onEditRole: PropTypes.func.isRequired,
    onResetSession: PropTypes.func.isRequired,
    onEditImage: PropTypes.func.isRequired,
    onEditPassword: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    i18n: PropTypes.object.isRequired
};

AccountEdit.defaultProps = {
    form: FORM_INITIAL_STATE
};

const editAction = {
    EDIT_INFO: "EDIT_INFO",
    EDIT_INFO_XML: "EDIT_INFO_XML",
    EDIT_IMAGE: "EDIT_IMAGE",
    EDIT_PASSWORD: "EDIT_PASSWORD",
    EDIT_ROLE: "EDIT_ROLE",
    EDIT_DELETED: "EDIT_DELETED",
    RESET_SESSION: "RESET_SESSION"
};
