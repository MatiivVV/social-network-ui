import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {changeFormByEventAction} from "../../../action/form/changeFormAction";
import {raiseTimedNotificationAction} from "../../../action/notification/raiseNotificationAction";
import formType from "../../../action/form/formType";
import {withRouter} from 'react-router-dom'
import {getAccountForEditAction} from "../../../action/account/edit/getAccountForEditAction";
import AccountEdit from "./AccountEdit";
import {editAccountInfoAction} from "../../../action/account/edit/editAccountInfoAction";
import {editAccountDeletedAction} from "../../../action/account/edit/editAccountDeletedAction";
import {editAccountRoleAction} from "../../../action/account/edit/editAccountoRoleAction";
import {editAccountPasswordAction} from "../../../action/account/edit/editAccountPasswordAction";
import {resetAccountSessionAction} from "../../../action/account/edit/resetAccountSessionAction";
import {editAccountImageAction} from "../../../action/account/edit/editAccountImageAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_EDIT]
});

const mapDispatchToProps = dispatch => ({
    onFetch: (id, history, accountMediaType) => dispatch(getAccountForEditAction(id, history, accountMediaType)),
    onChange: e => dispatch(changeFormByEventAction(formType.ACCOUNT_EDIT, e)),
    onNotification: (type, message) => dispatch(raiseTimedNotificationAction(type, message)),
    onEditInfo: (id, info, infoMediaType, history) => dispatch(editAccountInfoAction(id, info, infoMediaType, history)),
    onEditDeleted: (id, deleted) => dispatch(editAccountDeletedAction(id, deleted)),
    onEditRole: (id, role) => dispatch(editAccountRoleAction(id, role)),
    onResetSession: id => dispatch(resetAccountSessionAction(id)),
    onEditImage: (id, image) => dispatch(editAccountImageAction(id, image)),
    onEditPassword: (id, oldPassword, password) => dispatch(editAccountPasswordAction(id, oldPassword, password))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountEdit)));
