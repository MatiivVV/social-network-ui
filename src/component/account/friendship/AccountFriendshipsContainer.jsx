import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../action/form/formType";
import AccountFriendships from "./AccountFriendships";
import {getAccountFriendshipsAction} from "../../../action/account/friendship/getAccountFriendshipsAction";
import {deleteAccountFriendshipAction} from "../../../action/account/friendship/deleteAccountFriendshipAction";
import {editAccountFriendshipStatusAction} from "../../../action/account/friendship/editAccountFriendshipStatusAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_FRIENDSHIPS]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, types, statuses, page, size, history) => dispatch(getAccountFriendshipsAction(id, types, statuses, page, size, history)),
    onEditFriendshipStatus: (id, friendId, status, history) => dispatch(editAccountFriendshipStatusAction(id, friendId, status, history)),
    onDeleteFriendship: (id, friendId, history) => dispatch(deleteAccountFriendshipAction(id, friendId, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountFriendships)));
