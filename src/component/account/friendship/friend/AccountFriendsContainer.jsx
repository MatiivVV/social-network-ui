import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../../action/form/formType";
import AccountFriends from "./AccountFriends";
import {getAccountFriendsAction} from "../../../../action/account/friendship/friend/getAccountFriendsAction";
import {deleteAccountFriendAction} from "../../../../action/account/friendship/friend/deleteAccountFriendAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_FRIENDS]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, page, size, history) => dispatch(getAccountFriendsAction(id, page, size, history)),
    onDeleteFriend: (id, friendId, history) => dispatch(deleteAccountFriendAction(id, friendId, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountFriends)));
