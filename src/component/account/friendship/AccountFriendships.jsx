import React, {Component} from "react";
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../../initialState";
import {Button, Container, Spinner} from "reactstrap";
import ListGroup from "reactstrap/es/ListGroup";
import AccountPreview from "../../ui/preview/AccountPreview";
import DynamicPagination from "../../ui/DynamicPagination";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index";
import {faUserCheck, faUserTimes} from "@fortawesome/free-solid-svg-icons/index";
import SelectButtonGroup from "../../ui/form/SelectButtonGroup";
import status from "../../../constant/domain/status";
import {getEventAsObject} from "../../../util/formUtil";
import Form from "reactstrap/es/Form";
import friendshipType from "../../../constant/domain/friendshipType";

const VISIBLE_PAGES = 5;
const DEFAULT_FORM = {
    ...FORM_INITIAL_STATE,
    types: [friendshipType.REQUEST.value, friendshipType.RESPONSE.value],
    statuses: [status.PENDING.value],
    page: 1,
    size: 5
};

class AccountFriendships extends Component {
    constructor(props) {
        super(props);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    handleRefresh(content) {
        let {form, onRefresh, match, history} = this.props;
        let actualForm = {
            ...DEFAULT_FORM,
            ...form,
            ...content
        };
        onRefresh(match.params.id, actualForm.types, actualForm.statuses, actualForm.page, actualForm.size, history);
    }

    componentDidMount() {
        this.handleRefresh({page: 1});
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location !== this.props.location) {
            this.handleRefresh({page: 1});
        }
    }

    render() {
        let {form, onEditFriendshipStatus, onDeleteFriendship, t, history} = this.props;
        let content = form._results.content;
        return <Container className="mb-3">
            {form._submitting ?
                <div className="text-center mt-3">
                    <Spinner color="primary"/>
                </div> :
                <div className="mt-3">
                    <p className="font-weight-light mt-3 d-inline">
                        {t("message.resultsFound", {
                            postProcess: "interval",
                            count: form._results.totalElements
                        })}
                    </p>
                    {form._results.totalPages > 1 && <DynamicPagination
                        page={form._results.page}
                        totalPages={form._results.totalPages}
                        visiblePages={VISIBLE_PAGES}
                        onSelectPage={page => this.handleRefresh({page: page})}
                        size="sm"
                        className="d-inline-flex float-right"
                    />}
                    <Form inline={true} className="mt-3">
                        <SelectButtonGroup name="types"
                                           value={form.types}
                                           buttons={Object.values(friendshipType)
                                               .map(type => ({
                                                   value: type.value,
                                                   label: t(type.title)
                                               }))}
                                           onChange={e => this.handleRefresh(getEventAsObject(e))}
                                           className="mb-1 mr-2"/>
                        <SelectButtonGroup name="statuses"
                                           value={form.statuses}
                                           buttons={Object.values(status)
                                               .map(status => ({
                                                   value: status.value,
                                                   label: t(status.title)
                                               }))}
                                           onChange={e => this.handleRefresh(getEventAsObject(e))}
                                           className="mb-1 mr-2"/>
                    </Form>
                    <ListGroup flush={true} className="mt-3 border-top border-bottom">
                        {content && content.map(friendship => <AccountPreview key={friendship.id}
                                                                              account={friendship}
                                                                              descriptor={t(Object
                                                                                  .values(status)
                                                                                  .find(status => status.value === friendship.status)
                                                                                  .title)}>
                            {friendship.type === friendshipType.RESPONSE.value && friendship.status === status.PENDING.value &&
                            <Button color="primary"
                                    onClick={e => {
                                        e.preventDefault();
                                        onEditFriendshipStatus(
                                            form.id,
                                            friendship.id,
                                            status.ACCEPTED.value,
                                            history
                                        )
                                    }}
                                    className="m-1">
                                <FontAwesomeIcon icon={faUserCheck}/>
                                {" " + t("title.request.accept")}
                            </Button>}
                            {friendship.type === friendshipType.RESPONSE.value && friendship.status === status.PENDING.value &&
                            <Button color="primary"
                                    onClick={e => {
                                        e.preventDefault();
                                        onEditFriendshipStatus(
                                            form.id,
                                            friendship.id,
                                            status.DECLINED.value,
                                            history
                                        )
                                    }}
                                    className="m-1">
                                <FontAwesomeIcon icon={faUserTimes}/>
                                {" " + t("title.request.decline")}
                            </Button>}
                            {friendship.type === friendshipType.REQUEST.value && friendship.status !== status.ACCEPTED.value &&
                            <Button color="primary"
                                    onClick={e => {
                                        e.preventDefault();
                                        onDeleteFriendship(form.id, friendship.id, history)
                                    }}
                                    className="m-1">
                                <FontAwesomeIcon icon={faUserTimes}/>
                                {" " + t("title.delete")}
                            </Button>}
                        </AccountPreview>)}
                    </ListGroup>
                </div>}
        </Container>;
    }
}

export default AccountFriendships;

AccountFriendships.propTypes = {
    form: PropTypes.shape({
        id: PropTypes.string,
        types: PropTypes.arrayOf(PropTypes.string.isRequired),
        statuses: PropTypes.arrayOf(PropTypes.string.isRequired),
        page: PropTypes.number,
        size: PropTypes.number
    }),
    onRefresh: PropTypes.func.isRequired,
    onEditFriendshipStatus: PropTypes.func.isRequired,
    onDeleteFriendship: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

AccountFriendships.defaultProps = {
    form: DEFAULT_FORM
};
