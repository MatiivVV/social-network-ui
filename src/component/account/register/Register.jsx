import React, {Component} from "react";
import {Button, Col, Container, CustomInput, FormFeedback, FormGroup, Input, Row} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAt,
    faBuilding,
    faCalendar,
    faHome,
    faImage,
    faUnlock,
    faUser,
    faUserPlus
} from "@fortawesome/free-solid-svg-icons";
import {faSkype} from "@fortawesome/free-brands-svg-icons";
import PropTypes from "prop-types";
import Address from "../../ui/form/Address";
import FormInputGroup from "../../ui/form/FormInputGroup";
import ProgressButton from "../../ui/form/ProgressButton";
import CardForm from "../../ui/form/CardForm";
import DateInput from "../../ui/form/DateInput";
import PhoneList from "../../ui/form/PhoneList";
import InputGroupIcon from "../../ui/form/InputGroupIcon";
import notificationType from "../../../action/notification/notificationType";
import {existViolationsByField, getViolationsByField} from "../../../util/formUtil";
import {FORM_INITIAL_STATE} from "../../../initialState";
import contactType from "../../../constant/domain/contactType";
import AcceptModal from "../../ui/AcceptModal";

class Register extends Component {
    constructor(props) {
        super(props);
        this.handleToggleForm = this.handleToggleForm.bind(this);
        this.handleToggleModal = this.handleToggleModal.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.image = React.createRef();
        this.state = {
            formCollapsed: false,
            modalActive: false
        };
    }

    handleToggleForm() {
        this.setState(state => ({
            formCollapsed: !state.formCollapsed
        }));
    }

    handleToggleModal() {
        this.setState(state => ({
            modalActive: !state.modalActive
        }));
    }

    handleRegister() {
        let {form, onNotification, onRegister} = this.props;
        this.handleToggleModal();
        if (form.confirmPassword === form.rawPassword) {
            onRegister(form, this.image.current.files[0]);
        } else {
            onNotification(notificationType.ERROR, "message.passwordMismatch");
        }
    }

    render() {
        let {form, onChange, onNotification, t, i18n} = this.props;
        let {formCollapsed, modalActive} = this.state;
        return <Container className="mb-3">
            <Row className="justify-content-center align-items-center">
                <Col lg="8">
                    <CardForm collapsible={true}
                              collapsed={formCollapsed}
                              header={<Button color="link" onClick={this.handleToggleForm}>
                                  <h4>{t("title.account.register")}</h4>
                              </Button>}
                              className="mt-3">
                        <FormInputGroup label={t("title.account.emailAddress")}
                                        prepend={<InputGroupIcon icon={faAt}/>}
                                        violations={getViolationsByField(form._violations, "emailAddress")}>
                            <Input type="email"
                                   name="emailAddress"
                                   value={form.emailAddress || ""}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "emailAddress")}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.password")}
                                        prepend={<InputGroupIcon icon={faUnlock}/>}
                                        violations={getViolationsByField(form._violations, "rawPassword")}>
                            <Input type="password"
                                   name="rawPassword"
                                   value={form.rawPassword || ""}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "rawPassword")}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.confirmPassword")}
                                        prepend={<InputGroupIcon icon={faUnlock}/>}
                                        violations={form.confirmPassword === form.rawPassword ? [] :
                                            [{field: "confirmPassword", message: t("message.passwordMismatch")}]}>
                            <Input type="password"
                                   name="confirmPassword"
                                   value={form.confirmPassword || ""}
                                   onChange={onChange}
                                   invalid={form.confirmPassword !== form.rawPassword}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.name")}
                                        prepend={<InputGroupIcon icon={faUser}/>}
                                        violations={[
                                            ...getViolationsByField(form._violations, "firstName"),
                                            ...getViolationsByField(form._violations, "lastName"),
                                            ...getViolationsByField(form._violations, "paternalName")
                                        ]}>
                            <Input type="text"
                                   name="firstName"
                                   value={form.firstName || ""}
                                   placeholder={t("title.account.firstName")}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "firstName")}/>
                            <Input type="text"
                                   name="lastName"
                                   value={form.lastName || ""}
                                   placeholder={t("title.account.lastName")}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "lastName")}/>
                            <Input type="text"
                                   name="paternalName"
                                   value={form.paternalName || ""}
                                   placeholder={t("title.account.paternalName")}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "paternalName")}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.birthday")}
                                        prepend={<InputGroupIcon icon={faCalendar}/>}
                                        violations={getViolationsByField(form._violations, "birthday")}>
                            <DateInput name="birthday"
                                       value={form.birthday}
                                       onChange={onChange}
                                       invalid={existViolationsByField(form._violations, "birthday")}
                                       openToDate="1990-01-01"
                                       locale={i18n.language}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.image")}
                                        prepend={<InputGroupIcon icon={faImage}/>}>
                            <CustomInput type="file"
                                         name="image"
                                         id="image"
                                         label={t("title.browse")}
                                         innerRef={this.image}
                                         accept="image/jpeg, image/jpg, image/png, image/gif"
                                         invalid={existViolationsByField(form._violations, "image")}/>
                            {getViolationsByField(form._violations, "image")
                                .map((violation, index) => <FormFeedback key={index}>
                                    {violation.message}
                                </FormFeedback>)}
                        </FormInputGroup>
                        <Address name="personalAddress"
                                 value={form.personalAddress}
                                 label={t("title.account.address")}
                                 icon={faHome}
                                 countryPlaceholder={t("title.account.country")}
                                 cityPlaceholder={t("title.account.city")}
                                 streetPlaceholder={t("title.account.street")}
                                 onChange={onChange}
                                 violations={getViolationsByField(form._violations, "personalAddress")}/>
                        <Address name="businessAddress"
                                 value={form.businessAddress}
                                 icon={faBuilding}
                                 countryPlaceholder={t("title.account.country")}
                                 cityPlaceholder={t("title.account.city")}
                                 streetPlaceholder={t("title.account.street")}
                                 onChange={onChange}
                                 violations={getViolationsByField(form._violations, "personalAddress")}/>
                        <PhoneList name="phones"
                                   label={t("title.account.phones")}
                                   value={form.phones}
                                   onChange={onChange}
                                   addLabel={t("title.account.phone.add")}
                                   onInvalid={() => onNotification(notificationType.ERROR, "message.invalidPhone")}
                                   numberPlaceholder={t("message.enterPhone")}
                                   options={Object.values(contactType).map(type => ({
                                       value: type.value,
                                       title: t(type.title)
                                   }))}
                                   violations={getViolationsByField(form._violations, "phones")}/>
                        <FormInputGroup label={t("title.account.skype")}
                                        prepend={<InputGroupIcon icon={faSkype}/>}
                                        violations={getViolationsByField(form._violations, "skype")}>
                            <Input type="text"
                                   name="skype"
                                   value={form.skype || ""}
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "skype")}/>
                        </FormInputGroup>
                        <FormInputGroup label={t("title.account.additionalInfo")}
                                        violations={getViolationsByField(form._violations, "additionalInfo")}>
                            <Input type="textarea"
                                   name="additionalInfo"
                                   value={form.additionalInfo || ""}
                                   rows="6"
                                   onChange={onChange}
                                   invalid={existViolationsByField(form._violations, "additionalInfo")}/>
                        </FormInputGroup>
                        <FormGroup>
                            <ProgressButton color="primary"
                                            active={form._submitting}
                                            onClick={this.handleToggleModal}>
                                <FontAwesomeIcon icon={faUserPlus}/>
                                {" " + t("title.account.register")}
                            </ProgressButton>
                        </FormGroup>
                    </CardForm>
                    <AcceptModal active={modalActive}
                                 onToggle={this.handleToggleModal}
                                 onAccept={this.handleRegister}
                                 title={t("message.performAction")}
                                 acceptTitle={t("title.accept")}
                                 cancelTitle={t("title.cancel")}/>
                </Col>
            </Row>
        </Container>;
    }
}

export default Register;

Register.propTypes = {
    form: PropTypes.shape({
        emailAddress: PropTypes.string,
        rawPassword: PropTypes.string,
        confirmPassword: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        paternalName: PropTypes.string,
        birthday: PropTypes.string,
        personalAddress: PropTypes.shape({
            country: PropTypes.string,
            city: PropTypes.string,
            street: PropTypes.string
        }),
        businessAddress: PropTypes.shape({
            country: PropTypes.string,
            city: PropTypes.string,
            street: PropTypes.string
        }),
        phones: PropTypes.arrayOf(PropTypes.shape({
            number: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired
        })),
        icq: PropTypes.string,
        skype: PropTypes.string,
        additionalInfo: PropTypes.string
    }),
    onChange: PropTypes.func.isRequired,
    onNotification: PropTypes.func.isRequired,
    onRegister: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    i18n: PropTypes.object.isRequired
};

Register.defaultProps = {
    form: FORM_INITIAL_STATE
};
