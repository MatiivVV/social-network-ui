import Register from "./Register";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {changeFormByEventAction} from "../../../action/form/changeFormAction";
import {raiseTimedNotificationAction} from "../../../action/notification/raiseNotificationAction";
import {registerAccountAction} from "../../../action/account/registerAccountAction";
import formType from "../../../action/form/formType";


const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_REGISTER]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.ACCOUNT_REGISTER, e)),
    onNotification: (type, message) => dispatch(raiseTimedNotificationAction(type, message)),
    onRegister: (form, image) => dispatch(registerAccountAction(form, image))
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Register));
