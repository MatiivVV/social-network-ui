import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../action/form/formType";
import AccountMemberships from "./AccountMemberships";
import {getAccountMembershipsAction} from "../../../action/account/membership/getAccountMembershipsAction";
import {deleteAccountMembershipAction} from "../../../action/account/membership/deleteAccountMembershipAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_MEMBERSHIPS]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, statuses, page, size, history) => dispatch(getAccountMembershipsAction(id, statuses, page, size, history)),
    onDeleteMembership: (id, groupId, history) => dispatch(deleteAccountMembershipAction(id, groupId, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountMemberships)));
