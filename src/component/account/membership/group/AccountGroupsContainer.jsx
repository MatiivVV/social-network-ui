import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {withRouter} from 'react-router-dom'
import formType from "../../../../action/form/formType";
import AccountGroups from "./AccountGroups";
import {getAccountGroupsAction} from "../../../../action/account/membership/group/getAccountGroupsAction";
import {deleteAccountGroupAction} from "../../../../action/account/membership/group/deleteAccountGroupAction";

const mapStateToProps = state => ({
    form: state.forms[formType.ACCOUNT_GROUPS]
});

const mapDispatchToProps = dispatch => ({
    onRefresh: (id, roles, page, size, history) => dispatch(getAccountGroupsAction(id, roles, page, size, history)),
    onDeleteGroup: (id, groupId, history) => dispatch(deleteAccountGroupAction(id, groupId, history))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AccountGroups)));
