import {withTranslation} from "react-i18next";
import Error from "./Error";
import {withRouter} from 'react-router-dom'

export default withRouter(withTranslation()(Error));
