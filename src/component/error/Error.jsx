import React from "react";
import PropTypes from "prop-types";
import {Container, Jumbotron} from "reactstrap";
import errorType from "../../constant/errorType";

const Error = ({match, t}) => {
    let errors = Object.values(errorType).filter(error => error.value === match.params.error);
    return <Container className="mb-3">
        <Jumbotron className="m-3">
            <h3>{t("title.error")}</h3>
            {errors.length && <p>{t(errors[0].message)}</p>}
        </Jumbotron>
    </Container>;
};

export default Error;

Error.propTypes = {
    t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired
};
