import React, {Component} from "react";
import PropTypes from "prop-types";
import * as queryString from 'query-string';
import {FORM_INITIAL_STATE} from "../../initialState";
import {Button, Container, CustomInput, Input, Spinner} from "reactstrap";
import ListGroup from "reactstrap/es/ListGroup";
import entityType from "../../constant/domain/entityType";
import AccountPreview from "../ui/preview/AccountPreview";
import GroupPreview from "../ui/preview/GroupPreview";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import FormInputGroup from "../ui/form/FormInputGroup";
import route from "../../constant/route";
import DynamicPagination from "../ui/DynamicPagination";

const VISIBLE_PAGES = 5;
const DEFAULT_FORM = {
    ...FORM_INITIAL_STATE,
    pattern: "",
    type: entityType.ACCOUNT.value,
    page: 1,
    size: 5
};

class Search extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearch(location) {
        this.props.onSearch({
                ...DEFAULT_FORM,
                ...queryString.parse(location.search),
            }
        );
    }

    componentDidMount() {
        this.handleSearch(this.props.location);
    }

    componentDidUpdate(prevProps) {
        let {location} = this.props;
        if (prevProps.location !== location) {
            this.handleSearch(location);
        }
    }

    render() {
        let {form, onChange, location, history, t} = this.props;
        let query = queryString.parse(location.search);
        let content = form._results.content;
        return <Container className="mb-3">
            <FormInputGroup className="mt-3"
                            append={
                                <Button color="primary"
                                        onClick={() => {
                                            history.push({
                                                pathname: route.SEARCH(),
                                                search: "?" + queryString.stringify({
                                                    pattern: form.pattern,
                                                    type: form.type,
                                                    page: 1
                                                })
                                            })
                                        }}>
                                    <FontAwesomeIcon icon={faSearch}/>
                                    {" "}
                                </Button>}>
                <Input type="text"
                       name="pattern"
                       value={form.pattern}
                       placeholder={t("title.search")}
                       onChange={onChange}/>
                <CustomInput type="select"
                             id="type"
                             name="type"
                             value={form.type}
                             onChange={onChange}
                             className="col-3">
                    {Object.values(entityType).map(option =>
                        <option key={option.value} value={option.value}>
                            {t(option.title)}
                        </option>
                    )}
                </CustomInput>
            </FormInputGroup>
            {form._submitting ?
                <div className="text-center mt-3">
                    <Spinner color="primary"/>
                </div> :
                <div className="mt-3">
                    <p className="font-weight-light mt-3 d-inline">
                        {t("message.resultsFound", {
                            postProcess: "interval",
                            count: form._results.totalElements
                        })}
                    </p>
                    {form._results.totalPages > 1 && <DynamicPagination
                        page={form._results.page}
                        totalPages={form._results.totalPages}
                        visiblePages={VISIBLE_PAGES}
                        onSelectPage={page => history.push({
                            pathname: route.SEARCH(),
                            search: "?" + queryString.stringify({
                                pattern: form.pattern,
                                type: form.type,
                                page: page
                            })
                        })}
                        size="sm"
                        className="d-inline-flex float-right"
                    />}
                    <ListGroup flush={true} className="mt-3 border-top border-bottom">
                        {content && content.map(item => query.type === entityType.GROUP.value ?
                            <GroupPreview key={item.id} group={item}/> :
                            <AccountPreview key={item.id} account={item}/>
                        )}
                    </ListGroup>
                </div>}
        </Container>;
    }
}

export default Search;

Search.propTypes = {
    form: PropTypes.shape({
        pattern: PropTypes.string,
        type: PropTypes.string,
        page: PropTypes.number,
        size: PropTypes.number
    }),
    onChange: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

Search.defaultProps = {
    form: DEFAULT_FORM
};
