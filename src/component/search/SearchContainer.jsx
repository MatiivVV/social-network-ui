import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import Search from "./Search";
import {searchAction} from "../../action/search/searchAction";
import {changeFormByEventAction} from "../../action/form/changeFormAction";
import {withRouter} from 'react-router-dom'
import formType from "../../action/form/formType";


const mapStateToProps = state => ({
    form: state.forms[formType.SEARCH]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.SEARCH, e)),
    onSearch: form => dispatch(searchAction(form))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Search)));
