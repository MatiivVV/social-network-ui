import React from "react";
import {Button, Input, InputGroup, InputGroupAddon, Spinner,} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index"
import PropTypes from "prop-types";
import {FORM_INITIAL_STATE} from "../../../../initialState";
import Autosuggest from 'react-autosuggest';
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import route from "../../../../constant/route";
import * as queryString from 'query-string';
import entityType from "../../../../constant/domain/entityType";

const MIN_LENGTH = 2;
const DEFAULT_FORM = {
    ...FORM_INITIAL_STATE,
    pattern: "",
    size: 5
};

const QuickSearch = ({form, onChange, onSearch, onClearResults, history, t} = {}) => {
    return <Autosuggest multiSection={true}
                        suggestions={form._results}
                        getSectionSuggestions={section => section.results}
                        getSuggestionValue={suggestion => suggestion.name}
                        onSuggestionsFetchRequested={({value}) => onSearch(value, form.size)}
                        onSuggestionsClearRequested={onClearResults}
                        onSuggestionSelected={(e, {suggestion}) => {
                            onChange({
                                target: {
                                    name: "pattern",
                                    value: ""
                                }
                            });
                            history.push({
                                pathname: route.SEARCH(),
                                search: "?" + queryString.stringify({
                                    pattern: suggestion.name,
                                    type: suggestion.type.value
                                })
                            });
                        }}
                        shouldRenderSuggestions={value => value.trim().length >= MIN_LENGTH}
                        inputProps={{
                            type: "search",
                            name: "pattern",
                            value: form.pattern || "",
                            placeholder: t("title.search"),
                            onChange: onChange
                        }}
                        renderSuggestion={suggestion =>
                            <span>
                                 {suggestion.name}
                             </span>}
                        renderSectionTitle={section =>
                            <strong>
                                {t(section.type.pluralTitle)}
                            </strong>}
                        renderInputComponent={inputProps =>
                            <InputGroup>
                                <Input {...inputProps}/>
                                <InputGroupAddon addonType="append">
                                    <Button color="outline-light"
                                            onClick={() => {
                                                history.push({
                                                    pathname: route.SEARCH(),
                                                    search: "?" + queryString.stringify({
                                                        pattern: form.pattern,
                                                        type: entityType.ACCOUNT.value
                                                    })
                                                })
                                            }}>
                                        {form._submitting ?
                                            <Spinner size="sm"
                                                     color="light"
                                                     className="mr-1"/> :
                                            <FontAwesomeIcon icon={faSearch}/>}
                                        {" "}
                                    </Button>
                                </InputGroupAddon>
                            </InputGroup>}/>;
};

export default QuickSearch;

QuickSearch.propTypes = {
    form: PropTypes.shape({
        pattern: PropTypes.string,
        size: PropTypes.number
    }),
    onChange: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
    onClearResults: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

QuickSearch.defaultProps = {
    form: DEFAULT_FORM
};
