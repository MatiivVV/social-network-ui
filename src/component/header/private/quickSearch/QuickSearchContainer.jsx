import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import QuickSearch from "./QuickSearch";
import {quickSearchAction} from "../../../../action/search/quickSearchAction";
import {changeFormAction, changeFormByEventAction} from "../../../../action/form/changeFormAction";
import {withRouter} from 'react-router-dom'
import formType from "../../../../action/form/formType";

const mapStateToProps = state => ({
    form: state.forms[formType.QUICK_SEARCH]
});

const mapDispatchToProps = dispatch => ({
    onChange: e => dispatch(changeFormByEventAction(formType.QUICK_SEARCH, e)),
    onSearch: (pattern = "", size) => dispatch(quickSearchAction(pattern, size)),
    onClearResults: () => dispatch(changeFormAction(formType.QUICK_SEARCH, {_results: []}))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(QuickSearch)));
