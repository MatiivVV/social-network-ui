import React, {Component} from "react";
import {Button, Collapse, Navbar, NavbarToggler,} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index"
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons/index"
import PropTypes from "prop-types";
import QuickSearchContainer from "./quickSearch/QuickSearchContainer";
import {Link} from "react-router-dom";
import route from "../../../constant/route";
import * as imageApi from "../../../api/imageApi";

class HeaderPrivate extends Component {
    constructor(props) {
        super(props);
        this.handleToggleNavbar = this.handleToggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    handleToggleNavbar() {
        this.setState(state => ({
            collapsed: !state.collapsed
        }));
    }

    render() {
        let {user, onLogout, t} = this.props;
        return <Navbar color="primary" dark expand="lg">
            <NavbarToggler onClick={this.handleToggleNavbar}/>
            <Collapse isOpen={!this.state.collapsed} navbar>
                <img src={user.imageId ? imageApi.getImageUrl(user.imageId) : "/image/missingAccountImage.jpg"}
                     className="my-1 mr-2 rounded-circle img-icon"
                     alt=""/>
                {user.id && <Link to={route.ACCOUNT(user.id)} className="navbar-brand mr-auto my-2 my-lg-1">
                    {user.firstName && user.firstName}
                    {user.firstName && user.lastName && " "}
                    {user.lastName && user.lastName}
                </Link>}
                <div className="my-2 my-lg-1 mr-2">
                    <QuickSearchContainer/>
                </div>
                <Button color="outline-light"
                        className="my-2 my-lg-1 mr-2"
                        onClick={onLogout}>
                    <FontAwesomeIcon icon={faSignOutAlt}/>
                    {" " + t("title.account.logout")}
                </Button>
            </Collapse>
        </Navbar>;
    }
}

export default HeaderPrivate;

HeaderPrivate.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.number,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        imageId: PropTypes.string

    }).isRequired,
    onLogout: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};
