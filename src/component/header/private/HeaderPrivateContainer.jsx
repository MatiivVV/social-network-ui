import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import HeaderPrivate from "./HeaderPrivate";
import {logoutAction} from "../../../action/user/logoutAction";

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => ({
    onLogout: () => dispatch(logoutAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(HeaderPrivate));
