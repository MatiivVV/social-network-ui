import {withTranslation} from "react-i18next";
import HeaderPublic from "./HeaderPublic";
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import {changeUserAction} from "../../../action/user/changeUserAction";

const mapDispatchToProps = dispatch => ({
    onChangeLocale: locale => dispatch(changeUserAction({locale: locale}))
});

export default withRouter(connect(null, mapDispatchToProps)(withTranslation()(HeaderPublic)));