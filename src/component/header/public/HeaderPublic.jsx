import React, {Component} from "react";
import {
    Button,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    UncontrolledDropdown
} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index"
import {faGlobe, faSignInAlt, faUserPlus} from "@fortawesome/free-solid-svg-icons/index"
import PropTypes from "prop-types";
import route from "../../../constant/route"
import lang from "../../../constant/domain/lang";

class HeaderPublic extends Component {
    constructor(props) {
        super(props);
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        let {onChangeLocale, history, t} = this.props;
        return <Navbar color="primary" dark expand="lg">
            <NavbarToggler onClick={this.toggleNavbar}/>
            <Collapse isOpen={!this.state.collapsed} navbar>
                <NavbarBrand className="my-2 my-lg-1">
                    {t("title.brand")}
                </NavbarBrand>
                <UncontrolledDropdown className="mr-auto">
                    <DropdownToggle color="outline-light" className="mr-2" caret>
                        <FontAwesomeIcon icon={faGlobe}/>
                    </DropdownToggle>
                    <DropdownMenu>
                        {Object.values(lang).map(lang =>
                            <DropdownItem key={lang.value} onClick={() => onChangeLocale(lang.value)}>
                                {t(lang.title)}
                            </DropdownItem>)}
                    </DropdownMenu>
                </UncontrolledDropdown>
                <Button color="outline-light"
                        className="my-2 my-lg-1 mr-2"
                        onClick={() => history.push(route.LOGIN())}>
                    <FontAwesomeIcon icon={faSignInAlt}/>
                    {" " + t("title.account.login")}
                </Button>
                <Button color="outline-light"
                        className="my-2 my-lg-1 mr-2"
                        onClick={() => history.push(route.REGISTER())}>
                    <FontAwesomeIcon icon={faUserPlus}/>
                    {" " + t("title.account.register")}
                </Button>
            </Collapse>
        </Navbar>;
    }
}

export default HeaderPublic;

HeaderPublic.propTypes = {
    onChangeLocale: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};