import {connect} from "react-redux";
import Notification from "./Notification";
import {withTranslation} from "react-i18next";
import {closeNotificationAction} from "../../action/notification/closeNotificationAction";

const mapStateToProps = state => ({
    notification: state.notification
});

const mapDispatchToProps = dispatch => ({
    onClose: () => dispatch(closeNotificationAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Notification));
