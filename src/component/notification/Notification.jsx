import React from "react";
import {Alert} from "reactstrap";
import PropTypes from "prop-types";

const Notification = ({notification, onClose, t} = {}) => {
    return <Alert color={notification.type.className}
                  className="mt-1 mx-1"
                  isOpen={notification.active}
                  toggle={onClose}
                  fade={false}>
        {notification.message && t(notification.message)}
        {notification.description && `: ${notification.description}`}
    </Alert>;
};

export default Notification;

Notification.propTypes = {
    notification: PropTypes.shape({
        active: PropTypes.bool.isRequired,
        type: PropTypes.shape({
            className: PropTypes.string.isRequired
        }),
        message: PropTypes.string.isRequired,
        description: PropTypes.string
    }).isRequired,
    onClose: PropTypes.func.isRequired
};
