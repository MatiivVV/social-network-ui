import React, {Component} from "react";
import HeaderPrivate from "./header/private/HeaderPrivateContainer";
import HeaderPublic from "./header/public/HeaderPublicContainer";
import Notification from "./notification/NotificationContainer";
import {Redirect, Route, Switch, withRouter} from "react-router-dom"
import {connect} from "react-redux";
import ErrorContainer from "./error/ErrorContainer";
import LoginContainer from "./login/LoginContainer";
import AccountContainer from "./account/AccountContainer";
import RegisterContainer from "./account/register/RegisterContainer";
import PropTypes from "prop-types";
import route from "../constant/route";
import GroupContainer from "./group/GroupContainer";
import GroupCreateContainer from "./group/create/GroupCreateContainer";
import SearchContainer from "./search/SearchContainer";
import errorType from "../constant/errorType";
import AccountEditContainer from "./account/edit/AccountEditContainer";
import GroupEditContainer from "./group/edit/GroupEditContainer";
import AccountFriendsContainer from "./account/friendship/friend/AccountFriendsContainer";
import AccountGroupsContainer from "./account/membership/group/AccountGroupsContainer";
import GroupMembersContainer from "./group/membership/member/GroupMembersContainer";
import AccountFriendshipsContainer from "./account/friendship/AccountFriendshipsContainer";
import AccountMembershipsContainer from "./account/membership/AccountMembershipsContainer";
import AccountChatsContainer from "./account/chat/chats/AccountChatsContainer";
import GroupMembershipContainer from "./group/membership/GroupMembershipContainer";
import AccountChatContainer from "./account/chat/AccountChatContainer";
import {subscribeNotificationsAction} from "../action/user/subscribeNotificationsAction";

class App extends Component {
    constructor(props) {
        super(props);
        this.isPublicPath = this.isPublicPath.bind(this);
    }

    componentDidMount() {
        let {loggedIn, onAuthorized} = this.props;
        if (loggedIn) {
            onAuthorized();
        }
    }

    isPublicPath(path) {
        const publicPaths = [
            route.LOGIN(),
            route.REGISTER()
        ];
        return publicPaths.includes(path);
    };

    render() {
        let {id, loggedIn, location} = this.props;
        if (!this.isPublicPath(location.pathname) && !loggedIn) {
            return <Redirect to={route.LOGIN()}/>;
        }
        if (this.isPublicPath(location.pathname) && loggedIn) {
            return <Redirect to={route.ACCOUNT(id)}/>;
        }
        return <div>
            <div className="sticky-top">
                {loggedIn ? <HeaderPrivate/> : <HeaderPublic/>}
                <Notification/>
            </div>
            <Switch>
                <Route exact path={route.LOGIN()} component={LoginContainer}/>
                <Route exact path={route.SEARCH()} component={SearchContainer}/>
                <Route exact path={route.ACCOUNT()} component={AccountContainer}/>
                <Route exact path={route.REGISTER()} component={RegisterContainer}/>
                <Route exact path={route.ACCOUNT_EDIT()} component={AccountEditContainer}/>
                <Route exact path={route.ACCOUNT_FRIENDS()} component={AccountFriendsContainer}/>
                <Route exact path={route.ACCOUNT_FRIENDSHIPS()} component={AccountFriendshipsContainer}/>
                <Route exact path={route.ACCOUNT_GROUPS()} component={AccountGroupsContainer}/>
                <Route exact path={route.ACCOUNT_MEMBERSHIPS()} component={AccountMembershipsContainer}/>
                <Route exact path={route.GROUP_CREATE()} component={GroupCreateContainer}/>
                <Route exact path={route.GROUP()} component={GroupContainer}/>
                <Route exact path={route.GROUP_EDIT()} component={GroupEditContainer}/>
                <Route exact path={route.GROUP_MEMBERS()} component={GroupMembersContainer}/>
                <Route exact path={route.GROUP_MEMBERSHIPS()} component={GroupMembershipContainer}/>
                <Route exact path={route.ACCOUNT_CHATS()} component={AccountChatsContainer}/>
                <Route exact path={route.ACCOUNT_CHAT()} component={AccountChatContainer}/>
                <Redirect exact from={route.INDEX()} to={route.ACCOUNT(id)}/>
                <Route exact path={route.ERROR()} component={ErrorContainer}/>
                <Redirect to={route.ERROR(errorType.NOT_FOUND)}/>
            </Switch>
        </div>;
    }
}

const mapStateToProps = state => ({
    id: state.user.id,
    loggedIn: state.user.loggedIn
});

const mapDispatchToProps = dispatch => ({
    onAuthorized: () => dispatch(subscribeNotificationsAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));

App.propTypes = {
    id: PropTypes.number,
    loggedIn: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
    onAuthorized: PropTypes.func.isRequired
};
