import i18n from "i18next/index";
import {initReactI18next} from "react-i18next";
import intervalPlural from "i18next-intervalplural-postprocessor";
import translationEN from "./translationEN.json";
import translationRU from "./translationRU.json";
import {registerLocale} from "react-datepicker/es";
import dateTranslationEN from "date-fns/locale/en-US";
import dateTranslationRU from "date-fns/locale/ru";
import lang from "../constant/domain/lang";

const resources = {
    en: {
        translation: translationEN
    },
    ru: {
        translation: translationRU
    }
};

i18n
    .use(initReactI18next)
    .use(intervalPlural)
    .init({
        interpolation: {
            escapeValue: false,
        },
        debug: true,
        resources,
        fallbackLng: lang.EN.value
    });

export default i18n;

registerLocale(lang.EN.value, dateTranslationEN);
registerLocale(lang.RU.value, dateTranslationRU);
