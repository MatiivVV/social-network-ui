import notificationType from "./action/notification/notificationType";

export const APP_INITIAL_STATE = {
    user: {
        loggedIn: false,
        locale: "en"
    },
    notification: {
        active: false,
        type: notificationType.INFO,
        message: ""
    },
    forms: {}
};

export const FORM_INITIAL_STATE = {
    _fetching: false,
    _submitting: false,
    _results: [],
    _violations: []
};
