export default {
    PERSONAL: {
        value: "PERSONAL",
        title: "title.account.contact.personal"
    },
    BUSINESS: {
        value: "BUSINESS",
        title: "title.account.contact.business"
    }
};
