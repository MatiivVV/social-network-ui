export default {
    EN: {
        value: "en",
        title: "title.lang.en"
    },
    RU: {
        value: "ru",
        title: "title.lang.ru"
    }
};
