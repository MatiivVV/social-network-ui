export default {
    PENDING: {
        value: "PENDING",
        title: "title.request.pending"
    },
    ACCEPTED: {
        value: "ACCEPTED",
        title: "title.request.accepted"
    },
    DECLINED: {
        value: "DECLINED",
        title: "title.request.declined"
    }
};
