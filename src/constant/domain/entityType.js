export default {
    ACCOUNT: {
        value: "ACCOUNT",
        title: "title.account.value",
        pluralTitle: "title.accounts"
    },
    GROUP: {
        value: "GROUP",
        title: "title.group.value",
        pluralTitle: "title.groups"
    }
};
