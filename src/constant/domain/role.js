export default {
    USER: {
        value: "USER",
        title: "title.account.role.user"
    },
    ADMIN: {
        value: "ADMIN",
        title: "title.account.role.admin"
    }
};
