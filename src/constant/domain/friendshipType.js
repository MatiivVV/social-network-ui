export default {
    REQUEST: {
        value: "REQUEST",
        title: "title.account.friendRequests.out"
    },
    RESPONSE: {
        value: "RESPONSE",
        title: "title.account.friendRequests.in"
    }
};
