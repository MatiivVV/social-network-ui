export default {
    NOT_FOUND: {
        value: "notFound",
        message: "message.notFound"
    },
    NOT_AUTHORIZED: {
        value: "notAuthorized",
        message: "message.notAuthorized"
    }
};
