const LOCAL_STORAGE_KEY = "reduxStore";

export const getLocalStorageState = initialState => {
    return localStorage[LOCAL_STORAGE_KEY] ? JSON.parse(localStorage[LOCAL_STORAGE_KEY]) : initialState;
};

export const getLocalStorageMiddleware = store => next => action => {
    next(action);
    localStorage[LOCAL_STORAGE_KEY] = JSON.stringify(store.getState());
};
