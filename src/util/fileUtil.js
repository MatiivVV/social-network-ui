export const readFile = file => {
    new Promise((resolve, reject) => {
        let fileReader = new FileReader();
        fileReader.onload = () => {
            resolve(fileReader.result)
        };
        fileReader.onerror = reject;
        fileReader.readAsText(file);
    });
};
