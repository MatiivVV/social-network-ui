export const mapLocation = location => {
    let index = location.lastIndexOf("/");
    if (index !== -1) {
        return location.substr(location.lastIndexOf("/") + 1);
    }
};

export const mapError = (error, violationPrefix) => {
    if (violationPrefix) {
        return {
            message: error.message,
            violations: (error.constraintViolations || [])
                .map(violation => ({
                    ...violation,
                    field: violation.field.substr(violationPrefix.length + 1)
                }))
        };
    }
    return {
        message: error.message
    };
};

export const mapPage = page => ({
    content: page.content,
    page: page.number + 1,
    size: page.size,
    pageElements: page.numberOfElements,
    totalPages: page.totalPages,
    totalElements: page.totalElements
});
