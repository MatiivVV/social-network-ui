export const getEventAsObject = e => {
    if (e) {
        return {[e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value};
    }
    return {};
};

export const getViolationsByField = (violations, field) => {
    if (!violations) {
        return [];
    }
    return [
        ...violations
            .filter(v => v.field === field)
            .map(v => ({message: v.message})),
        ...violations
            .filter(v => v.field.startsWith(`${field}.`))
            .map(v => ({
                field: v.field.substring(field.length + 1),
                message: v.message
            })),
        ...violations
            .filter(v => v.field.startsWith(`${field}[`))
            .map(v => ({
                field: v.field.substring(field.length),
                message: v.message
            }))
    ];
};

export const existViolationsByField = (violations, field) => {
    if (!violations) {
        return false;
    }
    return violations.filter(v => v.field === field
        || v.field.startsWith(`${field}.`)
        || v.field.startsWith(`${field}[`)).length > 0;
};
