const TOKEN_EXPIRATION_DELAY = 60 * 1000;

export const getTokenAsObject = token => {
    let base64 = token
        .split(".")[1]
        .replace("-", "+")
        .replace("_", "/");
    return JSON.parse(window.atob(base64));
};

export const isTokenExpired = token => {
    if (!token) {
        return true;
    }
    return getTokenAsObject(token).exp * 1000 < Date.now() + TOKEN_EXPIRATION_DELAY;
};
