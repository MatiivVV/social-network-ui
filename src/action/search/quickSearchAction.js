import {getAccountsApi} from "../../api/account/accountApi";
import {ensureAuthAction} from "../user/ensureAuthAction";
import {getGroupsApi} from "../../api/group/groupApi";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../form/submitFormAction";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import notificationType from "../notification/notificationType";
import entityType from "../../constant/domain/entityType";
import formType from "../form/formType";
import {changeFormAction} from "../form/changeFormAction";

export const quickSearchAction = (pattern, size) => dispatch => {
    dispatch(submitFormStartAction(formType.QUICK_SEARCH));
    return dispatch(ensureAuthAction()).then(credentials => {
        dispatch(changeFormAction(formType.QUICK_SEARCH, {
            pattern: pattern,
            type: size
        }));
        return Promise.all([
            getAccountsApi(pattern,
                1,
                size,
                credentials.accessToken,
                credentials.locale).then(result => {
                return {
                    type: entityType.ACCOUNT,
                    results: result.content.map(result => ({
                        id: result.id,
                        type: entityType.ACCOUNT,
                        name: `${result.firstName} ${result.lastName}`
                    }))
                };
            }),
            getGroupsApi(pattern,
                1,
                size,
                credentials.accessToken,
                credentials.locale).then(result => {
                return {
                    type: entityType.GROUP,
                    results: result.content.map(result => ({
                        id: result.id,
                        type: entityType.GROUP,
                        name: result.name
                    }))
                };
            })
        ]);
    }).then(results => {
        dispatch(submitFormSuccessAction(formType.QUICK_SEARCH, false, results.filter(result => result.results.length > 0)));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.QUICK_SEARCH));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
