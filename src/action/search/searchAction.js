import {getAccountsApi} from "../../api/account/accountApi";
import {ensureAuthAction} from "../user/ensureAuthAction";
import {getGroupsApi} from "../../api/group/groupApi";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../form/submitFormAction";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import notificationType from "../notification/notificationType";
import formType from "../form/formType";
import entityType from "../../constant/domain/entityType";
import {changeFormAction} from "../form/changeFormAction";

export const searchAction = ({pattern, type, page, size}) => dispatch => {
    dispatch(submitFormStartAction(formType.SEARCH));
    return dispatch(ensureAuthAction()).then(credentials => {
        dispatch(changeFormAction(formType.SEARCH, {
            pattern: pattern,
            type: type,
            page: Number(page),
            size: Number(size)
        }));
        switch (type) {
            case entityType.ACCOUNT.value:
                return getAccountsApi(pattern,
                    page,
                    size,
                    credentials.accessToken,
                    credentials.locale);
            case entityType.GROUP.value:
                return getGroupsApi(pattern,
                    page,
                    size,
                    credentials.accessToken,
                    credentials.locale);
            default:
                throw new Error(`Illegal entity type - ${type}`)
        }
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.SEARCH, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.SEARCH));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
