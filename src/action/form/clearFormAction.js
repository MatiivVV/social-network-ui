import actionType from "../actionType";

export const clearFormAction = name => {
    return {
        type: actionType.CLEAR_FORM,
        name: name
    };
};
