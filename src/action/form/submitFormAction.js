import actionType from "../actionType";

export const submitFormStartAction = name => {
    return {
        type: actionType.SUBMIT_FORM_START,
        name: name,
    };
};

export const submitFormSuccessAction = (name, clear = false, results = []) => {
    return {
        type: actionType.SUBMIT_FORM_SUCCESS,
        name: name,
        results: results,
        clear: clear
    };
};

export const submitFormFailAction = (name, violations = []) => {
    return {
        type: actionType.SUBMIT_FORM_FAIL,
        name: name,
        violations: violations
    };
};
