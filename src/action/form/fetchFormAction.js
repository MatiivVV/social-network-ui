import actionType from "../actionType";

export const fetchFormStartAction = name => {
    return {
        type: actionType.FETCH_FORM_START,
        name: name
    };
};

export const fetchFormSuccessAction = (name, content = {}, merge = false) => {
    return {
        type: actionType.FETCH_FORM_SUCCESS,
        name: name,
        content: content,
        merge: merge
    };
};

export const fetchFormFailAction = name => {
    return {
        type: actionType.FETCH_FORM_FAIL,
        name: name
    };
};
