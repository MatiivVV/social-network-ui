import actionType from "../actionType";
import {getEventAsObject} from "../../util/formUtil";

export const changeFormAction = (name, content = {}) => {
    return {
        type: actionType.CHANGE_FORM,
        name: name,
        content: content
    };
};

export const changeFormByEventAction = (name, e) => {
    return changeFormAction(name, getEventAsObject(e));
};
