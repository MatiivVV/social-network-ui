import {getGroupApi} from "../../api/group/groupApi"
import {getMembershipApi} from "../../api/group/accountMembershipApi"
import {ensureAuthAction} from "../user/ensureAuthAction";
import route from "../../constant/route";
import role from "../../constant/domain/role";
import status from "../../constant/domain/status";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import notificationType from "../notification/notificationType";
import formType from "../form/formType";
import {fetchFormFailAction, fetchFormStartAction, fetchFormSuccessAction} from "../form/fetchFormAction";
import {clearFormAction} from "../form/clearFormAction";
import {getGroupMessagesAction} from "./message/getGroupMessagesAction";
import errorType from "../../constant/errorType";

export const getGroupAction = (id, history, messagePageSize) => dispatch => {
    dispatch(fetchFormStartAction(formType.GROUP));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getGroupApi(id, credentials.accessToken, credentials.locale).then(async group => {
            if (group) {
                let membership = await getMembershipApi(credentials.id, id, credentials.accessToken, credentials.locale);
                group.related = !!membership;
                if (credentials.role === role.ADMIN.value) {
                    group.messageUsable = true;
                    group.editable = true;
                } else {
                    group.messageUsable = membership && membership.status === status.ACCEPTED.value;
                    group.editable = membership && membership.role === role.ADMIN.value;
                }
                dispatch(fetchFormSuccessAction(formType.GROUP, group));
                dispatch(clearFormAction(formType.GROUP_MESSAGE));
                dispatch(clearFormAction(formType.GROUP_MESSAGES));
                if (group.messageUsable && messagePageSize) {
                    dispatch(getGroupMessagesAction(id, 1, messagePageSize));
                }
                return Promise.resolve(group);
            }
            dispatch(fetchFormFailAction(formType.GROUP));
            history.push(route.ERROR(errorType.NOT_FOUND));
            return Promise.resolve();
        });
    }).catch(e => {
        dispatch(fetchFormFailAction(formType.GROUP));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
