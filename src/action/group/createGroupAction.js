import {createGroupApi} from "../../api/group/groupApi"
import notificationType from "../notification/notificationType";
import formType from "../form/formType";
import route from "../../constant/route";
import {ensureAuthAction} from "../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../form/submitFormAction";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../notification/raiseNotificationAction";
import {UnprocessableEntityError} from "../../api/error/UnprocessableEntityError";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";

export const createGroupAction = (form, image, history) => dispatch => {
    dispatch(submitFormStartAction(formType.GROUP_CREATE));
    return dispatch(ensureAuthAction()).then(credentials => {
        let group = {...form};
        delete group._results;
        delete group._violations;
        delete group._submitting;
        return createGroupApi(group, image, credentials.accessToken, credentials.locale).then(id => {
            dispatch(submitFormSuccessAction(formType.GROUP_CREATE, true));
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.createGroupSuccess"));
            history.push(route.GROUP(id));
            return id;
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.GROUP_CREATE, e.error.violations));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else {
            dispatch(submitFormFailAction(formType.GROUP_CREATE));
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        }
    });
};
