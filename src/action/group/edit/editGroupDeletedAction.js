import {editGroupDeletedApi} from "../../../api/group/groupApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {changeFormAction} from "../../form/changeFormAction";

export const editGroupDeletedAction = (id, deleted) => dispatch => {
    dispatch(submitFormStartAction(formType.GROUP_EDIT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return editGroupDeletedApi(id, deleted, credentials.accessToken, credentials.locale).then(() => {
            dispatch(changeFormAction(formType.GROUP_EDIT, {deleted: deleted}));
            dispatch(submitFormSuccessAction(formType.GROUP_EDIT));
            let message = deleted ? "message.deleteGroupSuccess" : "message.restoreGroupSuccess";
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, message));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.GROUP_EDIT));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(submitFormFailAction(formType.GROUP_EDIT));
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
