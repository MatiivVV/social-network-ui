import {getGroupApi} from "../../../api/group/groupApi"
import {getMembershipApi} from "../../../api/group/accountMembershipApi"
import {ensureAuthAction} from "../../user/ensureAuthAction";
import route from "../../../constant/route";
import role from "../../../constant/domain/role";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {fetchFormFailAction, fetchFormStartAction, fetchFormSuccessAction} from "../../form/fetchFormAction";
import errorType from "../../../constant/errorType";

export const getGroupForEditAction = (id, history) => dispatch => {
    dispatch(fetchFormStartAction(formType.GROUP_EDIT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getGroupApi(id, credentials.accessToken, credentials.locale).then(async group => {
            if (group) {
                let editable;
                if (credentials.role === role.ADMIN.value) {
                    editable = true;
                } else {
                    let membership = await getMembershipApi(credentials.id, id, credentials.accessToken, credentials.locale);
                    editable = membership && membership.role === role.ADMIN.value;
                }
                if (editable) {
                    dispatch(fetchFormSuccessAction(formType.GROUP_EDIT, group));
                    return Promise.resolve(group);
                }
                history.push(route.ERROR(errorType.NOT_AUTHORIZED));
            } else {
                history.push(route.ERROR(errorType.NOT_FOUND));
            }
            dispatch(fetchFormFailAction(formType.GROUP_EDIT));
            return Promise.resolve();
        });
    }).catch(e => {
        dispatch(fetchFormFailAction(formType.GROUP_EDIT));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
