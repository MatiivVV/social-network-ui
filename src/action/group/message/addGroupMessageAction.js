import {submitFormSuccessAction} from "../../form/submitFormAction";
import formType from "../../form/formType";

export const addGroupMessageAction = message => (dispatch, getState) => {
    let {page, size, _results} = getState().forms[formType.GROUP_MESSAGES];
    if (page === 1 && _results.content) {
        let {totalElements, totalPages} = _results;
        let content = [..._results.content];
        content.unshift(message);
        if (_results.content.length === size) {
            content.pop();
        }
        dispatch(submitFormSuccessAction(formType.GROUP_MESSAGES, false, {
            ..._results,
            content,
            totalElements: totalElements + 1,
            totalPages: totalElements + 1 > totalPages * size ? totalPages + 1 : totalPages
        }));
    }
};
