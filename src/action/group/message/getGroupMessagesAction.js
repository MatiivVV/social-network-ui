import {getGroupMessagesApi} from "../../../api/group/groupMessageApi";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {changeFormAction} from "../../form/changeFormAction";

export const getGroupMessagesAction = (id, page, size) => dispatch => {
    dispatch(changeFormAction(formType.GROUP_MESSAGES, {
        id,
        page,
        size
    }));
    dispatch(submitFormStartAction(formType.GROUP_MESSAGES));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getGroupMessagesApi(id,
            page,
            size,
            credentials.accessToken,
            credentials.locale);
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.GROUP_MESSAGES, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.GROUP_MESSAGES));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
