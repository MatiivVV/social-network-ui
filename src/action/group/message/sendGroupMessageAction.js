import {sendGroupMessageApi} from "../../../api/group/groupMessageApi"
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {addGroupMessageAction} from "./addGroupMessageAction";

export const sendGroupMessageAction = (id, form, image) => dispatch => {
    dispatch(submitFormStartAction(formType.GROUP_MESSAGE));
    return dispatch(ensureAuthAction()).then(credentials => {
        let message = {...form};
        delete message._results;
        delete message._violations;
        delete message._submitting;
        message.message = message.message || "";
        return sendGroupMessageApi(id,
            credentials.id,
            message,
            image,
            credentials.accessToken,
            credentials.locale
        ).then(message => {
            dispatch(submitFormSuccessAction(formType.GROUP_MESSAGE, true));
            dispatch(raiseTimedNotificationAction(notificationType.INFO, "message.sendMessageSuccess"));
            dispatch(addGroupMessageAction(message));
            return Promise.resolve(message);
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.GROUP_MESSAGE, e.error.violations));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else {
            dispatch(submitFormFailAction(formType.GROUP_MESSAGE));
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        }
    });
};
