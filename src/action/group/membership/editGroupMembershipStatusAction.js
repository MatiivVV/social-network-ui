import {editMembershipStatusApi} from "../../../api/group/groupMembershipApi"
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import formType from "../../form/formType";
import {getGroupMembershipsAction} from "./getGroupMembershipsAction";

export const editGroupMembershipStatusAction = (id, memberId, status, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return editMembershipStatusApi(id, memberId, status, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.editGroupMembershipStatusSuccess"));
            let membershipsForm = getState().forms[formType.GROUP_MEMBERSHIPS];
            if (id === membershipsForm.id) {
                dispatch(getGroupMembershipsAction(id,
                    membershipsForm.statuses,
                    membershipsForm.page,
                    membershipsForm.size,
                    history
                ));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
