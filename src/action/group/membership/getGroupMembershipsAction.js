import {getMembershipApi, getMembershipsApi} from "../../../api/group/groupMembershipApi";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {changeFormAction} from "../../form/changeFormAction";
import role from "../../../constant/domain/role";
import {getGroupApi} from "../../../api/group/groupApi";
import route from "../../../constant/route";
import errorType from "../../../constant/errorType";

export const getGroupMembershipsAction = (id, statuses, page, size, history) => dispatch => {
    dispatch(submitFormStartAction(formType.GROUP_MEMBERSHIPS));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getGroupApi(id, credentials.accessToken, credentials.locale).then(async group => {
            if (group) {
                let editable;
                if (credentials.role === role.ADMIN.value) {
                    editable = true;
                } else {
                    let membership = await getMembershipApi(id, credentials.id, credentials.accessToken, credentials.locale);
                    editable = membership && membership.role === role.ADMIN.value;
                }
                if (editable) {
                    dispatch(changeFormAction(formType.GROUP_MEMBERSHIPS, {
                        id,
                        statuses,
                        page,
                        size,
                        editable
                    }));
                    return getMembershipsApi(id,
                        [],
                        statuses,
                        page,
                        size,
                        credentials.accessToken,
                        credentials.locale);
                }
                history.push(route.ERROR(errorType.NOT_AUTHORIZED));
            } else {
                history.push(route.ERROR(errorType.NOT_FOUND));
            }
            dispatch(submitFormFailAction(formType.GROUP_MEMBERSHIPS));
            return Promise.resolve();
        });
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.GROUP_MEMBERSHIPS, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.GROUP_MEMBERSHIPS));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
