import {getMembersApi, getMembershipApi} from "../../../../api/group/groupMembershipApi";
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../../form/submitFormAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import formType from "../../../form/formType";
import {changeFormAction} from "../../../form/changeFormAction";
import role from "../../../../constant/domain/role";
import {getGroupApi} from "../../../../api/group/groupApi";
import route from "../../../../constant/route";
import errorType from "../../../../constant/errorType";

export const getGroupMembersAction = (id, roles, page, size, history) => dispatch => {
    dispatch(submitFormStartAction(formType.GROUP_MEMBERS));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getGroupApi(id, credentials.accessToken, credentials.locale).then(async group => {
            if (group) {
                let editable;
                if (credentials.role === role.ADMIN.value) {
                    editable = true;
                } else {
                    let membership = await getMembershipApi(id, credentials.id, credentials.accessToken, credentials.locale);
                    editable = membership && membership.role === role.ADMIN.value;
                }
                dispatch(changeFormAction(formType.GROUP_MEMBERS, {
                    id,
                    roles,
                    page,
                    size,
                    editable
                }));
                return getMembersApi(id,
                    roles,
                    page,
                    size,
                    credentials.accessToken,
                    credentials.locale);
            }
            history.push(route.ERROR(errorType.NOT_FOUND));
            dispatch(submitFormFailAction(formType.GROUP_MEMBERS));
            return Promise.resolve();
        });
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.GROUP_MEMBERS, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.GROUP_MEMBERS));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
