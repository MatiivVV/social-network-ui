import {editMembershipRoleApi} from "../../../../api/group/groupMembershipApi"
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import {UnprocessableEntityError} from "../../../../api/error/UnprocessableEntityError";
import formType from "../../../form/formType";
import {getGroupMembersAction} from "./getGroupMembersAction";

export const editGroupMemberRoleAction = (id, memberId, role, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return editMembershipRoleApi(id, memberId, role, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.editGroupMemberRoleSuccess"));
            let membersForm = getState().forms[formType.GROUP_MEMBERS];
            if (id === membersForm.id) {
                dispatch(getGroupMembersAction(id, membersForm.roles, membersForm.page, membersForm.size, history));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
