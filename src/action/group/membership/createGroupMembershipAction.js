import {createMembershipApi} from "../../../api/group/accountMembershipApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {changeFormAction} from "../../form/changeFormAction";

export const createGroupMembershipAction = id => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return createMembershipApi(credentials.id, id, credentials.accessToken, credentials.locale).then(() => {
            if (getState().forms[formType.GROUP].id === id) {
                dispatch(changeFormAction(formType.GROUP, {
                    related: true
                }));
            }
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.createGroupMembershipSuccess"));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
