import {getTokenApi} from "../../api/authApi"
import notificationType from "../notification/notificationType";
import formType from "../form/formType";
import actionType from "../actionType";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../form/submitFormAction";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../notification/raiseNotificationAction";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {clearFormAction} from "../form/clearFormAction";
import {subscribeNotificationsAction} from "./subscribeNotificationsAction";

export const loginAction = ({emailAddress, password, rememberMe = false} = {}) => dispatch => {
    dispatch(submitFormStartAction(formType.LOGIN));
    return getTokenApi(emailAddress, password).then(token => {
        if (!rememberMe) {
            delete token["refresh_token"];
        }
        dispatch(getTokenSuccessAction(token));
        dispatch(submitFormSuccessAction(formType.LOGIN));
        dispatch(clearFormAction(formType.LOGIN));
        dispatch(raiseTimedNotificationAction(notificationType.INFO, "message.loginSuccess"));
        dispatch(subscribeNotificationsAction());
        return token;
    }).catch(e => {
        dispatch(submitFormFailAction(formType.LOGIN));
        if (e instanceof UnauthorizedError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.loginFail"));
        } else {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};

export const getTokenSuccessAction = token => ({
    type: actionType.GET_TOKEN_SUCCESS,
    token: token
});
