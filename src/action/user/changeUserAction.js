import actionType from "../actionType";
import i18n from "../../i18n/i18n";

export const changeUserAction = content => {
    if (content.locale) {
        i18n.changeLanguage(content.locale);
    }
    return {
        type: actionType.CHANGE_USER,
        content: content
    };
};
