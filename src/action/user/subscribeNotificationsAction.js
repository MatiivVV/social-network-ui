import {ensureAuthAction} from "../user/ensureAuthAction";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import notificationType from "../notification/notificationType";
import {subscribeNotificationsApi} from "../../api/notificationApi";
import {receiveChatMessageAction} from "../account/chat/message/receiveChatMessageAction";

export const subscribeNotificationsAction = () => dispatch => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return subscribeNotificationsApi(credentials.id,
            message => dispatch(receiveChatMessageAction(JSON.parse(message.body))),
            credentials.accessToken
        ).catch(e => {
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        });
    });
};
