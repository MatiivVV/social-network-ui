import notificationType from "../notification/notificationType";
import actionType from "../actionType";
import {raiseTimedNotificationAction} from "../notification/raiseNotificationAction";
import {unsubscribeNotificationsAction} from "./unsubscribeNotificationsAction";

export const logoutAction = () => dispatch => {
    dispatch(logoutSuccessAction());
    dispatch(raiseTimedNotificationAction(notificationType.INFO, "message.logoutSuccess"));
    dispatch(unsubscribeNotificationsAction());
};

export const logoutSuccessAction = () => ({
    type: actionType.LOGOUT
});
