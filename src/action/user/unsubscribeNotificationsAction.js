import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import notificationType from "../notification/notificationType";
import {unsubscribeNotificationsApi} from "../../api/notificationApi";

export const unsubscribeNotificationsAction = () => dispatch => {
    return unsubscribeNotificationsApi().catch(e => {
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
