import {isTokenExpired} from "../../util/tokenUtil";
import {refreshTokenApi} from "../../api/authApi"
import notificationType from "../notification/notificationType";
import actionType from "../actionType";
import {logoutSuccessAction} from "./logoutAction";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";

export const ensureAuthAction = () => (dispatch, getState) => {
    let {id, token, locale, role} = getState().user;
    return Promise.resolve(token).then(token => {
        if (isTokenExpired(token["access_token"])) {
            if (isTokenExpired(token["refresh_token"])) {
                throw new UnauthorizedError();
            }
            return refreshTokenApi(token["refresh_token"]).then(newToken => {
                dispatch(refreshTokenSuccessAction(newToken));
                return newToken["access_token"]
            })
        }
        return token["access_token"]
    }).then(accessToken => ({
        id: id,
        accessToken: accessToken,
        locale: locale,
        role: role
    })).catch(e => {
        dispatch(logoutSuccessAction());
        if (e instanceof UnauthorizedError) {
            dispatch(raiseNotificationAction(notificationType.INFO, "message.sessionExpired"));
        }
        throw e;
    });
};

export const refreshTokenSuccessAction = token => ({
    type: actionType.REFRESH_TOKEN_SUCCESS,
    token: token
});
