import actionType from "../actionType";

export const closeNotificationAction = () => ({
    type: actionType.CLOSE_NOTIFICATION
});
