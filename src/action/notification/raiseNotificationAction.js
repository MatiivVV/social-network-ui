import actionType from "../actionType";
import {closeNotificationAction} from "./closeNotificationAction";

const DEFAULT_TIMEOUT = 3000;

export const raiseNotificationAction = (type, message, description) => ({
    type: actionType.RAISE_NOTIFICATION,
    notificationType: type,
    message: message,
    description: description
});

export const raiseTimedNotificationAction = (type, message, description, timeout = DEFAULT_TIMEOUT) => dispatch => {
    dispatch(raiseNotificationAction(type, message, description));
    window.setTimeout(() => {
        dispatch(closeNotificationAction())
    }, timeout);
};
