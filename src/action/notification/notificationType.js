export default {
    INFO: {
        value: "EDIT_INFO",
        className: "primary"
    },
    SUCCESS: {
        value: "SUCCESS",
        className: "success"
    },
    ERROR: {
        value: "ERROR",
        className: "danger"
    }
};
