import {getChatsApi} from "../../../api/account/chatApi";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {changeFormAction} from "../../form/changeFormAction";
import {getAccountApi} from "../../../api/account/accountApi";
import mediaType from "../../../api/mediaType";
import route from "../../../constant/route";
import errorType from "../../../constant/errorType";

export const getAccountChatsAction = (id, page, size, history) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_CHATS));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale, mediaType.JSON).then(account => {
            if (account) {
                if (credentials.id === parseInt(id)) {
                    dispatch(changeFormAction(formType.ACCOUNT_CHATS, {
                        id,
                        page,
                        size
                    }));
                    return getChatsApi(id,
                        page,
                        size,
                        credentials.accessToken,
                        credentials.locale);
                }
                history.push(route.ERROR(errorType.NOT_AUTHORIZED));
            } else {
                history.push(route.ERROR(errorType.NOT_FOUND));
            }
            dispatch(submitFormFailAction(formType.ACCOUNT_CHATS));
            return Promise.resolve();
        });
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.ACCOUNT_CHATS, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.ACCOUNT_CHATS));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
