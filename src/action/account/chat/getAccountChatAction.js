import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import route from "../../../constant/route";
import errorType from "../../../constant/errorType";
import {fetchFormFailAction, fetchFormStartAction, fetchFormSuccessAction} from "../../form/fetchFormAction";
import {getAccountApi} from "../../../api/account/accountApi";
import mediaType from "../../../api/mediaType";
import {clearFormAction} from "../../form/clearFormAction";
import {getChatMessagesAction} from "./message/getChatMessagesAction";

export const getAccountChatAction = (id, opponentId, history, messagePageSize) => dispatch => {
    dispatch(fetchFormStartAction(formType.ACCOUNT_CHAT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale, mediaType.JSON).then(async account => {
            let opponent = await getAccountApi(opponentId, credentials.accessToken, credentials.locale, mediaType.JSON);
            if (account && opponent && account.id !== opponent.id) {
                if (credentials.id === parseInt(id)) {
                    let chat = {
                        account,
                        opponent
                    };
                    dispatch(fetchFormSuccessAction(formType.ACCOUNT_CHAT, chat));
                    dispatch(clearFormAction(formType.ACCOUNT_CHAT_MESSAGE));
                    dispatch(clearFormAction(formType.ACCOUNT_CHAT_MESSAGES));
                    if (messagePageSize) {
                        dispatch(getChatMessagesAction(id, opponentId, 1, messagePageSize));
                    }
                    return Promise.resolve(chat);
                }
                history.push(route.ERROR(errorType.NOT_AUTHORIZED));
            } else {
                history.push(route.ERROR(errorType.NOT_FOUND));
            }
            dispatch(fetchFormFailAction(formType.ACCOUNT_CHAT));
            return Promise.resolve();
        }).catch(e => {
            dispatch(fetchFormFailAction(formType.ACCOUNT_CHAT));
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        });
    });
};
