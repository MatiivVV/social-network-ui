import {deleteChatApi} from "../../../api/account/chatApi"
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import formType from "../../form/formType";
import {getAccountChatsAction} from "./getAccountChatsAction";

export const deleteAccountChatAction = (id, opponentId, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return deleteChatApi(id, opponentId, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.deleteAccountChatSuccess"));
            let chatsForm = getState().forms[formType.ACCOUNT_CHATS];
            if (id === chatsForm.id) {
                dispatch(getAccountChatsAction(id, chatsForm.page, chatsForm.size, history));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
