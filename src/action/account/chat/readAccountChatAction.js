import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {setChatReadApi} from "../../../api/account/chatApi";
import formType from "../../form/formType";
import {changeFormAction} from "../../form/changeFormAction";
import {submitFormSuccessAction} from "../../form/submitFormAction";

export const readAccountChatAction = (id, opponentId) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return setChatReadApi(id, opponentId, credentials.accessToken, credentials.locale).then(readMessageCount => {
            let accountForm = getState().forms[formType.ACCOUNT];
            if (accountForm && accountForm.id === id) {
                dispatch(changeFormAction(formType.ACCOUNT, {
                    newMessageCount: readMessageCount < accountForm.newMessageCount
                        ? accountForm.newMessageCount - readMessageCount
                        : 0
                }));
            }
            let chatsForm = getState().forms[formType.ACCOUNT_CHATS];
            if (chatsForm && chatsForm._results && chatsForm._results.content) {
                let chats = chatsForm._results;
                dispatch(submitFormSuccessAction(formType.ACCOUNT_CHATS, false, {
                    ...chats,
                    content: chats.content.map(chat => {
                        let newMessageCount;
                        if (chat.opponentId === opponentId) {
                            newMessageCount = readMessageCount < chat.newMessageCount
                                ? chat.newMessageCount - readMessageCount
                                : 0;
                        } else {
                            newMessageCount = chat.newMessageCount;
                        }
                        return {
                            ...chat,
                            newMessageCount: newMessageCount
                        };
                    })
                }));
            }
        }).catch(e => {
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        });
    });
};
