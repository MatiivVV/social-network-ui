import {getChatMessagesApi} from "../../../../api/account/chatMessageApi";
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../../form/submitFormAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import formType from "../../../form/formType";
import {changeFormAction} from "../../../form/changeFormAction";

export const getChatMessagesAction = (id, opponentId, page, size) => dispatch => {
    dispatch(changeFormAction(formType.ACCOUNT_CHAT_MESSAGES, {
        id,
        page,
        size
    }));
    dispatch(submitFormStartAction(formType.ACCOUNT_CHAT_MESSAGES));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getChatMessagesApi(id,
            opponentId,
            page,
            size,
            credentials.accessToken,
            credentials.locale);
    }).then(result => {
        let content = [...result.content];
        content.reverse();
        dispatch(submitFormSuccessAction(formType.ACCOUNT_CHAT_MESSAGES, false, {
            ...result,
            content
        }));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.ACCOUNT_CHAT_MESSAGES));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
