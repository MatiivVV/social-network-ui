import {sendChatMessageApi} from "../../../../api/account/chatMessageApi"
import notificationType from "../../../notification/notificationType";
import formType from "../../../form/formType";
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../../form/submitFormAction";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../../notification/raiseNotificationAction";
import {UnprocessableEntityError} from "../../../../api/error/UnprocessableEntityError";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {addChatMessageAction} from "./addChatMessageAction";

export const sendChatMessageAction = (id, authorId, form, image) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_CHAT_MESSAGE));
    return dispatch(ensureAuthAction()).then(credentials => {
        let message = {...form};
        delete message._results;
        delete message._violations;
        delete message._submitting;
        message.message = message.message || "";
        return sendChatMessageApi(id,
            authorId,
            message,
            image,
            credentials.accessToken,
            credentials.locale
        ).then(message => {
            dispatch(submitFormSuccessAction(formType.ACCOUNT_CHAT_MESSAGE, true));
            dispatch(addChatMessageAction(message));
            return Promise.resolve(message);
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.ACCOUNT_CHAT_MESSAGE, e.error.violations));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else {
            dispatch(submitFormFailAction(formType.ACCOUNT_CHAT_MESSAGE));
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        }
    });
};
