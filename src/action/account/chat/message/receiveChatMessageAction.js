import formType from "../../../form/formType";
import {changeFormAction} from "../../../form/changeFormAction";
import {addChatMessageAction} from "./addChatMessageAction";
import {submitFormSuccessAction} from "../../../form/submitFormAction";

export const receiveChatMessageAction = message => (dispatch, getState) => {
    let accountForm = getState().forms[formType.ACCOUNT];
    if (accountForm && accountForm.id === message.recipientId) {
        dispatch(changeFormAction(formType.ACCOUNT, {newMessageCount: accountForm.newMessageCount + 1}));
    }

    let chatForm = getState().forms[formType.ACCOUNT_CHAT];
    if (chatForm && chatForm.account && chatForm.opponent
        && chatForm.account.id === message.recipientId && chatForm.opponent.id === message.authorId) {
        dispatch(addChatMessageAction(message));
    }

    let chatsForm = getState().forms[formType.ACCOUNT_CHATS];
    if (chatsForm && chatsForm._results && chatsForm._results.content) {
        let chats = chatsForm._results;
        dispatch(submitFormSuccessAction(formType.ACCOUNT_CHATS, false, {
            ...chats,
            content: chats.content.map(chat => ({
                ...chat,
                newMessageCount: chat.opponentId === message.authorId ? chat.newMessageCount + 1 : chat.newMessageCount
            }))
        }));
    }
    return Promise.resolve(message);
};
