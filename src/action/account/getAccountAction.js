import {getAccountApi} from "../../api/account/accountApi"
import {getFriendshipApi} from "../../api/account/friendshipApi"
import {ensureAuthAction} from "../user/ensureAuthAction";
import route from "../../constant/route";
import role from "../../constant/domain/role";
import {UnauthorizedError} from "../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../notification/raiseNotificationAction";
import notificationType from "../notification/notificationType";
import formType from "../form/formType";
import {fetchFormFailAction, fetchFormStartAction, fetchFormSuccessAction} from "../form/fetchFormAction";
import {changeUserAction} from "../user/changeUserAction";
import {clearFormAction} from "../form/clearFormAction";
import {getAccountMessagesAction} from "./message/getAccountMessagesAction";
import errorType from "../../constant/errorType";

export const getAccountAction = (id, history, messagePageSize) => dispatch => {
    dispatch(fetchFormStartAction(formType.ACCOUNT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale).then(async account => {
            if (account) {
                account.owned = credentials.id === parseInt(id);
                if (account.owned) {
                    dispatch(changeUserAction({
                        firstName: account.firstName,
                        lastName: account.lastName,
                        imageId: account.imageId,
                        role: account.role,
                        locale: account.locale
                    }));
                }
                account.related = account.owned || !!await getFriendshipApi(credentials.id, id, credentials.accessToken, credentials.locale);
                account.editable = credentials.role === role.ADMIN.value || credentials.id === parseInt(id);
                dispatch(fetchFormSuccessAction(formType.ACCOUNT, account));
                dispatch(clearFormAction(formType.ACCOUNT_MESSAGE));
                dispatch(clearFormAction(formType.ACCOUNT_MESSAGES));
                if (messagePageSize) {
                    dispatch(getAccountMessagesAction(id, 1, messagePageSize));
                }
                return Promise.resolve(account);
            }
            dispatch(fetchFormFailAction(formType.ACCOUNT));
            history.push(route.ERROR(errorType.NOT_FOUND));
            return Promise.resolve();
        });
    }).catch(e => {
        dispatch(fetchFormFailAction(formType.ACCOUNT));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
