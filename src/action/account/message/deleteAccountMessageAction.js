import {deleteAccountMessageApi} from "../../../api/account/accountMessageApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {getAccountMessagesAction} from "./getAccountMessagesAction";

export const deleteAccountMessageAction = (id, accountId, authorId) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return deleteAccountMessageApi(accountId, authorId, id, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.deleteMessageSuccess"));
            let messagesForm = getState().forms[formType.ACCOUNT_MESSAGES];
            dispatch(getAccountMessagesAction(accountId, messagesForm.page, messagesForm.size));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
