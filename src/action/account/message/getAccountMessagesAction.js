import {getAccountMessagesApi} from "../../../api/account/accountMessageApi";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {changeFormAction} from "../../form/changeFormAction";

export const getAccountMessagesAction = (id, page, size) => dispatch => {
    dispatch(changeFormAction(formType.ACCOUNT_MESSAGES, {
        id,
        page,
        size
    }));
    dispatch(submitFormStartAction(formType.ACCOUNT_MESSAGES));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountMessagesApi(id,
            page,
            size,
            credentials.accessToken,
            credentials.locale);
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.ACCOUNT_MESSAGES, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.ACCOUNT_MESSAGES));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
