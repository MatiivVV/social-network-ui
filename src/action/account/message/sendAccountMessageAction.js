import {sendAccountMessageApi} from "../../../api/account/accountMessageApi"
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {addAccountMessageAction} from "./addAccountMessageAction";

export const sendAccountMessageAction = (id, form, image) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_MESSAGE));
    return dispatch(ensureAuthAction()).then(credentials => {
        let message = {...form};
        delete message._results;
        delete message._violations;
        delete message._submitting;
        message.message = message.message || "";
        return sendAccountMessageApi(id,
            credentials.id,
            message,
            image,
            credentials.accessToken,
            credentials.locale
        ).then(message => {
            dispatch(submitFormSuccessAction(formType.ACCOUNT_MESSAGE, true));
            dispatch(raiseTimedNotificationAction(notificationType.INFO, "message.sendMessageSuccess"));
            dispatch(addAccountMessageAction(message));
            return Promise.resolve(message);
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.ACCOUNT_MESSAGE, e.error.violations));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else {
            dispatch(submitFormFailAction(formType.ACCOUNT_MESSAGE));
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        }
    });
};
