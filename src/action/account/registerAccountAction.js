import {registerAccountApi} from "../../api/account/accountApi"
import notificationType from "../notification/notificationType";
import formType from "../form/formType";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../form/submitFormAction";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../notification/raiseNotificationAction";
import {UnprocessableEntityError} from "../../api/error/UnprocessableEntityError";
import {loginAction} from "../user/loginAction";

export const registerAccountAction = (form, image) => (dispatch, getState) => {
    dispatch(submitFormStartAction(formType.ACCOUNT_REGISTER));
    let locale = getState().user.locale;
    let account = {...form};
    account.locale = locale;
    delete account._results;
    delete account._violations;
    delete account._submitting;
    delete account.confirmPassword;
    return registerAccountApi(account, image, locale).then(id => {
        dispatch(submitFormSuccessAction(formType.ACCOUNT_REGISTER, true));
        dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.registerSuccess"));
        dispatch(loginAction({
            emailAddress: account.emailAddress,
            password: account.rawPassword,
            rememberMe: false
        }));
        return id;
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.ACCOUNT_REGISTER, e.error.violations));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else {
            dispatch(submitFormFailAction(formType.ACCOUNT_REGISTER));
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
