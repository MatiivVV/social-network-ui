import {editAccountDeletedApi} from "../../../api/account/accountApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {changeFormAction} from "../../form/changeFormAction";

export const editAccountDeletedAction = (id, deleted) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_EDIT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return editAccountDeletedApi(id, deleted, credentials.accessToken, credentials.locale).then(async () => {
            dispatch(changeFormAction(formType.ACCOUNT_EDIT, {deleted: deleted}));
            dispatch(submitFormSuccessAction(formType.ACCOUNT_EDIT));
            let message = deleted ? "message.deleteAccountSuccess" : "message.restoreAccountSuccess";
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, message));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT));
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
