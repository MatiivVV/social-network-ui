import {editAccountRoleApi} from "../../../api/account/accountApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {changeFormAction} from "../../form/changeFormAction";
import {changeUserAction} from "../../user/changeUserAction";

export const editAccountRoleAction = (id, role) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_EDIT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return editAccountRoleApi(id, role, credentials.accessToken, credentials.locale).then(async () => {
            dispatch(changeFormAction(formType.ACCOUNT_EDIT, {role: role}));
            if (credentials.id === parseInt(id)) {
                dispatch(changeUserAction({
                    role: role
                }));
            }
            dispatch(submitFormSuccessAction(formType.ACCOUNT_EDIT));
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.editAccountRoleSuccess"));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT));
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
