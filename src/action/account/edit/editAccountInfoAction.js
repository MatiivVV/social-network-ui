import {editAccountInfoApi} from "../../../api/account/accountApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {changeUserAction} from "../../user/changeUserAction";
import mediaType from "../../../api/mediaType";
import {getAccountForEditAction} from "./getAccountForEditAction";
import {readFile} from "../../../util/fileUtil";

export const editAccountInfoAction = (id, info, infoMediaType, history) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_EDIT));
    return dispatch(ensureAuthAction()).then(async credentials => {
        return editAccountInfoApi(id,
            infoMediaType === mediaType.JSON ? info : await readFile(info),
            credentials.accessToken,
            credentials.locale,
            infoMediaType).then(async () => {
            if (credentials.id === parseInt(id)) {
                if (infoMediaType === mediaType.JSON) {
                    dispatch(changeUserAction({
                        firstName: info.firstName,
                        lastName: info.lastName,
                        locale: info.locale
                    }));
                } else {
                    dispatch(getAccountForEditAction(id, history, mediaType.JSON));
                }
            }
            dispatch(submitFormSuccessAction(formType.ACCOUNT_EDIT));
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.editAccountInfoSuccess"));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            switch (infoMediaType) {
                case mediaType.JSON:
                    dispatch(submitFormFailAction(formType.ACCOUNT_EDIT, e.error.violations));
                    break;
                case mediaType.XML:
                    dispatch(submitFormFailAction(formType.ACCOUNT_EDIT, e.error.violations.map(violation => ({
                        field: "xml",
                        message: violation.message
                    }))));
                    break;
                default:
                    throw new Error(`Illegal account info mediaType - ${infoMediaType}`)
            }
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT));
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
