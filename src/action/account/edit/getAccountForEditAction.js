import {getAccountApi} from "../../../api/account/accountApi"
import {ensureAuthAction} from "../../user/ensureAuthAction";
import route from "../../../constant/route";
import role from "../../../constant/domain/role";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {fetchFormFailAction, fetchFormStartAction, fetchFormSuccessAction} from "../../form/fetchFormAction";
import {changeUserAction} from "../../user/changeUserAction";
import errorType from "../../../constant/errorType";
import mediaType from "../../../api/mediaType";

export const getAccountForEditAction = (id, history, accountMediaType) => dispatch => {
    dispatch(fetchFormStartAction(formType.ACCOUNT_EDIT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale, accountMediaType).then(async account => {
            if (account) {
                switch (accountMediaType) {
                    case mediaType.JSON:
                        if (credentials.id === parseInt(id)) {
                            dispatch(changeUserAction({
                                firstName: account.firstName,
                                lastName: account.lastName,
                                imageId: account.imageId,
                                role: account.role,
                                locale: account.locale
                            }));
                        }
                        account.administered = credentials.role === role.ADMIN.value;
                        if (account.administered || credentials.id === parseInt(id)) {
                            dispatch(fetchFormSuccessAction(formType.ACCOUNT_EDIT, account));
                            return Promise.resolve(account);
                        }
                        break;
                    case mediaType.XML:
                        if (credentials.role === role.ADMIN.value || credentials.id === parseInt(id)) {
                            let a = document.createElement("a");
                            a.href = `data:${mediaType.XML};charset=UTF-8,${encodeURIComponent(account)}`;
                            a.download = "accountInfo.xml";
                            a.click();
                            dispatch(fetchFormSuccessAction(formType.ACCOUNT_EDIT, {}, true));
                            return Promise.resolve(account);
                        }
                        break;
                    default:
                        throw new Error(`Illegal account mediaType - ${accountMediaType}`)
                }
                history.push(route.ERROR(errorType.NOT_AUTHORIZED));
            } else {
                history.push(route.ERROR(errorType.NOT_FOUND));
            }
            dispatch(fetchFormFailAction(formType.ACCOUNT_EDIT));
            return Promise.resolve();
        });
    }).catch(e => {
        dispatch(fetchFormFailAction(formType.ACCOUNT_EDIT));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
