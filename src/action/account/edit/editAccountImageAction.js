import {editAccountImageApi} from "../../../api/account/accountApi"
import formType from "../../form/formType";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {changeUserAction} from "../../user/changeUserAction";

export const editAccountImageAction = (id, image) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_EDIT));
    return dispatch(ensureAuthAction()).then(credentials => {
        return editAccountImageApi(id, image, credentials.accessToken, credentials.locale).then(async location => {
            if (credentials.id === parseInt(id)) {
                dispatch(changeUserAction({
                    imageId: location
                }));
            }
            dispatch(submitFormSuccessAction(formType.ACCOUNT_EDIT));
            let message = image ? "message.editAccountImageSuccess" : "message.deleteAccountImageSuccess";
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, message));
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT, e.error.violations));
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(submitFormFailAction(formType.ACCOUNT_EDIT));
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
