import {getFriendsApi} from "../../../../api/account/friendshipApi";
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../../form/submitFormAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import formType from "../../../form/formType";
import {changeFormAction} from "../../../form/changeFormAction";
import role from "../../../../constant/domain/role";
import {getAccountApi} from "../../../../api/account/accountApi";
import mediaType from "../../../../api/mediaType";
import route from "../../../../constant/route";
import errorType from "../../../../constant/errorType";

export const getAccountFriendsAction = (id, page, size, history) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_FRIENDS));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale, mediaType.JSON).then(account => {
            if (account) {
                let editable = credentials.role === role.ADMIN.value || credentials.id === parseInt(id);
                dispatch(changeFormAction(formType.ACCOUNT_FRIENDS, {
                    id,
                    page,
                    size,
                    editable
                }));
                return getFriendsApi(id,
                    page,
                    size,
                    credentials.accessToken,
                    credentials.locale);
            }
            history.push(route.ERROR(errorType.NOT_FOUND));
            dispatch(submitFormFailAction(formType.ACCOUNT_FRIENDS));
            return Promise.resolve();
        });
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.ACCOUNT_FRIENDS, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.ACCOUNT_FRIENDS));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
