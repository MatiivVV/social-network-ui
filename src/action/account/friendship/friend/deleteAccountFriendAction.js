import {deleteFriendshipApi} from "../../../../api/account/friendshipApi"
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import {UnprocessableEntityError} from "../../../../api/error/UnprocessableEntityError";
import formType from "../../../form/formType";
import {getAccountFriendsAction} from "./getAccountFriendsAction";

export const deleteAccountFriendAction = (id, friendId, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return deleteFriendshipApi(id, friendId, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.deleteAccountFriendSuccess"));
            let friendsForm = getState().forms[formType.ACCOUNT_FRIENDS];
            if (id === friendsForm.id) {
                dispatch(getAccountFriendsAction(id, friendsForm.page, friendsForm.size, history));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
