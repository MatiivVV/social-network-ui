import {editFriendshipStatusApi} from "../../../api/account/friendshipApi"
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import formType from "../../form/formType";
import {getAccountFriendshipsAction} from "./getAccountFriendshipsAction";

export const editAccountFriendshipStatusAction = (id, friendId, status, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return editFriendshipStatusApi(id, friendId, status, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.editAccountFriendshipStatusSuccess"));
            let friendshipsForm = getState().forms[formType.ACCOUNT_FRIENDSHIPS];
            if (id === friendshipsForm.id) {
                dispatch(getAccountFriendshipsAction(id,
                    friendshipsForm.types,
                    friendshipsForm.statuses,
                    friendshipsForm.page,
                    friendshipsForm.size,
                    history
                ));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
