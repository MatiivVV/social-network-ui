import {getGroupsApi} from "../../../../api/group/accountMembershipApi";
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../../form/submitFormAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import formType from "../../../form/formType";
import {changeFormAction} from "../../../form/changeFormAction";
import role from "../../../../constant/domain/role";
import {getAccountApi} from "../../../../api/account/accountApi";
import mediaType from "../../../../api/mediaType";
import route from "../../../../constant/route";
import errorType from "../../../../constant/errorType";

export const getAccountGroupsAction = (id, roles, page, size, history) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_GROUPS));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale, mediaType.JSON).then(account => {
            if (account) {
                let editable = credentials.role === role.ADMIN.value || credentials.id === parseInt(id);
                dispatch(changeFormAction(formType.ACCOUNT_GROUPS, {
                    id,
                    roles,
                    page,
                    size,
                    editable
                }));
                return getGroupsApi(id,
                    roles,
                    page,
                    size,
                    credentials.accessToken,
                    credentials.locale);
            }
            history.push(route.ERROR(errorType.NOT_FOUND));
            dispatch(submitFormFailAction(formType.ACCOUNT_GROUPS));
            return Promise.resolve();
        });
    }).then(result => {
        dispatch(submitFormSuccessAction(formType.ACCOUNT_GROUPS, false, result));
    }).catch(e => {
        dispatch(submitFormFailAction(formType.ACCOUNT_GROUPS));
        if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
