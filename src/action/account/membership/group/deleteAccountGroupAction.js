import {deleteMembershipApi} from "../../../../api/group/accountMembershipApi"
import {ensureAuthAction} from "../../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../../notification/raiseNotificationAction";
import notificationType from "../../../notification/notificationType";
import {UnprocessableEntityError} from "../../../../api/error/UnprocessableEntityError";
import formType from "../../../form/formType";
import {getAccountGroupsAction} from "./getAccountGroupsAction";

export const deleteAccountGroupAction = (id, groupId, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return deleteMembershipApi(id, groupId, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.deleteAccountGroupSuccess"));
            let groupsForm = getState().forms[formType.ACCOUNT_GROUPS];
            if (id === groupsForm.id) {
                dispatch(getAccountGroupsAction(id, groupsForm.roles, groupsForm.page, groupsForm.size, history));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
