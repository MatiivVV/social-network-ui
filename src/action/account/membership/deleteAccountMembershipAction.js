import {deleteMembershipApi} from "../../../api/group/accountMembershipApi"
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction, raiseTimedNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import {UnprocessableEntityError} from "../../../api/error/UnprocessableEntityError";
import formType from "../../form/formType";
import {getAccountMembershipsAction} from "./getAccountMembershipsAction";

export const deleteAccountMembershipAction = (id, groupId, history) => (dispatch, getState) => {
    return dispatch(ensureAuthAction()).then(credentials => {
        return deleteMembershipApi(id, groupId, credentials.accessToken, credentials.locale).then(() => {
            dispatch(raiseTimedNotificationAction(notificationType.SUCCESS, "message.deleteAccountMembershipSuccess"));
            let membershipsForm = getState().forms[formType.ACCOUNT_MEMBERSHIPS];
            if (id === membershipsForm.id) {
                dispatch(getAccountMembershipsAction(id,
                    membershipsForm.statuses,
                    membershipsForm.page,
                    membershipsForm.size,
                    history
                ));
            }
        });
    }).catch(e => {
        if (e instanceof UnprocessableEntityError) {
            dispatch(raiseTimedNotificationAction(notificationType.ERROR, "message.unableToPerform", e.error.message));
        } else if (!(e instanceof UnauthorizedError)) {
            dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
        }
    });
};
