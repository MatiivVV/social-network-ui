import {getMembershipsApi} from "../../../api/group/accountMembershipApi";
import {ensureAuthAction} from "../../user/ensureAuthAction";
import {submitFormFailAction, submitFormStartAction, submitFormSuccessAction} from "../../form/submitFormAction";
import {UnauthorizedError} from "../../../api/error/UnauthorizedError";
import {raiseNotificationAction} from "../../notification/raiseNotificationAction";
import notificationType from "../../notification/notificationType";
import formType from "../../form/formType";
import {changeFormAction} from "../../form/changeFormAction";
import role from "../../../constant/domain/role";
import route from "../../../constant/route";
import errorType from "../../../constant/errorType";
import {getAccountApi} from "../../../api/account/accountApi";
import mediaType from "../../../api/mediaType";

export const getAccountMembershipsAction = (id, statuses, page, size, history) => dispatch => {
    dispatch(submitFormStartAction(formType.ACCOUNT_MEMBERSHIPS));
    return dispatch(ensureAuthAction()).then(credentials => {
        return getAccountApi(id, credentials.accessToken, credentials.locale, mediaType.JSON).then(account => {
            if (account) {
                if (credentials.role === role.ADMIN.value || credentials.id === parseInt(id)) {
                    dispatch(changeFormAction(formType.ACCOUNT_MEMBERSHIPS, {
                        id,
                        statuses,
                        size,
                        page
                    }));
                    return getMembershipsApi(id,
                        [],
                        statuses,
                        page,
                        size,
                        credentials.accessToken,
                        credentials.locale);
                }
                history.push(route.ERROR(errorType.NOT_AUTHORIZED));
            } else {
                history.push(route.ERROR(errorType.NOT_FOUND));
            }
            dispatch(submitFormFailAction(formType.ACCOUNT_FRIENDSHIPS));
            return Promise.resolve();
        }).then(result => {
            dispatch(submitFormSuccessAction(formType.ACCOUNT_MEMBERSHIPS, false, result));
        }).catch(e => {
            dispatch(submitFormFailAction(formType.ACCOUNT_MEMBERSHIPS));
            if (!(e instanceof UnauthorizedError)) {
                dispatch(raiseNotificationAction(notificationType.ERROR, "message.unexpectedError", e.message));
            }
        });
    });
};
