import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-datepicker/dist/react-datepicker.css";
import i18n from "./i18n/i18n";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import App from "./component/App";
import {appReducer} from "./reducer/appReducer";
import {getLocalStorageMiddleware, getLocalStorageState} from "./util/localStorageUtil";
import {APP_INITIAL_STATE} from "./initialState";

const store = createStore(
    appReducer,
    getLocalStorageState(APP_INITIAL_STATE),
    applyMiddleware(
        thunk,
        getLocalStorageMiddleware
    )
);

i18n.changeLanguage(store.getState().user.locale).then(() => {
    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </Provider>,
        document.getElementById("root")
    );
});
