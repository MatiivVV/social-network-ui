import actionType from "../action/actionType";
import {APP_INITIAL_STATE} from "../initialState";

export default (state, action) => {
    switch (action.type) {
        case actionType.RAISE_NOTIFICATION:
            return {
                active: true,
                type: action.notificationType,
                message: action.message,
                description: action.description
            };
        case actionType.CLOSE_NOTIFICATION:
            return APP_INITIAL_STATE.notification;
        default:
            return state;
    }
};
