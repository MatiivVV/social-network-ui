import userReducer from "./userReducer";
import notificationReducer from "./notificationReducer";
import formsReducer from "./formsReducer";
import actionType from "../action/actionType";
import {APP_INITIAL_STATE} from "../initialState";

export const appReducer = (state, action) => {
    if (action.type === actionType.LOGOUT) {
        return {
            ...APP_INITIAL_STATE,
            user: userReducer(state.user, action),
        };
    }
    return {
        user: userReducer(state.user, action),
        notification: notificationReducer(state.notification, action),
        forms: formsReducer(state.forms, action)
    };
};
