import actionType from "../action/actionType";
import {getTokenAsObject} from "../util/tokenUtil";

export default (state, action) => {
    switch (action.type) {
        case actionType.GET_TOKEN_SUCCESS:
            return {
                ...state,
                id: getTokenAsObject(action.token["access_token"])["user_id"],
                loggedIn: true,
                token: action.token,
                role: getTokenAsObject(action.token["access_token"])["user_role"]
            };
        case actionType.REFRESH_TOKEN_SUCCESS:
            return {
                ...state,
                token: action.token
            };
        case actionType.CHANGE_USER:
            return {
                ...state,
                ...action.content
            };
        case actionType.LOGOUT:
            return {
                loggedIn: false,
                locale: state.locale
            };
        default:
            return state;
    }
};
