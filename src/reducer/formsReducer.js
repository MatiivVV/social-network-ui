import actionType from "../action/actionType";
import {FORM_INITIAL_STATE} from "../initialState";

export default (state, action) => {
    if (action.name) {
        return {
            ...state,
            [action.name]: formReducer(state[action.name] || FORM_INITIAL_STATE, action)
        };
    }
    return state;
};

const formReducer = (state, action) => {
    switch (action.type) {
        case actionType.FETCH_FORM_START:
            return {
                ...FORM_INITIAL_STATE,
                _fetching: true
            };
        case actionType.FETCH_FORM_SUCCESS:
            return {
                ...(action.merge ? state : FORM_INITIAL_STATE),
                ...action.content,
                _fetching: false
            };
        case actionType.FETCH_FORM_FAIL:
            return {
                ...FORM_INITIAL_STATE
            };
        case actionType.CHANGE_FORM:
            return {
                ...state,
                ...action.content
            };
        case actionType.CLEAR_FORM:
            return {
                ...FORM_INITIAL_STATE
            };
        case actionType.SUBMIT_FORM_START:
            return {
                ...state,
                _fetching: false,
                _submitting: true,
                _results: [],
                _violations: []
            };
        case actionType.SUBMIT_FORM_SUCCESS:
            return {
                ...(action.clear ? FORM_INITIAL_STATE : state),
                _fetching: false,
                _submitting: false,
                _results: action.results,
                _violations: []
            };
        case actionType.SUBMIT_FORM_FAIL:
            return {
                ...state,
                _fetching: false,
                _submitting: false,
                _results: [],
                _violations: action.violations
            };
        default:
            return state;
    }
};
